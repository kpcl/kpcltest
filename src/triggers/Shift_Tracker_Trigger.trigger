trigger Shift_Tracker_Trigger on SHIFT_Tracker__c (before update,before insert) {
if(trigger.isBefore)
    {
        Shift_Tracker_Handler.autoFill_data(trigger.new);    
    }
}
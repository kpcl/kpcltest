/*********************************************************************************
Class Name      : ServiceChargeTrigger
Description     : Trigger on Service charge Object to implement Roll up Summary function in a lookup Relation between ServiceCharge and (RFQ,Opportunity)
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Designed as per client requirements.
*********************************************************************************/
trigger ServiceChargeTrigger on Service_Charge__c (before insert,before update,after insert,after update,after delete) {
    
    //RecordType ServiceChargeForRFQ  = [SELECT Id,name FROM RecordType WHERE SObjectType='Service_Charge__c' AND Name='Service Charge For RFQ' LIMIT 1];
    //RecordType AdditionalServiceCharge = [SELECT Id,name FROM RecordType WHERE SObjectType='Service_Charge__c' AND Name='Additional Service Charge' LIMIT 1];
    set<id> ServiceChargeId = new set<id>();
    set<id> AdditionalServiceChargeId = new set<id>();
    set<id> oldServiceChargeId = new set<id>();
    set<id> oldAdditionalServiceChargeId = new set<id>();
      set<id> BeforeInsertServiceChargeIds = new set<id>();
    if(trigger.isBefore)
    {
        if(trigger.isupdate || trigger.isinsert)
        {
           ServiceChargeTriggerHandler.UpdateTotalMargin(trigger.new);
       }  
       
    }
    if(trigger.isAfter)
    {
        if(trigger.isinsert)
        {
            for(Service_Charge__c s : trigger.new)
            {
                if(s.RecordTypeId == Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId())
                {
                    ServiceChargeId.add(s.id);   
                }
                if(s.RecordTypeId == Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Additional Service Charge').getRecordTypeId())
                {
                    AdditionalServiceChargeId.add(s.Id);
                }   
            }
        }
        if(trigger.isupdate)
        {
            for(Service_Charge__c s : trigger.new)
            {
                if(s.RecordTypeId == Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId())
                {
                    ServiceChargeId.add(s.id);   
                    system.debug('ServiceChargeId');
                }
                if(s.RecordTypeId == Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Additional Service Charge').getRecordTypeId())
                {
                    AdditionalServiceChargeId.add(s.Id);
                }   
            }
        }
        if(Trigger.isDelete)
        {
            
            for(Service_Charge__c s : trigger.old)
            {
                if(s.RecordTypeId == Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId())
                {
                    oldServiceChargeId.add(s.id);   
                }
                if(s.RecordTypeId == Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Additional Service Charge').getRecordTypeId())
                {
                    oldAdditionalServiceChargeId.add(s.Id);
                }    
            }
            
            
        }
    
    }
    if(ServiceChargeId.size()>0)
    {
         system.debug('Eneterinf');
        ServiceChargeTriggerHandler.sumupValues(ServiceChargeId, trigger.new);
        system.debug('Eneterinf');
    }
    if(AdditionalServiceChargeId.size()>0)
    {
        ServiceChargeTriggerHandler.OpportunitySumUp(AdditionalServiceChargeId, trigger.new);
    }
    if(oldServiceChargeId.size()>0)
    {
        ServiceChargeTriggerHandler.sumupValues(ServiceChargeId, trigger.old);
    }
    if(oldAdditionalServiceChargeId.size()>0)
    {
        ServiceChargeTriggerHandler.OpportunitySumUp(AdditionalServiceChargeId, trigger.old);
    }
    
}
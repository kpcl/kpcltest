/*********************************************************************************
Trigger Name    : InsertVendorNamestoRFQservice
Description     : Trigger on RFQ.Used to collect RFQs which are approved and pass to handler
Created By      : Naga Sai
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                   Jan-2018        Designed as per client requirements.
*********************************************************************************/
trigger InsertVendorNamestoRFQservice on RFQ__c (after insert,after update,after delete) {
    public static boolean firstRun = true;
    if(trigger.isAfter)
    {
         if(trigger.isInsert)
        {            
            if(firstRun == true){
                set<id>RFQServiceIds = new set<id>();
                //iterating on trigger.new to check RFQ status of each record
                for(RFQ__c rfq:trigger.new){
                    //if RFQ status has been changed to approved add RFQService Ids to set
                    if(rfq.Status__c == 'Approved'){
                        RFQServiceIds.add(rfq.RFQ_Service__c);
                    }
                }
                if(RFQServiceIds.size()>0){
                    firstRun = false;
                    //Calling handler class method if list is not empty
                    InsertVendorNamestoRFQserviceHandler r = new InsertVendorNamestoRFQserviceHandler();
                    r.InsertVendornames(RFQServiceIds);
                }
            }
        }
        if(trigger.isUpdate)
        {            
            if(firstRun == true){
                set<id>RFQServiceIds = new set<id>();
                //iterating on trigger.new to check RFQ status of each record
                for(RFQ__c rfq:trigger.new){
                    //if RFQ status has been changed to approved add RFQService Ids to set
                    if((rfq.Status__c == 'Approved' && trigger.oldmap.get(rfq.id).Status__c != 'Approved') || (rfq.Status__c != 'Approved' && trigger.oldmap.get(rfq.id).Status__c == 'Approved')){
                        RFQServiceIds.add(rfq.RFQ_Service__c);
                    }
                }
                if(RFQServiceIds.size()>0){
                    firstRun = false;
                    //Calling handler class method if list is not empty
                    InsertVendorNamestoRFQserviceHandler r = new InsertVendorNamestoRFQserviceHandler();
                    r.InsertVendornames(RFQServiceIds);
                }
            }
        }
        if(trigger.isDelete)
        {            
            if(firstRun == true){
                set<id>RFQServiceIds = new set<id>();
                //iterating on trigger.new to check RFQ status of each record
                for(RFQ__c rfq:trigger.old){
                    //if RFQ status has been changed to approved add RFQService Ids to set
                    RFQServiceIds.add(rfq.RFQ_Service__c);
                }
                if(RFQServiceIds.size()>0){
                    firstRun = false;
                    //Calling handler class method if list is not empty
                    InsertVendorNamestoRFQserviceHandler r = new InsertVendorNamestoRFQserviceHandler();
                    r.InsertVendornames(RFQServiceIds);
                }
            }
        }
    }
}
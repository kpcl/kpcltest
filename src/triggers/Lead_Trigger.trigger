/*********************************************************************************
Class Name      : Lead_Trigger
Description     : Trigger on Lead. Used for round robin assignment of Leads.
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018        Designed as per client requirements.
*********************************************************************************/
trigger Lead_Trigger on Lead (after insert,before update) {
     if(trigger.isAfter)
    {
        if(trigger.isinsert)
        {
          for(Lead newLead:Trigger.new)
          {
              
              LeadTriggerHandler.Assign_LeadOwner_BasedOn_RRLogic_OnInsert(newLead);
              if(newLead.Pass_to_BD_Team__c==true)
              {
               LeadTriggerHandler.Assign_LeadOwner_BasedOn_RRLogic_OnInsert_if_Pass_to_BD_Team_Selected(newLead);   
              }
          }
        }
    }
    if(trigger.isBefore)
    {
        if(trigger.isUpdate)
        {
            for(Lead newLead:Trigger.new)
          {
              if(newLead.Pass_to_BD_Team__c==true)
              {
               LeadTriggerHandler.Assign_LeadOwner_BasedOn_RRLogic_OnUpdate_if_Pass_to_BD_Team_Selected(newLead);   
              }
              
          }
            
        }
    }

}
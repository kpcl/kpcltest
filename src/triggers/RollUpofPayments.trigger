/*********************************************************************************
Trigger Name      : RollUpofPayments
Description     : Being used to roll up payments to opportunity and RFQ respectively 
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                  Jan-2017        Designed as per Client requirements
*********************************************************************************/
trigger RollUpofPayments on Payment__c (after insert,after update,after delete) {
    set<id> Vendorpaymentids=new set<id>();  
    set<id> VendorpaymentOldids=new set<id>();
    set<id> Customerpaymentids=new set<id>();  
    set<id> CustomerpaymentOldids=new set<id>();
    string vendorrtid;
    string customerrtid;
    //querying recordtypes and storing 
    vendorrtid=Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Payment to Vendor').getRecordTypeId();
    customerrtid=Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Payment from Customer').getRecordTypeId();
    //storing ids
    if(trigger.Isinsert || trigger.isupdate){
        for(Payment__c p:trigger.new){   
            if( p.RecordTypeId==vendorrtid){
                Vendorpaymentids.add(p.id);
            }
            if( p.RecordTypeId==customerrtid){
                Customerpaymentids.add(p.id);
                system.debug('paymentids'+Customerpaymentids);
            }  
        }
    }
    //storing ids
    if(Trigger.isAfter && Trigger.isDelete){         
        for(Payment__c p:trigger.old){       
            if( p.RecordTypeId==vendorrtid){
                VendorpaymentOldids.add(p.id);
            }
            if( p.RecordTypeId==customerrtid){
                CustomerpaymentOldids.add(p.id);
            }
        }
    }
    if(Vendorpaymentids.size()>0){
        RollUpofPaymentsHandlerClass s=new RollUpofPaymentsHandlerClass();
        s.CalculateAmountonRFQ(Vendorpaymentids,trigger.new);
    }
    if(VendorpaymentOldids.size()>0){
        RollUpofPaymentsHandlerClass s=new RollUpofPaymentsHandlerClass();
        s.CalculateAmountonRFQ(VendorpaymentOldids,trigger.old);
    }
    if(Customerpaymentids.size()>0){
        RollUpofPaymentsHandlerClass s=new RollUpofPaymentsHandlerClass();
        s.CalculateAmountonOpportunity(Customerpaymentids,trigger.new);
    }
    if(CustomerpaymentOldids.size()>0){
        RollUpofPaymentsHandlerClass s=new RollUpofPaymentsHandlerClass();
        s.CalculateAmountonOpportunity(CustomerpaymentOldids,trigger.old);
    }
}
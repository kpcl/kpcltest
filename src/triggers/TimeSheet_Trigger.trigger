trigger TimeSheet_Trigger on TimeSheet__c (after insert,before update,before insert) {
    
    if(trigger.isBefore)
    {
        TimeSheet_Handler.autoFill_id_department_by_name(trigger.new);    
    }
    if(trigger.isAfter)
    {
        if(trigger.isinsert)
        {
            for(TimeSheet__c eachRecord : trigger.new)
            {
                TimeSheet_Handler.Create_TimeSheet_Entry(eachRecord);
            }
        }
    }
    
}
/*********************************************************************************
Class Name      : CreationOfRFQAndCMSServices
Description     : Trigger on Opportunity to automate RFQ Service and CMS operation record creation on 
				  opportunity stage change to RFQ and CMS stages respectively.
Created By      : Naga Sai
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                   Jan-2018          Designed as per client requirements.
*********************************************************************************/

trigger CreationOfRFQAndCMSServices on Opportunity (before update,after update,after insert) {
   //variables declared for ServiceChargeAndRFQvalidationHandler
    set<id> OpportuntyidNegotiation=new set<id>();
    map<id,Opportunity> oppmapsNegotiation=new  map<id,Opportunity>();
    set<id> Opportuntyidproposal=new set<id>();
    map<id,Opportunity> oppmapsproposal=new  map<id,Opportunity>();
    if(Trigger.isBefore)
    {
        if(Trigger.isUpdate)
        {
            for(Opportunity newop:Trigger.new)
            {
                Opportunity old = Trigger.oldMap.get(newop.Id);
                if(newop.Id==old.Id)
                {
                    if(newop.StageName!=old.StageName)
                    {
                        if(newop.StageName=='Closed Won')
                        {
                            Opportunity_Target_Handler.update_Target_Records(newop);
                        }
                        RRUserAssignmentOnOpportunity.UserAssignmentOnStageChange(newop);  
                    }
                }
                if( Trigger.oldMap.get(newop.Id).Storage_Calculation_Start_Date__c != Trigger.newMap.get(newop.Id).Storage_Calculation_Start_Date__c)
                {
                  RRUserAssignmentOnOpportunity.Add_Task_To_Finance_Member_on_Storage_Date_Change(newop);
                }
                if((Trigger.oldMap.get(newop.Id).StageName=='Negotiation' || Trigger.oldMap.get(newop.Id).StageName=='Proposal' || Trigger.oldMap.get(newop.Id).StageName=='CMS') && Trigger.newMap.get(newop.Id).StageName=='RFQ')
                {
                    RRUserAssignmentOnOpportunity.If_Stage_Changes_From_Nego_Or_Proposal_To_RFQ_Create_Task(newop);
                }
               //checking if stage is negotiation -------ServiceChargeAndRFQvalidationHandler
                if(newop.StageName=='Negotiation' && Trigger.oldMap.get(newop.Id).StageName!='Negotiation'){
                  OpportuntyidNegotiation.add(newop.id);  
                oppmapsNegotiation.put(newop.id, newop);
                }
                if(newop.StageName=='proposal' && Trigger.oldMap.get(newop.Id).StageName!='proposal'){
                  Opportuntyidproposal.add(newop.id);  
                oppmapsproposal.put(newop.id, newop);
                }
            }
            if(OpportuntyidNegotiation.size()>0)
                ServiceChargeAndRFQvalidationHandler.validatemarginonservicecharge(OpportuntyidNegotiation,oppmapsNegotiation);
             
         if(OpportuntyidProposal.size()>0)
                ServiceChargeAndRFQvalidationHandler.validateRFQsandServicechargesCount(Opportuntyidproposal,oppmapsproposal);
        }
    }
   
    if(Trigger.isAfter){
        if(Trigger.isInsert)
        {
                        
                       OpportunityTimeLine.firstRecord(Trigger.New);
            
            for(Opportunity p:Trigger.new){
                
                RRUserAssignmentOnOpportunity.ChangeStageToRFQ(p);
            }
          }
        //start of after update
        if(Trigger.isUpdate){
            //logic for OpportunityTimeLine Update
            for(Opportunity newop:Trigger.new)
            {
                Opportunity old = Trigger.oldMap.get(newop.Id);
                if(newop.Id==old.Id)
                {
                    if(newop.StageName!=old.StageName)
                    {
                        OpportunityTimeLine.otherRecord(old, newop);  
                    }
                }
            }
            list<Opportunity> OppListforRFQ=new list<Opportunity>();
            list<Opportunity> OppListforCMS=new list<Opportunity>();
            set<id> oppListIdsforCMS=new set<id>();
            set<id> Oppid=new set<id>();
            for(opportunity opp: trigger.new){
                Oppid.add(opp.id)  ;
            }
            //querying child records as trigger.new will not contain information about child records
            list<Opportunity> OppRFQs=[select id,(select id from RFQ_Services__r) from Opportunity where id in:Oppid];
            list<Opportunity> OppCMS=[select id,(select id from CMS_Operations__r) from Opportunity where id in:Oppid];
            map<id,list<RFQ_Service__c>> OppRFQmap=new map<id,list<RFQ_Service__c>>();
            map<id,list<CMS_Operations__c>> OppCMSmap=new map<id,list<CMS_Operations__c>>();
            //mapping opportunity id with RFQservice records
            for(Opportunity opp :OppRFQs){
                OppRFQmap.put(opp.id, opp.RFQ_Services__r);
            }
            //mapping opportunity id with CMSService records
            for(Opportunity opp :OppCMS){
                OppCMSmap.put(opp.id, opp.CMS_Operations__r);
            }
            for(Opportunity opp : trigger.new) {
                list<RFQ_service__c> rfq=new list<rfq_service__c>();
                list<CMS_Operations__c> CMS=new list<CMS_Operations__c>();
                rfq=OppRFQmap.get(opp.id);
                cms=OppCMSmap.get(opp.id);
                //checking if previous stage is not the same as the present stage ,checking if current satge is RFQ and checking if there are no RFQ servcie records created
                if(opp.StageName!=trigger.oldmap.get(opp.id).StageName && opp.StageName == 'RFQ' && rfq.isEmpty()){
                    OppListforRFQ.add(opp);          
                }
                //checking if previous stage is not the same as the present stage ,checking if current satge is CMS and checking if there are no CMS  records created
                if(opp.StageName!=trigger.oldmap.get(opp.id).StageName && opp.StageName == 'CMS' && cms.isEmpty()) {
                    OppListforCMS.add(Opp);
                    oppListIdsforCMS.add(opp.id);
                }               
            }
            //if size is greater han zero calling handler class
            if(OppListforRFQ.size()>0){
                CreationOfRFQAndCMSServicesHandler c=new CreationOfRFQAndCMSServicesHandler();
                c.createRFQService(OppListforRFQ);
            }
            if(OppListforCMS.size()>0){
                CreationOfRFQAndCMSServicesHandler c1=new CreationOfRFQAndCMSServicesHandler();
                c1.createCMSOperations(OppListforCMS,oppListIdsforCMS); 
            }
        }
    }//end of after update    
}
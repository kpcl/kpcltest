trigger Time_Sheet_Entry_Trigger on Time_Sheet_Entry__c (before insert,before update) {

    if(trigger.isBefore)
    {
        if(trigger.isinsert)
        {
            for(Time_Sheet_Entry__c newRecord:trigger.new)
            {
          Time_Sheet_Entry_Trigger_Handler.prevent_More_Than_9_Hours(newRecord);  
            }
        }
		if(trigger.isupdate)
        {
            System.debug('Time_Sheet_Entry_Trigger_Handler');
            for(Time_Sheet_Entry__c NewRecord:Trigger.new)
            {
                Time_Sheet_Entry__c oldRecord = Trigger.oldMap.get(NewRecord.Id);
                if(NewRecord.Id==oldRecord.Id)
                {
                    if(NewRecord.Duration_In_Hours__c !=oldRecord.Duration_In_Hours__c )
                    {
                        System.debug('Time_Sheet_Entry_Trigger_Handler - Entering FUnction');
                        Time_Sheet_Entry_Trigger_Handler.prevent_More_Than_9_Hours(NewRecord);  
                        //Time_Sheet_Entry_Trigger_Handler.prevent_More_Than_9_Hours_in_entry(Trigger.oldMap,Trigger.newMap);
                    }
                }      
       	   }
    	}
    }
    if(trigger.isAfter)
    {
    }
    
        


}
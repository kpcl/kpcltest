@istest
class Test_Schedule_Target_Record_Insertion {

   static testmethod void test() {
   Test.startTest();

      // Schedule the test job

      String jobId = System.schedule('Schedule Insert Target Records',
      '0 0 2 1 * ?', 
         new Schedule_Target_Record_Insertion());

      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
      System.assertEquals('0 0 2 1 * ?', 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

      // Verify the next time the job will run
      System.assertEquals('2018-05-01 02:00:00', 
         String.valueOf(ct.NextFireTime));
     

   Test.stopTest();

   
   }
}
/*********************************************************************************
Class Name      : LeadTriggerHandler_Test
Description     : Test Class for LeadTriggerHandler
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Design as per Client
*********************************************************************************/
@IsTest
public class LeadTriggerHandler_Test {
    @isTest static Lead TestDataGenerator()
    {
        //create Lead Records -- 
        Lead newLead=new Lead(lastname='krishnan',firstname='Unni',Email='bombay@gmail.com',Company='Bombay Enterprise',MobilePhone='2345678989',Status='New',Cargo__c='Weapons',POD__c='test',POL__c='test',Shipping_Options__c='Import',Shipping_Type__c='Bulk',Net_Weight__c=233,Scope_Of_Services_for_Container__c='CHA');
        insert newLead;  
        return newLead;
    }
    @isTest static void CheckOnInsert()
    {
        //Get List of User from Custom Meta Data To assign to any one of the member using RR
        List<LeadOwnerMember__mdt> LeadMembers = [select id,userName__c from LeadOwnerMember__mdt ];
        List<String> ListOfUserName = new List<String>();
        for (LeadOwnerMember__mdt m:LeadMembers)
        {
            ListOfUserName.add(m.userName__c); 
        }
        System.debug('ListOfUserName - on insert'+ListOfUserName);
        //Get the User Records from User Object using Username List 
        List<User> UserList = [select id,name,Username from User where Username=:ListOfUserName];
        Lead newLead=LeadTriggerHandler_Test.TestDataGenerator();
        if(UserList.size()>0){
            Lead Result = [select id,name,OwnerId,RRModInput__c from Lead where id=:newLead.Id];
            System.debug('RRMOD'+Result.RRModInput__c);
            ///RR LOgic
            Integer ResultRemainder = math.mod(integer.valueof(Result.RRModInput__c),UserList.size());
            System.assertEquals(UserList[ResultRemainder].id,Result.OwnerId);
        }
    }
    @isTest static void PassTOBDTeam(){
      List<BD_Member__mdt> LeadMembers = [select id,userName__c from BD_Member__mdt];
        List<String> ListOfUserName = new List<String>();
        for (BD_Member__mdt m:LeadMembers)
        {
            ListOfUserName.add(m.userName__c); 
        }
        System.debug('ListOfUserName - on pass to bd team'+ListOfUserName);
        //Get the User Records from User Object using Username List 
        List<User> UserList = [select id,name,Username from User where Username=:ListOfUserName];
        Lead newLead=LeadTriggerHandler_Test.TestDataGenerator();
        Id oldUserId = newLead.OwnerId;//get old user id
        newLead.Pass_to_BD_Team__c=true;
        update newLead;
        if(UserList.size()>0){
            Lead Result = [select id,name,OwnerId,RRModInput__c from Lead where id=:newLead.Id];
            System.debug('RRMOD'+Result.RRModInput__c);
            ///RR LOgic
            Integer ResultRemainder = math.mod(integer.valueof(Result.RRModInput__c),UserList.size());
            System.assertEquals(UserList[ResultRemainder].id,Result.OwnerId);
        }          
      // Insert manual share for user who is not record owner.
      }
    
}
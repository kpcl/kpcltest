/*********************************************************************************
Class Name      : TestApprovedRFQstoBDTeam
Description     : Being used to test ApprovedRFQstoBDTeam
Created By      : Naga sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga sai                 jan-2017            Design as per Client
*********************************************************************************/
@isTest
public class TestApprovedRFQstoBDTeam {
    @isTest static void testgetRFQserviceTable(){
        Date closedDate = Date.newInstance(2018, 01, 09);
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account account=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert account;
        //inserting Opportunity,RFQservice,RFQs and Service charges
        Opportunity op = new Opportunity(Name='testt',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Number_Of_20_type_Container__c=3,Per_TEU_Weight_in_MT__c=30);
        insert op;      
        RFQ_Service__c rfqservice=new RFQ_Service__c();
        rfqservice.name='CHAsd';
        rfqservice.Opportunity__c=op.id;
        insert rfqservice;
        //inserting RFQs and making it approved
        RFQ__c rfq1=new RFQ__c();
        rfq1.RFQ_Service__c=rfqservice.id;
        rfq1.Vendor_Account__c=account.id;
        rfq1.status__c='Approved';
        insert rfq1;
        RFQ__c rfq2=new RFQ__c();
        rfq2.RFQ_Service__c=rfqservice.id;
        rfq2.Vendor_Account__c=account.id;
        rfq2.status__c='Approved';
        insert rfq2;
        Service_Charge__c s1=new Service_Charge__c();
        s1.RFQ__c=rfq1.id;
         insert s1;
        Service_Charge__c s2=new Service_Charge__c();
        s2.RFQ__c=rfq1.id;
        insert s2;
        //Invoking method and assigning opportunity id
        ApprovedRFQstoBDTeam a=new ApprovedRFQstoBDTeam();        
        a.Opportunitid=op.id;
        a.getRFQserviceTable();
        a.getRFQsTable();        
    }
}
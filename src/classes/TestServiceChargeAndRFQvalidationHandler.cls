/*********************************************************************************
Class Name      : TestServiceChargeAndRFQvalidationHandler
Description     : used to test ServiceChargeAndRFQvalidationHandler
Created By      : Naga Sai
Created Date    : 28-Feb-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                  28-Feb-2018         Designed as per client requirements.
*********************************************************************************/
@isTest
public class TestServiceChargeAndRFQvalidationHandler {
   //without creating RFQs updating stage to proposal.
    @isTest static void testvalidateRFQsCount(){
        Account account=new Account(name='test',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId());
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        //to insert payment records we need opportunity,rfqservice and rfq 
        Opportunity op = new Opportunity(Name='test',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='878',Destination_PinCode__c='767',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=3);
        insert op;
        try{
            op.StageName='Proposal';
            update op;
        }
        catch(Exception e){
            
        }
    }
     //without creating servicecharges updating stage to proposal.
    @isTest static void testvalidateServicechargesCount(){
        Account account=new Account(name='test',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId());
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        //to insert payment records we need opportunity,rfqservice and rfq 
        Opportunity op = new Opportunity(Name='test',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='878',Destination_PinCode__c='767',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=3);
        insert op;
        list<RFQ_Service__c> service1 = [select id from RFQ_Service__c];
        //Creating RFQ Record for the RFQ service
        list<Rfq__c> rfqlist=new list<RFQ__c>();
        for(RFQ_Service__c rfqservice:service1){
            Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=rfqservice.id,Amount_Paid__c=50, Status__c='Approved');
            rfqlist.add(RFQ1);
        }
        insert rfqlist;
        try{
            op.StageName='Proposal';
            update op;
        }
        catch(Exception e){
            
        }
    }
     //without creating margin on service charge updating stage to negotitation.
    @isTest static void testvalidatemarginonservicecharge(){
        Account account=new Account(name='test',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId());
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        //to insert payment records we need opportunity,rfqservice and rfq 
        Opportunity op = new Opportunity(Name='test',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='878',Destination_PinCode__c='767',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=3);
        insert op;
        list<RFQ_Service__c> service1 = [select id from RFQ_Service__c];
        //Creating RFQ Record for the RFQ service
        list<Rfq__c> rfqlist=new list<RFQ__c>();
        for(RFQ_Service__c rfqservice:service1){
            Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=rfqservice.id,Amount_Paid__c=50, Status__c='Approved');
            rfqlist.add(RFQ1);
        }
        insert rfqlist;
        //craeting service charge for each RFQ
        list<Service_Charge__c> servicechargelist=new  list<Service_Charge__c>();
        for(RFQ__C rfq:rfqlist){
            Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
            servicechargelist.add(Charge1);
        }
        insert servicechargelist;
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
    }
}
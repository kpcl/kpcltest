public class RelatedTargetFieldsClass1 {
    @AuraEnabled
    public static List<Bulk_Commodity_Mapping__mdt> getCustomMetaData()
    {
        List<Bulk_Commodity_Mapping__mdt> allRecords = [select id,DeveloperName,Commodity_Name__c, RelatedCommodity__c  FROM Bulk_Commodity_Mapping__mdt];
        return allRecords;
    }
    
    public static List<String> ListOfAllCommodity()
    {
        List<String> allCommodityName = new List<String>();
        Map<String,List<String>> CommodityAndRelatedMapping = new Map<String,List<String>>();
        List<Bulk_Commodity_Mapping__mdt> allCustomMetaDataRecords = Related_Target_Fields_Class.getCustomMetaData();
        for(Bulk_Commodity_Mapping__mdt eachRecord :allCustomMetaDataRecords ){
            if(eachRecord.RelatedCommodity__c!='' || eachRecord.RelatedCommodity__c!=null)
            {
                String allCommodity = eachRecord.RelatedCommodity__c;
                String name =eachRecord.Commodity_Name__c; 
                CommodityAndRelatedMapping.put(name.replaceAll(' ','_'),allCommodity.split(','));
                allCommodityName.addAll(allCommodity.split(','));
                
            }
        }
        return allCommodityName;
        
    }
    @AuraEnabled
    public static String insertData(Map<String,String> TargetMap,Id recordID,Map<String,String> containerMap)
    {
        String result ='';
        DescribeSObjectResult describeResult = recordID.getSObjectType().getDescribe();	
        Map<String,Decimal> ExpandedTargetMap = new Map<String,Decimal>();
        for (String keys:TargetMap.keySet())
        {
            system.debug(keys+':'+TargetMap.get(keys));
        }
        List<String> allCommodityName = new List<String>();
        Map<String,List<String>> CommodityAndRelatedMapping = new Map<String,List<String>>();
        List<Bulk_Commodity_Mapping__mdt> allCustomMetaDataRecords = Related_Target_Fields_Class.getCustomMetaData();
        for(Bulk_Commodity_Mapping__mdt eachRecord :allCustomMetaDataRecords ){
            if(eachRecord.RelatedCommodity__c!='' || eachRecord.RelatedCommodity__c!=null)
            {
                String allCommodity = eachRecord.RelatedCommodity__c;
                String name =eachRecord.Commodity_Name__c; 
                CommodityAndRelatedMapping.put(name.replaceAll(' ','_'),allCommodity.split(','));
                allCommodityName.addAll(allCommodity.split(','));
                
            }
        }	
        System.debug(allCommodityName);
        Map <String, Schema.SObjectField> fieldMap = describeResult.fields.getMap();
        for(String f:fieldMap.keySet())
        {
            for(String each:allCommodityName){
                String strippedeach = each.replaceAll(' ','_');
                if(f.containsIgnoreCase(strippedeach))
                {
                    system.debug('f--'+f);
                    if(f.endsWith('proj__c'))
                    {
                        
                        String apiName = getKeyByValue(CommodityAndRelatedMapping,each);
                        if(f.contains('metric_ton'))
                        {
                            if(apiName!=null)
                            {
                                System.debug('APiName:'+apiName+'_Number_of_Metric_Ton_Proj__c');
                                String KeyToGetFromTargetMap = apiName+'_Number_of_Metric_Ton_Proj__c';
                                if(TargetMap.containsKey(KeyToGetFromTargetMap)){
                                    System.debug('Target Map wola :'+TargetMap.get(KeyToGetFromTargetMap));
                                    if(TargetMap.get(KeyToGetFromTargetMap)!=''){
                                        Decimal v =decimal.valueOf(TargetMap.get(KeyToGetFromTargetMap)); 
                                        ExpandedTargetMap.put(f,v);
                                    }  
                                }
                                
                            }
  
                        }
                        if(f.contains('revenue'))
                        {
                            System.debug('APiName:'+apiName+'_Revenue_of_Bulk_Proj__c');
                            String KeyToGetFromTargetMap = apiName+'_Revenue_of_Bulk_Proj__c';
                            if(TargetMap.containsKey(KeyToGetFromTargetMap)){
                                if(TargetMap.get(KeyToGetFromTargetMap)!=''){
                                    Decimal v =decimal.valueOf(TargetMap.get(KeyToGetFromTargetMap)); 
                                    ExpandedTargetMap.put(f,v);
                                }  
                            }
                            
                        }
                        
                    }
                }
            }
        }
        
        for(String key:containerMap.keySet())
        {
            if(containerMap.get(key)!=''){
          Decimal v =decimal.valueOf(containerMap.get(key));
          ExpandedTargetMap.put(key,v); 
            }
        }
        
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );	
        String query =' SELECT ' +String.join( fieldNames, ',' ) +' FROM '+describeResult.getName() +' WHERE ' +' id = :recordID ' +' LIMIT 1 ';
        
        Target__c rec = Database.query( query );
        for(String api:ExpandedTargetMap.keySet())
        {
            rec.put(api,ExpandedTargetMap.get(api));
        }
        System.debug('Target Map:---'+ExpandedTargetMap);
        Database.SaveResult srList = Database.update(rec, false);
        if (srList.isSuccess()) {
            result='Succesfully Update Target Records';
        }
        else {
            // Operation failed, so get all errors
            result='The following error has occurred.\n';                
            for(Database.Error err : srList.getErrors()) {
                result+= err.getStatusCode() + ': ' + err.getMessage();
            }
        }
        
        return  result;
    }
    public static string getKeyByValue(Map<String,List<String>> mymap,String value)
    {
        for(String s:mymap.keySet())
        {
            if(mymap.get(s).contains(value))
            {
                return s;
            }
            
        }
        return null;
    }
    @AuraEnabled
    public static string displayUserdetails(id id)
    {
        Target__c a=[Select id,User__r.Name from Target__c where id=:Id];
        return a.User__r.Name ;
        
    }
    @AuraEnabled
    public static TargetNameAndDate displayUserdetails_Month_Year(id id)
    {
        Target__c a=[Select id,User__r.Name, Start_Date__c,End_Date__c from Target__c where id=:Id];
        TargetNameAndDate r = new TargetNameAndDate();
        DateTime sdate = a.Start_Date__c ;
        r.month = sdate.format('MMMMM');
        r.Year = sdate.year();
        r.name = a.User__r.Name ;
        return r;
    }
    public class TargetNameAndDate{
        @AuraEnabled public String month ;
        @AuraEnabled public Integer Year; 
        @AuraEnabled public String name; 
    }
     @AuraEnabled
   public static boolean isrendered(){
    boolean isrendered = true;
    if (UserInfo.getUserType() == '') {
        System.debug(UserInfo.getUserType());
        isrendered = false;
    } 
    return isrendered;
   }
    
}
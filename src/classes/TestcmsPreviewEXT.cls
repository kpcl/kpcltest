/*********************************************************************************
Class Name      : TestcmsPreviewEXT
Description     : Being used to testcmsPreview component on Opportuntiy
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
@isTest
public class TestcmsPreviewEXT {
    @isTest static void testComponentController(){
        Date closedDate = Date.newInstance(2018, 01, 09);
        Account account=new Account(name='test');
        insert account;
        //inserting opportunity and CMS Operations
        Opportunity op = new Opportunity(Name='test',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert op;
        RecordType rt = [SELECT Id,name FROM RecordType WHERE SObjectType='CMS_Operations__c' AND Name='Bagging' LIMIT 1];
        CMS_Operations__c cmsservice=new CMS_Operations__c();
        cmsservice.name='Test';
        cmsservice.Opportunity__c=op.Id;
        insert cmsservice;
        //Invoking method
        cmsPreviewEXT c=new cmsPreviewEXT();
        cmsPreviewEXT.getCMSOperations(op.Id);
    }
}
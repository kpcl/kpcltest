/*********************************************************************************
Class Name      : PopulateMailSenttime
Description     : Being used to populate mail sent time in RFQ
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
public class PopulateMailSenttime {
    
    @AuraEnabled
    public static String sendMail(string request){
        String templateName = 'EmailToSDPForQuote2(New)';
        RfQ__c rfq=new RFQ__c();
        List<String> ids=new List<String>();
        List<Id> vids=new List<Id>();
        List<String> primaryMailIds=new List<String>();
        List<String> secondaryMailIds=new List<String>();
        list<string> userid=new list<string>();
        try{    
            user profilename=[select Profile.name  from user where id=:UserInfo.getUserId()];
            profiles__c pfs=profiles__c.getOrgDefaults();
            if(pfs.KPCL_RFQ_User__c==profilename.Profile.name || pfs.KPCL_Admin__c==profilename.Profile.name ||  profilename.Profile.name=='system Administrator'){
                RfQ__c RfQOpp=[select RFQ_Service__r.id,Vendor_Account__c,Mail_Sent_to_Vendor__c from RfQ__c where id=:request];
                //getting RFQ respective vendor contacts from using RFQ id
                List<Contact> contacts = [Select Id,Email,Type__c From Contact Where AccountId = :RfQOpp.Vendor_Account__c and Email!=Null];
                if(contacts.size()>0){
                    RFQ_Service__c RFQserviceopp=[select Opportunity__r.ownerid from RFQ_Service__c where id=:RfQOpp.RFQ_Service__r.id];
                    //query opportunity owneremail to send mail to him in CC
                    list<OpportunityTeamMember> oppteammembers= [select UserId from OpportunityTeamMember where TeamMemberRole='RFQ Manager' AND OpportunityId=:RFQserviceopp.Opportunity__r.id];
                    for(OpportunityTeamMember Oppteam:oppteammembers){
                        userid.add(Oppteam.UserId);
                    }
                    list<string> HODS =Label.Payment_Approver.split(';');
                    list<user> users=[select email from user where id in :userid or id in :HODS];
                    for(user u:users){
                        secondaryMailIds.add(u.email);  
                    }
                    //adding salesforce mail id as CC ,so as to capture the reply 
                    secondaryMailIds.add(label.RFQ_Email_Service);
                    for(Contact con: contacts)
                    {
                        If(con.Email != NULL && con.Type__c == 'Primary'){
                            primaryMailIds.add(con.Email);
                        }
                        If(con.Email != NULL && con.Type__c == 'Secondary'){
                            secondaryMailIds.add(con.Email);
                        }
                    }       
                    List<Messaging.SingleEmailMessage>  myEmails = new List<Messaging.SingleEmailMessage>();
                    EmailTemplate template =  [SELECT Id, Name FROM EmailTemplate WHERE Name like :templateName LIMIT 1];
                    contact  c=[select id,email from contact limit 1];  
                    Messaging.SingleEmailMessage emails=new Messaging.SingleEmailMessage();            
                    emails.setTreatTargetObjectAsRecipient(false);        
                    emails.setTargetObjectId(c.id);            
                    emails.setWhatId(RfQOpp.id);
                    emails.setTemplateId(template.Id);   
                    emails.setToAddresses(primaryMailIds);            
                    emails.setccAddresses(secondaryMailIds);
                    Messaging.SendEmail(New Messaging.SingleEmailMessage[]{emails});  
                    //updating time to current mailsent time
                    DateTime Dt=datetime.now();        
                    RfQOpp.MailSenttime__c=Dt;
                    RfQOpp.Mail_Sent_to_Vendor__c=true;
                    update RfQOpp;       
                    return 'SUCCESS'; 
                }
                else{
                    return 'No contact with email found';
                }
            }
            else{
                return 'Insufficient priviliges for action';
            }
        }
        catch(Exception e){
            return 'FAILED';
        }
    }
}
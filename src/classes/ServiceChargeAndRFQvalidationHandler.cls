/*********************************************************************************
Class Name      : ServiceChargeAndRFQvalidationHandler
Description     : Validation on Margin field on service charge and atleast one rfq should be cretaed for each rfq service
Created By      : Naga Sai
Created Date    : 27-Feb-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                  27-Feb-2018         Designed as per client requirements.
*********************************************************************************/
public class ServiceChargeAndRFQvalidationHandler {
    //validationg margin on service charge
    public static void validatemarginonservicecharge(set<id> oppids,map<id,Opportunity> oppmaps){
        //querying servicecharges and checking for marginperunit fields empty or not
        list<Service_Charge__c> servicecharges=[select Margin_Per_Unit__c,RFQ__r.RFQ_Service__r.Opportunity__c from Service_Charge__c where RFQ__r.RFQ_Service__r.Opportunity__c in:oppids and RFQ__r.Status__c='Approved' order by Opportunity__c] ;
        for(Service_Charge__c servicecharge:servicecharges){
            if(servicecharge.Margin_Per_Unit__c==Null ) 
                oppmaps.get(servicecharge.RFQ__r.RFQ_Service__r.Opportunity__c ).adderror('Please enter margin in service charges for approved RFQs');
        }
    }
    public static void validateRFQsandServicechargesCount(set<id> oppids,map<id,Opportunity> oppmaps){
        Boolean NoRFQ=false;
        //querying for approved rfqs and checking if size is greater than zero 
        list<RFQ_Service__c> rfqservicelist=[select id,Opportunity__c,(select id from RFQs__r where status__c='Approved') from RFQ_Service__c where Opportunity__c in:oppids and Status__c!='Not in Scope'];
        list<RFQ__c> rfqlist=[select id,RFQ_Service__r.Opportunity__c,(select id from Service_Charges__r) from RFQ__c where RFQ_Service__r.Opportunity__c in:oppids and RFQ_Service__r.status__c!='Not in Scope' and status__c='Approved'];
        for(RFQ_Service__c rfqservice:rfqservicelist){
            system.debug('rfqservice.RFQs__r.size()'+rfqservice.RFQs__r.size());
            if(rfqservice.RFQs__r.size()==0){
                NoRFQ=true;
                oppmaps.get(rfqservice.Opportunity__c).adderror('Please create atleast one Approved RFQ for each RFQ service');
            }
        }
        system.debug('NoRFQ'+NoRFQ);
        if(NoRFQ==false){
            for(RFQ__c rfq:rfqlist){
                if(rfq.Service_Charges__r.size()==0)
                    oppmaps.get(rfq.RFQ_Service__r.Opportunity__c).adderror('Please create atleast one servicecharge for each RFQ');
            }
        }
    }
}
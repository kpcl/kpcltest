/*********************************************************************************
Class Name      : TestDisplayRFQsOnOpportunityController
Description     : Being used to test DisplayRFQsOnOpportunityController class
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Designed as per client requirements.
*********************************************************************************/
@isTest
public class TestDisplayRFQsOnOpportunityController {
    @isTest static void testgetRFQ(){
        //inserting opportunity,RFQService and RFQs 
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account account=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp;
        opp.StageName='RFQ';
        update Opp;
          list<RFQ_Service__c> service1 = [select id from RFQ_Service__c];
        //Creating RFQ Record for the RFQ service
        list<Rfq__c> rfqlist=new list<RFQ__c>();
        for(RFQ_Service__c rfqservice:service1){
            Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=rfqservice.id,Amount_Paid__c=50, Status__c='Approved');
            rfqlist.add(RFQ1);
        }
        insert rfqlist;
        //craeting service charge for each RFQ
        list<Service_Charge__c> servicechargelist=new  list<Service_Charge__c>();
        for(RFQ__C rfq:rfqlist){
            Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
            servicechargelist.add(Charge1);
        }
        insert servicechargelist;

        Opp.StageName='Proposal';
        update Opp;
        Opp.StageName='Negotiation';
        update Opp;
        Opp.StageName='CMS';
        update Opp;
        RFQ_Service__c rfqservice=new RFQ_Service__c();
        rfqservice.name='CHA';
        rfqservice.Opportunity__c=opp.id;
        insert rfqservice;
        RFQ__c rfq=new RFQ__c();
        rfq.RFQ_Service__c=rfqservice.id;
        rfq.Vendor_Account__c=account.id;
        rfq.Status__c='Approved';
        insert rfq;
        //Invoking method
        DisplayRFQsOnOpportunityController.getRFQ(opp.Id);
    }
}
/*********************************************************************************
Class Name      : TestPopulateMailReceivedTime
Description     : Being used to test populatemailreceivedtime class in RFQ
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
@isTest
public class TestPopulateMailReceivedTime {
    @isTest static void TestInboundHandler(){
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and name='vendor'];
        Account account=new Account(name='test',Recordtypeid=RecType.id);
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        //inserting Opportunity,RFQService,RFQ
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp;
        Contact con1=new Contact(LastName='Smith',Email='meghamanu06@gmail.com',AccountId=account.id,Type__c='Primary');
        insert con1;
        Contact con2=new Contact(LastName='Smith',Email='nagasai.chalamalasetti@absyz.com',AccountId=account.id,Type__c='Secondary');
        insert con2;
        RFQ_Service__c rfqservice=new RFQ_Service__c(Name='CHA',Opportunity__c=opp.id);
        insert rfqservice;
        RFQ__c rfq=new RFQ__c(Vendor_Account__c=account.id,RFQ_Service__c=rfqservice.id);
        insert rfq;
        RFQ__c InsertedRFQ=[select name from RFQ__C where id=:rfq.id];
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        // setup the data for the email
        email.subject = 'VishwaSamudra - Request for Quote for {!RFQ__c.RFQ_Service__c} service. Enquiry reference number - {!RFQ__c.Reference_Number__c}. RFQ number - '+InsertedRFQ.Name+'.';
        email.fromname = 'FirstName LastName';
        email.fromAddress='nagasai.chalamalasetti@absyz.com';
        PopulateMailReceivedTime p=new PopulateMailReceivedTime();
        p.handleInboundEmail(email, env);
        RFQ__c TimeUpdatedRFQ=[select MailReceivedTime__c from RFQ__C where id=:rfq.id];
    }
    @isTest static void TestException(){
        Account account=new Account(name='test');
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        //not inserting contact so that it throughs exception
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp;
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        PopulateMailReceivedTime p=new PopulateMailReceivedTime();
        p.handleInboundEmail(email, env);
    }
}
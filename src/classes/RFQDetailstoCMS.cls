/*********************************************************************************
Class Name      : SubmitRFQPaymentsForApproval
Description     : Being used by a visualforceComponent RFQDetailstoCMS 
Created By      : Megha Manu
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Megha Manu                jan-2017            Design as per Client
*********************************************************************************/
public class RFQDetailstoCMS {
    public string Opportunitid{get;set;}
    public boolean RFQsSize{get;set;}
 public List<RFQ_Service__c> RFQY=new list<RFQ_Service__c>();
    public  Opportunity opp=new Opportunity();
    public List<RFQ_Service__c> getRFQs()
    { 
        system.debug('Opportunitid'+Opportunitid);
  RFQY = [SELECT id,Venodorlist__c,Services__c,Name FROM RFQ_Service__c WHERE Opportunity__c =: Opportunitid];
      system.debug('RFQY'+RFQY);
       
 return RFQY;       
    }  
   }
/*********************************************************************************
Class Name      : OpportunityTimeLine
Description     : Used To Create Opportunity Time Line Records when Ever the Opporutnity stage is changed or created. 
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Designed as per client requirements.
*********************************************************************************/
public class OpportunityTimeLine {
    //this method will be called when Opportunity is inserted
    public static void firstRecord(List<Opportunity> op)
    {
     List<OpportunityTimeLine__c> OpportunityTimeLineList = new List<OpportunityTimeLine__c>();
     for(Opportunity x:op)
     {
         OpportunityTimeLine__c OpportunityTimeLineRecord = new OpportunityTimeLine__c(); 
         OpportunityTimeLineRecord.FromTime__c=x.CreatedDate;
         OpportunityTimeLineRecord.FromStage__c=x.StageName;
         OpportunityTimeLineRecord.Opportunity__c=x.Id;
         OpportunityTimeLineList.add(OpportunityTimeLineRecord);
     }
     List<Database.SaveResult> result = Database.insert(OpportunityTimeLineList, false);
    }
    //this method will be invoked when ever Opportunity stage is updated
    public static void otherRecord(Opportunity old,Opportunity newop)
    {
        List<OpportunityTimeLine__c> OpportunityTimeLineRecordtoInsert = new List<OpportunityTimeLine__c>();
        //getting the existing records in OpportunityTimeLine
        List<OpportunityTimeLine__c> TimeLineRecords = [select id,FromStage__c,ToStage__c,FromTime__c,ToTime__c,Opportunity__c from OpportunityTimeLine__c where Opportunity__c=:old.Id ];
        //new OpportunityTimeLine Record to insert
        OpportunityTimeLine__c newRecord = new OpportunityTimeLine__c();
        if(TimeLineRecords.size()>0){
            for(OpportunityTimeLine__c x:TimeLineRecords){
                //if the ToStage is null and to Time is null in existing records
                if((x.ToStage__c==NULL || x.ToStage__c=='') && x.ToTime__c==NULL)
                {
                    //adding current stage to Exisitng record ToStage
                    x.ToStage__c=newop.StageName;
                    x.ToTime__c=DateTime.Now();
                    //add current stage to New Record From Stage
                    newRecord.FromStage__c=newop.StageName;
                    newRecord.FromTime__c=DateTime.now();
                    newRecord.Opportunity__c=old.Id;
                    OpportunityTimeLineRecordtoInsert.add(newRecord);
                }
            }
            //upsert the exisitng time line records
           	system.debug('***TimeLineRecords:'+TimeLineRecords);
            List<Database.UpsertResult> UpsertResultList = Database.upsert(TimeLineRecords, false);
            if(OpportunityTimeLineRecordtoInsert.size()>0)
            {
                system.debug('***OpportunityTimeLineRecordtoInsert:'+OpportunityTimeLineRecordtoInsert);
                //inserting the newly created Opportunity time line records
                List<Database.SaveResult> ResultListForNewRecord = Database.insert(OpportunityTimeLineRecordtoInsert, false);
            
            }
        }
        
        
    }
    
    
}
/*********************************************************************************
Class Name      : OpportunityTimeLineTest
Description     : Used To Test OpportunityTimeLine 
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Design as per Client
*********************************************************************************/
@isTest
public class OpportunityTimeLineTest {
@isTest static void checkFirstRecordInsertAndThenUpdate()
{
    //Creating a new Instance of DateObject for Opportunity CloseDate Field (*mandatory field)
    Account a = new Account(Name='TestAccount'); 
    insert a;
    Date closedDate = Date.newInstance(2018, 01, 09);
    Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate);
    insert op;
    //Creating an Expected OpportunityTimeLine Record 
    OpportunityTimeLine__c ExpectedHiphopRecord = new OpportunityTimeLine__c(Opportunity__c=op.Id,FromStage__c=op.StageName);
   	//Querying the OpportunityTimeLine__c record created which is inserted after Opportunity is created 
    List<OpportunityTimeLine__c> actualRec = [select Opportunity__c,FromStage__c,ToStage__c from OpportunityTimeLine__c where Opportunity__c=:op.Id];
    //checking Expected and actual Record for Opportunity Id Equivalence
    System.assertEquals(ExpectedHiphopRecord.Opportunity__c,actualRec[0].Opportunity__c);
    //checking Expected and actual Record for FromStage Equivalence
    System.assertEquals(ExpectedHiphopRecord.FromStage__c,actualRec[0].FromStage__c);
    System.assertEquals('RFQ',actualRec[0].ToStage__c);
    //changing the stage of Opportunity 
    op.StageName='Proposal';
    //update opportunity to trigger after update and create related records
    update op;
    actualRec = [select Opportunity__c,FromStage__c,ToStage__c from OpportunityTimeLine__c where Opportunity__c=:op.Id];
    //Creating an Expected OpportunityTimeLine Record 
    OpportunityTimeLine__c updatedExpectedHiphopRecord = new OpportunityTimeLine__c(Opportunity__c=op.Id,FromStage__c='RFQ',ToStage__c='Proposal');
    //checking Expected and actual Record for Opportunity Id Equivalence
    System.assertEquals(updatedExpectedHiphopRecord.Opportunity__c,actualRec[1].Opportunity__c);
    //checking Expected and actual Record for FromStage Equivalence
    System.assertEquals(updatedExpectedHiphopRecord.FromStage__c,actualRec[1].FromStage__c);
    //checking Expected and actual Record for TOStage Equivalence
    System.assertEquals(updatedExpectedHiphopRecord.ToStage__c,actualRec[1].ToStage__c);
   
    //further checking 
    op.StageName='Negotiation';
    //update opportunity to trigger after update and create related records
    update op;
    actualRec = [select Opportunity__c,FromStage__c,ToStage__c from OpportunityTimeLine__c where Opportunity__c=:op.Id];
    //Creating an Expected OpportunityTimeLine Record 
    updatedExpectedHiphopRecord = new OpportunityTimeLine__c(Opportunity__c=op.Id,FromStage__c='Proposal',ToStage__c='Negotiation');
    //checking Expected and actual Record for Opportunity Id Equivalence
    System.assertEquals(updatedExpectedHiphopRecord.Opportunity__c,actualRec[2].Opportunity__c);
    //checking Expected and actual Record for FromStage Equivalence
    System.assertEquals(updatedExpectedHiphopRecord.FromStage__c,actualRec[2].FromStage__c);
    //checking Expected and actual Record for TOStage Equivalence
    System.assertEquals(updatedExpectedHiphopRecord.ToStage__c,actualRec[2].ToStage__c);
    
   
}

    
    
    
}
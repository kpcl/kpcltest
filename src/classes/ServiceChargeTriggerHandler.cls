/*********************************************************************************
Class Name      : ServiceChargeTriggerHandler
Description     : Handler class for ServiceChargeTrigger in Service charge Object to implement Roll up Summary function in a lookup Relation between ServiceCharge and (RFQ,Opportunity)
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Designed as per client requirements.
*********************************************************************************/
public class ServiceChargeTriggerHandler {
    
    public static void sumupValues(set<id> ServiceChargeId,List<Service_Charge__c> ServiceChargeRecords)
    {
        try{
            String orgCurr = UserInfo.getDefaultCurrency();
            System.debug(orgCurr);
            String fromCurreny=orgCurr;
            //get the list of RFQ Id's  
            set<id> RFQId = new set<id>();
            for(Service_Charge__c s:ServiceChargeRecords)
            {
                RFQId.add(s.RFQ__c); 
            }
            //get All Rfq Records whose id is on RFQID list 
            List<RFQ__c> RFQRecords = [select id,CurrencyIsoCode,convertCurrency(Total_Amount_Payable__c),convertCurrency(Total_Margin__c),convertCurrency(Total_Taxable_Value__c),convertCurrency(Total_Receivable__c),convertCurrency(Total_Quote_Amount__c),convertCurrency(Total_Vendor_Quote__c)   from RFQ__c where id=:RFQId];
            Map<id,RFQ__c> RFQRecordMap = new Map<id,RFQ__c>();
            //create a map with Rfq <id,RFQ Records>
            for(RFQ__c r:RFQRecords)
            {
                r.Total_Vendor_Quote__c=0;
                r.Total_Quote_Amount__c=0;
                r.Total_Receivable__c=0;
                r.Total_Taxable_Value__c=0;
                r.Total_Margin__c=0;
                r.Total_Amount_Payable__c=0;
                
                RFQRecordMap.put(r.id,r);   
            }
            //get Aggrerate Result
            AggregateResult[] groupedResults = [select sum(Balance_Payable__c)BalancePay,sum(Total_Margin2__c)Margin,sum(Taxable_Value__c)TaxableValue,sum(Balance_Receivable__c)BalanceReceive,sum(Final_Amount__c)FinalAmount,RFQ__c,sum(Total_Vendor_Amount__c)QuoteAmount,CurrencyIsoCode from Service_Charge__c  where RFQ__c in:RFQId group by RFQ__c,CurrencyIsoCode];
            if(groupedResults.size()>0){
                for(AggregateResult a:groupedResults)
                {
                    if(RFQRecordMap.containsKey((String)a.get('RFQ__c')))
                    {
                        //MultiCurrencyUtil.convertCurrency(fromIsoCode, toIsoCode, child.Unit_Cost__c);
                        //MultiCurrencyUtil.convertCurrency(a.get('CurrencyIsoCode'), RFQRecordMap.get((String)a.get('RFQ__c')).CurrencyIsoCode, (Decimal)a.get('BalancePay'));
                        
                        RFQRecordMap.get((String)a.get('RFQ__c')).Total_Amount_Payable__c+= (Decimal)a.get('BalancePay');
                        RFQRecordMap.get((String)a.get('RFQ__c')).Total_Margin__c+= (Decimal)a.get('Margin');
                        RFQRecordMap.get((String)a.get('RFQ__c')).Total_Taxable_Value__c+=(Decimal)a.get('TaxableValue');
                        RFQRecordMap.get((String)a.get('RFQ__c')).Total_Receivable__c+= (Decimal)a.get('BalanceReceive');
                        RFQRecordMap.get((String)a.get('RFQ__c')).Total_Quote_Amount__c+=(Decimal)a.get('FinalAmount');
                        RFQRecordMap.get((String)a.get('RFQ__c')).Total_Vendor_Quote__c+= (Decimal)a.get('QuoteAmount');
                    }
                }
                Database.update(RFQRecordMap.values(), false);
            }
            //if there is no service charge records if all are deleted
            else
            {
                for(RFQ__c r:RFQRecords)
                {
                    r.Total_Amount_Payable__c=0;
                    r.Total_Margin__c=0;
                    r.Total_Taxable_Value__c=0;
                    r.Total_Receivable__c=0;
                    r.Total_Quote_Amount__c=0;
                    r.Total_Vendor_Quote__c=0;
                }
                Database.update(RFQRecords,false);
                
            }
            //only for Testing Purpose
            if(Test.isRunningTest())
            {
                /*Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5);
insert charge1;*/
            } 
        }catch(Exception e)
        {
            system.debug('ErrorMessage'+e.getMessage());
        }
    }
    //Service charges related to Opportunity with Additional Service charge Layout
    public static void OpportunitySumUp(set<id> ServiceChargeId,List<Service_Charge__c> ServiceChargeRecords)
    {
        try{
            String orgCurr = UserInfo.getDefaultCurrency();
            System.debug(orgCurr);
            //get list of Opportunity Id's
            set<id> OppID = new set<id>();
            for(Service_Charge__c s:ServiceChargeRecords)
            {
                OppID.add(s.Opportunity__c); 
            }
            //get Related Opportunity Records
            List<Opportunity> OppRecords = [select id,Additional_Finance_Charges__c,Receivables_from_Additional_Charges__c from Opportunity where id=:OppID];
            Map<id,Opportunity> OppRecordMap = new Map<id,Opportunity>();
            //Create Map of Opportunity id and Records
            for(Opportunity r:OppRecords)
            {
                r.Additional_Finance_Charges__c=0;
                r.Receivables_from_Additional_Charges__c=0;
                OppRecordMap.put(r.id,r);   
            }
            //get Aggregate Result
            AggregateResult[] groupedResults = [select sum(Final_Amount__c)AddionalInvoice,sum(Balance_Receivable__c)AdditionalReceivable,Opportunity__c,CurrencyIsoCode  from Service_Charge__c  where Opportunity__c in:OppID group by Opportunity__c,CurrencyIsoCode];
            if(groupedResults.size()>0){
                for(AggregateResult a:groupedResults)
                {
                    if(OppRecordMap.containsKey((String)a.get('Opportunity__c')))
                    {
                        String fromCurreny=orgCurr;
                        //MultiCurrencyUtil.convertCurrency(fromCurreny, RFQRecordMap.get((String)a.get('RFQ__c')).CurrencyIsoCode, (Decimal)a.get('QuoteAmount'));
                        OppRecordMap.get((String)a.get('Opportunity__c')).Additional_Finance_Charges__c+=(Decimal)a.get('AddionalInvoice');
                        OppRecordMap.get((String)a.get('Opportunity__c')).Receivables_from_Additional_Charges__c+=(Decimal)a.get('AdditionalReceivable');
                    }
                }
                Database.update(OppRecordMap.values(), false);
            }
            //when there is no service charge record for Opportunity , if all records are deleted
            else
            {
                for(Opportunity r:OppRecords)
                {
                    r.Additional_Finance_Charges__c=0;
                    r.Receivables_from_Additional_Charges__c=0;
                }
                Database.update(OppRecords,false);
                
            }//for testing purpose
            if(Test.isRunningTest())
            {
                Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5);
                insert Charge1;
            }
        }catch(Exception e)
        {
            system.debug('ErrorMessage'+e.getMessage());
        }
        
        
    }
    public static void UpdateTotalMargin(list<Service_Charge__c> ServiceChargelist){
        
        system.debug('before updateservicechargelist'+servicechargelist);   
        for(Service_Charge__c sc:servicechargelist) {
            //updating  margin field depending upon project cargo
            if(sc.Asessable_Value_Applicable__c==true){
                if(sc.Margin_Per_Unit__c !=Null && sc.Assessable_Value__c!=Null)
                    sc.Total_Margin2__c	=sc.Margin_Per_Unit__c * sc.Assessable_Value__c;
                else
                    sc.Total_Margin2__c	=0;
                if(sc.Rate_Per_Unit__c !=Null && sc.Assessable_Value__c!=Null)
                    sc.Total_Amount__c=sc.Rate_Per_Unit__c*sc.Assessable_Value__c;
                else
                    sc.Total_Amount__c	=0;
                
                //system.debug(' sc.Total_Margin__c'+ sc.Total_Margin__c);
            }
            else{
                if(sc.Currency__c =='USD'||sc.Currency__c =='Others'){
                    if(sc.Margin_Per_Unit__c !=Null && sc.Quantity__c!=Null)
                        sc.Total_Margin2__c	=sc.Margin_Per_Unit__c * sc.Quantity__c * sc.conversion_rate__c ;
                    else
                        sc.Total_Margin2__c	=0;   
                    if(sc.Rate_Per_Unit__c !=Null && sc.Quantity__c!=Null)
                        sc.Total_Amount__c=sc.Rate_Per_Unit__c*sc.Quantity__c*sc.conversion_rate__c;
                    else
                        sc.Total_Amount__c	=0;
                }
                else{
                    if(sc.Margin_Per_Unit__c !=Null && sc.Quantity__c!=Null)
                        sc.Total_Margin2__c	=sc.Margin_Per_Unit__c * sc.Quantity__c ;
                    else
                        sc.Total_Margin2__c=0;
                    if(sc.Rate_Per_Unit__c !=Null && sc.Quantity__c!=Null)
                        sc.Total_Amount__c=sc.Rate_Per_Unit__c*sc.Quantity__c;
                    else
                        sc.Total_Amount__c	=0;
                }
            }
            // ending of updating  margin field depending upon project cargo
            
        }
        
    }
    
}
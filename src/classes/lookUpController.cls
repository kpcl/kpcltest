public class lookUpController {
   @AuraEnabled
 public static List < User > fetchAccount(String searchKeyWord) {
  String searchKey = searchKeyWord + '%';
  List < User > returnList = new List < User > ();
  List < User > lstOfAccount = [select id, Name from User where Name LIKE: searchKey];
 
  for (User acc: lstOfAccount) {
     returnList.add(acc);
     }
  return returnList;
 }
}
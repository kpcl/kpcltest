@isTest
public class TestdisplayVendorContactsController {
    @isTest static void testgetVondorContacts(){
          list<RecordType> RecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account account=new Account(name='test',recordtypeid=RecordTypeList[0].id);
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp;
        Contact con=new Contact(LastName='Smith',Email='meghamanu06@gmail.com',AccountId=account.id);
        insert con;
        RFQ_Service__c rfqservice=new RFQ_Service__c(Name='CHA',Opportunity__c=opp.id);
        insert rfqservice;
        RFQ__c rfq1=new RFQ__c(Vendor_Account__c=account.id,RFQ_Service__c=rfqservice.id);
        insert rfq1;
        displayVendorContactsController.getVondorContacts(rfq1.id);
    }
}
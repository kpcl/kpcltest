public class Opportunity_Target_Handler {
    public static void update_Target_Records(Opportunity newRecords)
    {
        System.debug('Target Records - Opportunity User Owner'+newRecords.OwnerId);
        System.debug('Target Records - Opportunity close Date'+newRecords.CloseDate);
        System.debug('Target Records - Opportunity volume Teu'+newRecords.Volume_TEUs__c);
        System.debug('Target Records - Opportunity Total AMount Receivable'+newRecords.Total_Amount_Receivable__c);
        System.debug('Target Records - Opportunity Type'+newRecords.Shipping_Type__c);
        DescribeSObjectResult describeResult = Target__c.getSObjectType().getDescribe();
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );	
        Id ownerID = newRecords.OwnerId;
        String query =' SELECT ' +String.join( fieldNames, ',' ) +' FROM '+describeResult.getName() +' WHERE ' +' User__c=:ownerID';
        
        List<Target__c> Target_Record = Database.query( query );
        
        
        //List<Target__c> Target_Record = [select id,User__c,Start_Date__c,End_Date__c,Revenue_of_Container_Actual__c, Number_of_TEUs_Actual__c from Target__c where User__c=:newRecords.OwnerId];
        system.debug('Target Records -'+Target_Record);
        Date cDate = newRecords.CloseDate;
        Integer OpportunityCloseMonth = cDate.month();
        Integer OpportunityCloseYear = cDate.year();
        System.debug('Target Records - Opportunity Month'+cDate.month());
        System.debug('Target Records - Opportunity Year'+cDate.year());
        Integer TargetMonth =0;
        Integer TargetYear = 0;
        DateTime sDate;
        List<Target__c> Target_Records_To_Update = new List<Target__c>();
        for(Target__c c:Target_Record){
            sDate =c.Start_Date__c;
            TargetMonth=sDate.month();
            TargetYear=sDate.year();
            if(OpportunityCloseMonth==TargetMonth && OpportunityCloseYear==TargetYear)
            {
                if(newRecords.Shipping_Type__c=='Container'){  
                    SYstem.debug('COntainer one');
                    c.Number_of_TEUs_Actual__c=(c.Number_of_TEUs_Actual__c==null)? newRecords.Volume_TEUs__c:c.Number_of_TEUs_Actual__c+newRecords.Volume_TEUs__c;
                    c.Revenue_of_Container_Actual__c=(c.Revenue_of_Container_Actual__c==null)? newRecords.Total_Amount_Receivable__c:c.Revenue_of_Container_Actual__c+newRecords.Total_Amount_Receivable__c;
                    
                }
                else
                {
                     SYstem.debug('bulks one');
                    String commodity=newRecords.Cargo__c;
                    commodity=commodity.toLowerCase();
                    String ResultCommodity='';
                    boolean flag=false;
                    System.debug('The commodity is '+commodity);
                    List<String> allCommodity=Related_Target_Fields_Class.ListOfAllCommodity();
                    for(String s:allCommodity)
                    {
                        if(commodity.containsIgnoreCase(s))
                        {
                            ResultCommodity=s;
                            flag=true;
                            break;
                        }
                        if(StringDistance.isDistanceOneOrTwo(s.toLowerCase(),commodity))
                        {
                            ResultCommodity=s;
                            flag=true;
                            break;
                        }
                    }
                    if(flag!=true)
                    {
                       ResultCommodity='Others'; 
                    }
                    ResultCommodity=ResultCommodity.replaceAll(' ','_');
                    Decimal MetricTon = newRecords.Net_Weight__c;
                    Decimal Revenue = newRecords.Total_Amount_Receivable__c;
                   
                   	
                    c.put(ResultCommodity+'_Number_of_Metric_Ton_Actual__c',(c.get(ResultCommodity+'_Number_of_Metric_Ton_Actual__c')==null)? MetricTon:((Decimal)c.get(ResultCommodity+'_Number_of_Metric_Ton_Actual__c')+MetricTon));
                    c.put(ResultCommodity+'_Revenue_of_Bulk_Actual__c',(c.get(ResultCommodity+'_Revenue_of_Bulk_Actual__c')==null)? Revenue:((Decimal)c.get(ResultCommodity+'_Revenue_of_Bulk_Actual__c')+Revenue));
                    
                }
               Target_Records_To_Update.add(c); 
            }
            
        }
        if(Target_Records_To_Update.size()>0){
      update Target_Records_To_Update;  
        }
    }
    public static void update_Bulk_Related_Fields(String UserEnteredCommodity,List<String> AllCommodity,Target__c TargetRecord)
    {
        
    }
}
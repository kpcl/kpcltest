/*********************************************************************************
Class Name      : PopulateMailReceivedtime
Description     : Being used to populate mail received time in RFQ
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
global class PopulateMailReceivedtime implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        try{
            Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
            string service;
            //fetching subject and address from the email received
            string FromAddress=email.fromAddress;
            system.debug(email.subject);
            string body=email.subject;  
            //taking out reference number from subject
            string refnumber=  body.substringAfterLast('-');
            string referencenumber='RFQ-'+refnumber.substringBefore('.');
            RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and name='vendor'];
            system.debug('FromAddress'+FromAddress+'Rectype.id'+Rectype.id);
            //querying for respective vendor account id through the email address
            Contact v=[select id,Accountid from Contact where email=:FromAddress and Account.RecordTypeId=:Rectype.id];          
            //querying respective rfq using reference number and vendoraccount id
            RFQ__C r=[select MailReceivedTime__c,name,Total_Number_Of_Mails_During_Negotiation__c from RFQ__C where Vendor_Account__c=:v.Accountid  and name=:referencenumber];           
            if(r!=null){
                DateTime Dt=datetime.now();
                Time t=Dt.time();
                system.debug(r.name);
                //updating first responce
                if(r.MailReceivedTime__c==null){
                    r.MailReceivedTime__c=Dt;
                }
            }
            return result;
        }
        catch(Exception e){
            return null; 
        }
    }
}
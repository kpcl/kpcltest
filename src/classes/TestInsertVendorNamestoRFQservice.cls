/*********************************************************************************
Class Name      : TestInsertVendorNamestoRFQservice
Description     : Being used to test InsertVendorNamestoRFQservice handler and trigger
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
@isTest
public class TestInsertVendorNamestoRFQservice {
    @isTest static void testInsertVendornames(){
        Date closedDate = Date.newInstance(2018, 01, 09);
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account account=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert account;
        Opportunity op = new Opportunity(Name='test',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert op;
        //update op;
        RFQ_Service__c rfqservice=new RFQ_Service__c();
        rfqservice.name='CHA';
        rfqservice.Opportunity__c=op.id;
        insert rfqservice;
        RFQ__c rfq=new RFQ__c();
        rfq.RFQ_Service__c=rfqservice.id;
        rfq.Vendor_Account__c=account.id;
        insert rfq;
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,RFQ__c=RFQ.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
        insert Charge1;
        rfq.Status__c='Approved';
        update rfq;
        Charge1.Margin_Per_Unit__c=9;
        update charge1;
        RFQ_Service__c insertedrfqservice=[select VenodorList__c from RFQ_Service__c where id=:rfqservice.id];
        System.assertEquals('test', insertedrfqservice.VenodorList__c);
        delete rfq;
    }
    
}
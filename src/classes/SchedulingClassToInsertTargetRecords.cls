global class SchedulingClassToInsertTargetRecords implements Schedulable {
    global void execute(SchedulableContext ctx) {
        InsertTargetRecords myBatchObject = new InsertTargetRecords(); 
Id batchId = Database.executeBatch(myBatchObject);
    }
}
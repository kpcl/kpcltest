@istest
public class timesheet_Handler_Test {
@istest
   public static TimeSheet__c recordGeneration() {
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='test9677@test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='test9677@test.com');
        insert u;
       Date myDate = Date.newInstance(2018, 2, 19);
       List< Employee_ID_Dept_Name__mdt  > EmployeeDetails = [select name__c,id__c,Department__c  from Employee_ID_Dept_Name__mdt ];
		TimeSheet__c TimeSheet = new TimeSheet__c();
       if(EmployeeDetails.size()>0)
    {
        TimeSheet = new TimeSheet__c(Employee_Names__c=EmployeeDetails[0].name__c,SHIFT__c='9:00 AM - 06:00 PM',Present_Absent__c='P',Total_Hours__c=5, Date__c=myDate);  
    }
    else
    {
        TimeSheet = new TimeSheet__c(Employee_Name__c=u.id,SHIFT__c='9:00 AM - 06:00 PM',Present_Absent__c='P',Total_Hours__c=5, Date__c=myDate);
    }
       
    insert TimeSheet;
    return TimeSheet;
   }
    @istest
   public static void checkCreationOfTimeSheetEntry() {
       	TimeSheet__c TimeSheet = timesheet_Handler_Test.recordGeneration();
       	List<Time_Sheet_Entry__c> EntryList = [select name,id from Time_Sheet_Entry__c where TimeSheet__c=:TimeSheet.id];
       List< Task_For_Employee_TimeSheet__mdt > Task = [select Task__c from Task_For_Employee_TimeSheet__mdt];
       if(Task.size()>0)
       {
           system.assertEquals(Task[0].Task__c,EntryList[0].name);
       }
    
   }@istest
   public static void checkDepartmentAndID() {
       	TimeSheet__c t = timesheet_Handler_Test.recordGeneration();
       TimeSheet__c TimeSheet=[select id,Departments__c,Employee_ID__c,Employee_Names__c from TimeSheet__c where id=:t.id];
       system.debug('new one '+TimeSheet);
       	List< Employee_ID_Dept_Name__mdt  > EmployeeDetails = [select name__c,id__c,Department__c  from Employee_ID_Dept_Name__mdt where name__c=:TimeSheet.Employee_Names__c];
    	system.debug(EmployeeDetails);
       if(EmployeeDetails.size()>0)
        {
			system.assertEquals(EmployeeDetails[0].id__c, TimeSheet.Employee_ID__c);
			system.assertEquals(EmployeeDetails[0].Department__c, TimeSheet.Departments__c);            
        }
   }
    
    
}
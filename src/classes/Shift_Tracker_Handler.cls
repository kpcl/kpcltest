public class Shift_Tracker_Handler {
public static void autoFill_data(List<SHIFT_Tracker__c> ShiftTrackerRecords )
{
    List< Employee_ID_Dept_Name__mdt  > EmployeeDetails = [select name__c,id__c,Department__c  from Employee_ID_Dept_Name__mdt ];
    Map<String,Employee_ID_Dept_Name__mdt> EmployeeMap = new Map<String,Employee_ID_Dept_Name__mdt>();
    for(Employee_ID_Dept_Name__mdt rec:EmployeeDetails)
    {
        EmployeeMap.put(rec.name__c,rec);
    }
	List< SHIFT_time_and_Name__mdt> ShiftDetails = [select Timing__c,Shift_Name__c,Shift__c   from SHIFT_time_and_Name__mdt ];
    Map<String,SHIFT_time_and_Name__mdt> ShiftMap = new Map<String,SHIFT_time_and_Name__mdt>();
    for(SHIFT_time_and_Name__mdt rec:ShiftDetails)
    {
        ShiftMap.put(rec.Shift__c,rec);
    }

    
    for(SHIFT_Tracker__c shift : ShiftTrackerRecords)
    {
     	if(EmployeeMap.containsKey(shift.Employee_Name__c))
        {
            Employee_ID_Dept_Name__mdt emp = EmployeeMap.get(shift.Employee_Name__c);
            if(emp.id__c!=null)
            {
                shift.Employee_ID__c=emp.id__c;
            }
            if(emp.Department__c!=null)
            {
                shift.Departments__c=emp.Department__c;
            }
                
        }
        if(ShiftMap.containsKey(shift.SHIFT__c))
        {
            SHIFT_time_and_Name__mdt shiftRec = ShiftMap.get(shift.SHIFT__c);
            if(shiftRec.Shift_Name__c!=null)
            {
                shift.SHIFT_Name__c=shiftRec.Shift_Name__c;
            }
            if(shiftRec.Timing__c!=null)
            {
                shift.SHIFT_Timing__c=shiftRec.Timing__c;
            }
            
        }
    
    
	}
}
}
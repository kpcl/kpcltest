@istest
public class Test_Shift_Tracker_Handler {
@istest
   public static SHIFT_Tracker__c recordGeneration() {
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='test9677@test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='test9677@test.com');
        insert u;
       Date myDate = Date.newInstance(2018, 3, 19);
       List< Employee_ID_Dept_Name__mdt  > EmployeeDetails = [select name__c,id__c,Department__c  from Employee_ID_Dept_Name__mdt ];
		SHIFT_Tracker__c shift = new SHIFT_Tracker__c();
       if(EmployeeDetails.size()>0)
    {
         shift = new SHIFT_Tracker__c(Employee_Name__c=EmployeeDetails[0].name__c,Date__c=myDate);  
    }
    else
    {
         shift = new SHIFT_Tracker__c(Employee_Name__c='Dileep ',Date__c=myDate);
    }
    List< SHIFT_time_and_Name__mdt> ShiftDetails = [select Timing__c,Shift_Name__c,Shift__c   from SHIFT_time_and_Name__mdt ];
    if(ShiftDetails.size()>0)
    {
        shift.SHIFT__c=ShiftDetails[0].Shift__c;
    }
       else{
           shift.SHIFT__c='D';
       }
       
       
    insert shift;
    return shift;
   }
    @istest
   public static void checkautoPopulatedValue() {
       	SHIFT_Tracker__c shiftrec = Test_Shift_Tracker_Handler.recordGeneration();
       SHIFT_Tracker__c shift=[select id,SHIFT_Name__c,Departments__c,Employee_Name__c,Employee_ID__c,SHIFT__c,SHIFT_Timing__c from SHIFT_Tracker__c where id=:shiftrec.id];
       	List< Employee_ID_Dept_Name__mdt  > EmployeeDetails = [select name__c,id__c,Department__c  from Employee_ID_Dept_Name__mdt where name__c=:shift.Employee_Name__c];
    	system.debug(EmployeeDetails);
       if(EmployeeDetails.size()>0)
        {
			system.assertEquals(EmployeeDetails[0].id__c, shift.Employee_ID__c);
			system.assertEquals(EmployeeDetails[0].Department__c, shift.Departments__c);            
        }
       List< SHIFT_time_and_Name__mdt> ShiftDetails = [select Timing__c,Shift_Name__c,Shift__c   from SHIFT_time_and_Name__mdt where Shift__c=:shift.SHIFT__c];
    	if(ShiftDetails.size()>0)
        {
			system.assertEquals(ShiftDetails[0].Timing__c, shift.SHIFT_Timing__c );
			system.assertEquals(ShiftDetails[0].Shift_Name__c, shift.SHIFT_Name__c);            
        }
   }
    
    
}
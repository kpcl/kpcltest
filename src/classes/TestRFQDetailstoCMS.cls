@isTest
public class TestRFQDetailstoCMS {
    @isTest static void testgetRFQserviceTable(){
        Date closedDate = Date.newInstance(2018, 01, 09);
        Account account=new Account(name='test');
        insert account;
        //inserting Opportunity,RFQservice,RFQs and Service charges
        Opportunity op = new Opportunity(Name='testt',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert op;      
        RFQ_Service__c rfqservice=new RFQ_Service__c();
        rfqservice.name='CHAsd';
        rfqservice.Opportunity__c=op.id;
        insert rfqservice;
        RFQDetailstoCMS r=new RFQDetailstoCMS();
        r.Opportunitid=op.id;
        r.getRFQs();
    }
}
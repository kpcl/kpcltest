/*********************************************************************************
Class Name      : RRUserAssignmentOnOpportunity
Description     : Used to assign Opportunity owner and assign corresponding team member to
				  the Opportunity
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018        Designed as per client requirement.
*********************************************************************************/
public class RRUserAssignmentOnOpportunity {
    Public static boolean firstrun=true;
    //Give the name of Custom MetaData Type to this funciton it returns a list of User object in the custom Member List
    public static List<User> getList_Of_User_From_This_Custom_MetaData(String objectName)
    {
        String Query='select id,userName__c from '+objectName;
        System.debug('UserAssignment ****** Handler ');
        List<String> ListOfUserName = new List<String>();
        //Creating a Dynamic Object Type with Custom Meta Data Name
        String listType = 'List<' + objectName + '>';
        String EachRecordType =  ''+objectName+'';
        //Creating a generic Sobject to hold data of Custom MetaData
        List<SObject> MembersInATeam = (List<SObject>)Type.forName(listType).newInstance();
        SObject IndividualMember = (SObject)Type.forName(EachRecordType).newInstance();
        //Execute the Query to get and store the data in Generic object
        MembersInATeam = Database.query(Query);
        List<User> UserList = new List<User>();
        if(MembersInATeam.size()>0){
            //iterate over the generic object
            for(Integer i=0;i<MembersInATeam.size();i++)
            {
                IndividualMember = MembersInATeam[i];
                ListOfUserName.add(String.valueOf(IndividualMember.get('userName__c')));
            }
        }
        System.debug('List of Username in apex '+ListOfUserName);
        if(ListOfUserName.size()>0)
        {
            //query User object with the list of username
            UserList = [select id,name,Username from User where Username=:ListOfUserName];
        }
        System.debug('UserList in get List method'+UserList);
        return UserList;
    }
    public static void UserAssignmentOnStageChange(Opportunity newOpportunity)
    {
        System.debug('Current Opportunity:'+newOpportunity);
        List<Opportunity_Stage_Mapping__mdt>Mapping = [select Stage_Name__c, CustomMetadataName__c from Opportunity_Stage_Mapping__mdt where Stage_Name__c=:newOpportunity.StageName];
        
        if(newOpportunity.StageName=='Negotiation' || newOpportunity.StageName=='Proposal')
        {
            List<Opportunity_Task__mdt> OpportunityTask = [select id,Subject__c,StageName__c,Due_Date__c,Description__c from Opportunity_Task__mdt where StageName__c=:newOpportunity.StageName];
            if(OpportunityTask.size()>0){
                Task task_Assigned_To_Team_Member = new Task(Ownerid=newOpportunity.OwnerId,IsReminderSet=true,Subject=OpportunityTask[0].Subject__c+' '+newOpportunity.Name, Description=OpportunityTask[0].Description__c, ActivityDate=date.today()+OpportunityTask[0].Due_Date__c.intValue(),ReminderDateTime=date.today()+OpportunityTask[0].Due_Date__c.intValue()-1);
                
                insert task_Assigned_To_Team_Member;
            }
            
        }
        
        //Irrespective of RR logic assign a user to Opportunity if its in CMS Stage
        if(newOpportunity.StageName=='CMS')
        {
            List<User>  UserList=RRUserAssignmentOnOpportunity.getList_Of_User_From_This_Custom_MetaData('StandardMemberOnCMSStage__mdt');
            if(UserList.size()>0)
            {
                //Assign on the first user to it
                OpportunityTeamMember  teamMember = new OpportunityTeamMember(OpportunityAccessLevel='Edit',UserId=UserList[0].id,OpportunityId=newOpportunity.Id,TeamMemberRole='CHA Manager'); 
                insert teamMember;
                List<Opportunity_Task__mdt> OpportunityTask = [select id,Subject__c,StageName__c,Due_Date__c,Description__c from Opportunity_Task__mdt where StageName__c=:newOpportunity.StageName];
                if(OpportunityTask.size()>0){
                    Task task_Assigned_To_Team_Member = new Task(Ownerid=teamMember.UserId,IsReminderSet=true,Subject=OpportunityTask[0].Subject__c+' '+newOpportunity.Name, Description=OpportunityTask[0].Description__c, ActivityDate=date.today()+OpportunityTask[0].Due_Date__c.intValue(),ReminderDateTime=date.today()+OpportunityTask[0].Due_Date__c.intValue()-1);
                    insert task_Assigned_To_Team_Member;
                }
            }    
        }
        //if a Opportunity stage and Custom Meta Data member object Mapping Exists 
        if(Mapping.size()>0)
        {
            List<User> UserList=RRUserAssignmentOnOpportunity.getList_Of_User_From_This_Custom_MetaData(Mapping[0].CustomMetadataName__c);
            System.debug('USerList in Apex update'+UserList);
            if(UserList.size()>0)
            {
                System.debug('Opportunity OwnerId  before'+newOpportunity.OwnerId+'For Opportunity'+newOpportunity.Name);
                System.debug('RRMODInput'+newOpportunity.RRModInput__c);
                //RRLogic
                Integer ResultRemainder = math.mod(integer.valueof(newOpportunity.RRModInput__c),UserList.size());
                String TeamMemberRoleName='';
                //If a Opportunity and memberstage Mapping Exists
                List<OpportunityStageAndTeamMemberRole__mdt> MemberRole = [select TeamMemberRole__c , StageName__c  from OpportunityStageAndTeamMemberRole__mdt where StageName__c=:newOpportunity.StageName];
                if(MemberRole.size()>0)
                {
                    TeamMemberRoleName=MemberRole[0].TeamMemberRole__c ;
                    System.debug('Team ROle'+TeamMemberRoleName);
                    //create Opportunity Team Member
                    OpportunityTeamMember  teamMember = new OpportunityTeamMember(OpportunityAccessLevel='Edit',UserId=UserList[ResultRemainder].id,OpportunityId=newOpportunity.Id,TeamMemberRole=TeamMemberRoleName); 
                    System.debug('Opportunity Team Member : '+teamMember);
                    insert teamMember;
                    List<Opportunity_Task__mdt> OpportunityTask = [select id,Subject__c,StageName__c,Due_Date__c,Description__c from Opportunity_Task__mdt where StageName__c=:newOpportunity.StageName];
                    if(OpportunityTask.size()>0){
                        Task task_Assigned_To_Team_Member = new Task(Ownerid=teamMember.UserId,IsReminderSet=true,Subject=OpportunityTask[0].Subject__c+' '+newOpportunity.Name, Description=OpportunityTask[0].Description__c, ActivityDate=date.today()+OpportunityTask[0].Due_Date__c.intValue(),ReminderDateTime=date.today()+OpportunityTask[0].Due_Date__c.intValue()-1);
                        if(firstrun==true){
                        insert task_Assigned_To_Team_Member;
                            firstrun=false;
                        }
                    }
                }
            }
        }   
    }
    public static void ChangeStageToRFQ(Opportunity newop)
    {
        Opportunity newOpportunity = [select id,RRModInput__c,OwnerId,name,Shipping_Type__c,Shipment_Number__c,StageName,Shipping_Options__c,Cargo__c from Opportunity where id=:newop.Id];
        newOpportunity.StageName='RFQ';
          string shipment;
         if(newOpportunity.Shipment_Number__c!=Null)
         shipment = 'Shipment '+newOpportunity.Shipment_Number__c;
        else
         shipment= '';
        newOpportunity.Name= newOpportunity.Name+shipment+'-'+newOpportunity.Shipping_Type__c + newOpportunity.Shipping_Options__c + newOpportunity.Cargo__c;
        update newOpportunity;
    }
    public static void Add_Task_To_Finance_Member_on_Storage_Date_Change(Opportunity opp)
    {
        if(opp.Storage_Free_Period__c==NULL)
        {
            opp.Storage_Free_Period__c=0;
        }
        List<OpportunityTeamMember> FinanceteamMember =[select id,name,UserId,TeamMemberRole from opportunityteammember where OpportunityId=:opp.Id and TeamMemberRole like '%finance%'];
        if(FinanceteamMember.size()>0)
        {
            List<Opportunity_Task__mdt> OpportunityTask = [select id,Subject__c,StageName__c,Due_Date__c,Description__c from Opportunity_Task__mdt where StageName__c='Storage Task'];
            if(OpportunityTask.size()>0){
            Date StorageDate = date.newinstance(opp.Storage_Calculation_Start_Date__c.year(),opp.Storage_Calculation_Start_Date__c.month(),opp.Storage_Calculation_Start_Date__c.day());
               List<Task> task_To_Be_Assigned = new List<Task>(); 
                for(OpportunityTeamMember fm :FinanceteamMember){
                  Task task_Assigned_To_Team_Member = new Task(Ownerid=fm.UserId,IsReminderSet=true,Subject=OpportunityTask[0].Subject__c+' '+opp.Name,Description=OpportunityTask[0].Description__c, ActivityDate=StorageDate+opp.Storage_Free_Period__c.intValue(),ReminderDateTime=StorageDate+opp.Storage_Free_Period__c.intValue()-2);  
               	task_To_Be_Assigned.add(task_Assigned_To_Team_Member);
                }
            insert task_To_Be_Assigned;
            }
        }
        
    }
    public static void If_Stage_Changes_From_Nego_Or_Proposal_To_RFQ_Create_Task(Opportunity opp)
    {
        List<OpportunityTeamMember> RFQTeamMember =[select id,name,UserId,TeamMemberRole from opportunityteammember where OpportunityId=:opp.Id and TeamMemberRole like '%rfq%'];
		  if(RFQTeamMember.size()>0)
        {
            List<Opportunity_Task__mdt> OpportunityTask = [select id,Subject__c,StageName__c,Due_Date__c,Description__c from Opportunity_Task__mdt where StageName__c='RFQ'];
            if(OpportunityTask.size()>0){
           // Date StorageDate = date.newinstance(opp.Storage_Calculation_Start_Date__c.year(),opp.Storage_Calculation_Start_Date__c.month(),opp.Storage_Calculation_Start_Date__c.day());
               List<Task> task_To_Be_Assigned = new List<Task>(); 
                for(OpportunityTeamMember fm :RFQTeamMember){
                  Task task_Assigned_To_Team_Member = new Task(Ownerid=fm.UserId,IsReminderSet=true,Subject=OpportunityTask[0].Subject__c+' '+opp.Name, Description=OpportunityTask[0].Description__c, ActivityDate=date.today()+OpportunityTask[0].Due_Date__c.intValue(),ReminderDateTime=date.today()+OpportunityTask[0].Due_Date__c.intValue()-1);  
               	task_To_Be_Assigned.add(task_Assigned_To_Team_Member);
                }
            insert task_To_Be_Assigned;
            }
        }
      }
/*
//On Opportunity Insert Assign Opportunity Owner
public static void UserAssignmentOnRecordInsert(Opportunity newop)
{	Opportunity newOpportunity = [select id,RRModInput__c,OwnerId,name,StageName from Opportunity where id=:newop.Id];
System.debug('Current Opportunity:'+newOpportunity);
List<Opportunity_Stage_Mapping__mdt>Mapping = [select Stage_Name__c, CustomMetadataName__c from Opportunity_Stage_Mapping__mdt where Stage_Name__c=:newOpportunity.StageName];
//if a Opportunity stage and Custom Meta Data member object Mapping Exists 
if(Mapping.size()>0)
{
List<User> UserList=RRUserAssignmentOnOpportunity.getList_Of_User_From_This_Custom_MetaData(Mapping[0].CustomMetadataName__c);
System.debug('USerList on Insert'+UserList);
if(UserList.size()>0){
System.debug('Opportunity OwnerId  before'+newOpportunity.OwnerId+'For Opportunity'+newOpportunity.Name);
//RR logic 
Integer ResultRemainder = math.mod(integer.valueof(newOpportunity.RRModInput__c),UserList.size());
newOpportunity.OwnerId=UserList[ResultRemainder].id;
System.debug('Opportunity OwnerId  After'+newOpportunity.OwnerId);
}
}
update newOpportunity;
}*/

}
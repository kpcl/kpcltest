/*********************************************************************************
Class Name      : CreationOfRFQAndCMSServicesHandler
Description     : Being used to automate creation of RFQ and CMS records
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                Jan-2017            Designed as per client requirements.
*********************************************************************************/
public class CreationOfRFQAndCMSServicesHandler {
    public static boolean firstRun = true;
    public static boolean SecondRun = true;  
    //default value assigning to SDS
    public  string stuffinganddestuffing='SDS';
    //method to create RFQ services
    public void createRFQService(list<Opportunity> opportunitylist){
        try{
            List<RFQ_Service__c> RFQList=new List<RFQ_Service__c>();
            for(Opportunity opp : opportunitylist) {
                //getting scope of services and iterating over it while creating RFQ record and assigning service as name to record
                String[] ScopeofServices=opp.Scope_Of_Services_for_Container__c.split(';');  
                if(ScopeofServices!=null){
                    for(String service:ScopeofServices)
                    {
                        RFQ_Service__c RFQService=new RFQ_Service__c();
                        RFQService.name=Service;
                        RFQService.Opportunity__c=opp.id;
                        RFQList.add(RFQService);                
                    }      
                }
            }  
            //To prevent the trigger to run multiple times we are  using firstrun variable
            if(firstRun == true){ 
                Database.insert(RFQList, false);
                firstRun=false;
            }
        }
        Catch(Exception e){
            system.debug('error message'+e.getMessage());
        }
    }
    public void createCMSOperations(list<Opportunity> opportunitylist,set<id> opportunityids){ 
        try{
            //querying field from shipping type records to decide which CMS records should be created
            list<Shipping_Type__c> Shippingtypes=[select CFS__c,CHA__c,Stuffing_Destuffing__c,ICD__c,Jetty__c,KPCL__c,KPCT__c,Logistics__c,Marine__c,Ocean_Freight__c,SDS__c,Stevedoring__c,Transportation__c,Vessel_Chartering__c,Warehousing__c,Bagging__c from Shipping_Type__c order by name];
            //querying RFQService records to get the vendor name list from it
            list<RFQ_Service__c> rfqservices=new list<RFQ_Service__c>();
            rfqservices=[select id,name,VenodorList__c,Opportunity__c from RFQ_Service__c where Opportunity__c in:opportunityids order by name];
            //mappping vendornames and opportunity ids started
            map<id,Opportunity> oppmap=new  map<id,Opportunity>();
            //we need to query again cause we cannot perform operation on trigger.new
            list<Opportunity> opplist =new list<Opportunity>();
            opplist=[select id,Vendor_List__c from Opportunity where id in:opportunityids];
            for(opportunity o:opplist){
                oppmap.put(o.id, o);
            }
            for(RFQ_Service__c rfqservice:rfqservices){
                //getting the vendor list of each Opportunity if anything is there and appending to it the new names
                string ExistedVendorNames= oppmap.get(rfqservice.Opportunity__c).Vendor_List__c;
                system.debug('oppvendors'+ExistedVendorNames);
                //updating the Vendor_List__c field of Opportunity records in the map 
                if(rfqservice.VenodorList__c!=null){
                    //the format of storing vendor names is "Servicename1-vendorname1,vendorname2,..;servicename2-vendorname1,vendorname2,..;....."
                    if(ExistedVendorNames!=null){
                        oppmap.get(rfqservice.Opportunity__c).Vendor_List__c=ExistedVendorNames+';'+rfqservice.name+'-'+rfqservice.VenodorList__c+';';                
                    }
                    else{
                        oppmap.get(rfqservice.Opportunity__c).Vendor_List__c=rfqservice.name+'-'+rfqservice.VenodorList__c; 
                    } 
                }
            }
            //mappping vendornames and opportunity ids compleetd
            //iterating over all the opportunities which invoked the trigger and creating CMS records
            list<CMS_Operations__c> ListOfCMSRecords=new list<CMS_Operations__c>();
            for(Opportunity opp:opportunitylist){
                //storing scopeofservices in services variable
                string ServicesList=opp.Scope_Of_Services_for_Container__c;
                if(ServicesList==Null)
                    ServicesList='None';
                //depending upon stuffing method the name of cms record varies SDS or ICD or CFS only for service type SDS,so storing the name in a variable
                if(opp.Stuffing_Method__c=='Port' || opp.De_Stuffing_Method__c=='Port'){
                    stuffinganddestuffing='SDS';
                }
                if(opp.Stuffing_Method__c=='ICD' || opp.De_Stuffing_Method__c=='ICD'){
                    stuffinganddestuffing='ICD';
                }
                if(opp.Stuffing_Method__c=='CFS'){
                    stuffinganddestuffing='CFS';
                }
                //Creating Email record irrespective of anyting
                CMS_Operations__c Email=new CMS_Operations__c();
                Email.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Email Tracking').getRecordTypeId();
                Email.Opportunity__c=opp.id;
                Email.name='Email Tracking';
                ListOfCMSRecords.add(Email); 
                CMS_Operations__c ChallengesFaced=new CMS_Operations__c();
                ChallengesFaced.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Challenges Faced').getRecordTypeId();
                ChallengesFaced.Opportunity__c=opp.id;
                ChallengesFaced.name='Challenges Faced';
                ListOfCMSRecords.add(ChallengesFaced); 
                CMS_Operations__c SDPFeedback=new CMS_Operations__c();
                SDPFeedback.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('SDP Feedback').getRecordTypeId();
                SDPFeedback.Opportunity__c=opp.id;
                SDPFeedback.name='Feedback';
                ListOfCMSRecords.add(SDPFeedback); 
                //depending upon stuffing method the name of cms record varies SDS or ICD or CFS only for service type SDS,so storing the name in a variable completed
                //depending upon shipping type  of opportuntiy we will consider which shipping type object record fields should be checked and will create only those records which fields are true
                if(opp.Shipping_Type__c=='Bulk' || opp.Shipping_Type__c=='Break Bulk'){
                    if(Shippingtypes[0].Vessel_Chartering__c==true){
                        CMS_Operations__c VesselChartering=new CMS_Operations__c();
                        VesselChartering.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Vessel Chartering').getRecordTypeId();
                        VesselChartering.Opportunity__c=opp.id;
                        VesselChartering.name='Vessel Chartering';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k;
                        if(s!=Null){
                            k=s.substringBetween('Vessel Chartering-', ';');
                        }
                        if(k!=null){
                            VesselChartering.Vendor_List__c=k;   
                        }
                        if(ServicesList.contains('Vessel Chartering'))
                            VesselChartering.In_Scope__c=true;
                        ListOfCMSRecords.add(VesselChartering);                       
                    }
                    if(Shippingtypes[0].Stevedoring__c==true){
                        CMS_Operations__c Stevedoring=new CMS_Operations__c();
                        Stevedoring.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Stevedoring').getRecordTypeId();
                        Stevedoring.Opportunity__c=opp.id;
                        Stevedoring.name='Stevedoring';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k;
                        if(s!=Null){
                            k=s.substringBetween('Stevedoring-', ';');
                        }
                        if(k!=null){
                            Stevedoring.Vendor_List__c=k;   
                        }
                        if(ServicesList.contains('Stevedoring'))
                            Stevedoring.In_Scope__c=true;
                        ListOfCMSRecords.add(Stevedoring);                         
                    }
                    if(Shippingtypes[0].Transportation__c==true){
                        CMS_Operations__c Transportation=new CMS_Operations__c();
                        Transportation.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Transportation for Bulk').getRecordTypeId();
                        Transportation.Opportunity__c=opp.id;
                        Transportation.name='Transportation for Bulk';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k='';
                        if(s!=Null){
                            if(s.contains('Transportation by Road-'))
                                k='Transportation by Road-'+s.substringBetween('Transportation by Road-', ';');
                            if(s.contains('Transportation by Rake-'))
                                k=k +'Transportation by Rake-'+s.substringBetween('Transportation by Rake-', ';');
                            if(s.contains('Transportation by Conveyor-'))
                                k=k+'Transportation by Conveyor-'+s.substringBetween('Transportation by Conveyor-', ';');
                        }
                        if(k!=null  && k!=''){
                            Transportation.Vendor_List__c=k;   
                        }
                        if(ServicesList.contains('Transportation'))
                            Transportation.In_Scope__c=true;
                        system.debug('Transportation.In_Scope__c'+Transportation.In_Scope__c);
                        ListOfCMSRecords.add(Transportation); 
                    }
                    if(Shippingtypes[0].CHA__c==true){
                        CMS_Operations__c CHAbulk=new CMS_Operations__c();
                        CHAbulk.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('CHA for Bulk').getRecordTypeId();
                        CHAbulk.Opportunity__c=opp.id;
                        CHAbulk.name='CHA for Bulk';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k;
                        if(s!=Null){
                            k=s.substringBetween('CHA-', ';');
                        }
                        if(k!=null){
                            CHAbulk.Vendor_List__c=k;   
                        }
                        if(ServicesList.contains('CHA'))
                            CHAbulk.In_Scope__c=true;
                        ListOfCMSRecords.add(CHAbulk); 
                    }
                    if(Shippingtypes[0].Logistics__c==true){
                        CMS_Operations__c Logistics=new CMS_Operations__c();
                        Logistics.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Logistics & Storage').getRecordTypeId();
                        Logistics.Opportunity__c=opp.id;
                        Logistics.name='Logistics & Storage';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k;
                        if(s!=Null){
                            k =s.substringBetween('Logistics-', ';');
                        }
                        if(k!=null){
                            Logistics.Vendor_List__c=k;   
                        }
                        if(ServicesList.contains('Logistics') || ServicesList.contains('Storage'))
                            Logistics.In_Scope__c=true;
                        ListOfCMSRecords.add(Logistics);                       
                    }
                    if(Shippingtypes[0].Bagging__c==true){
                        CMS_Operations__c Bagging=new CMS_Operations__c();
                        Bagging.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Bagging').getRecordTypeId();
                        Bagging.Opportunity__c=opp.id;
                        Bagging.name='Bagging';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        if(s!=null){
                            string k=s.substringBetween('Bagging-', ';');
                            if(k!=null){
                                Bagging.Vendor_List__c=k;   
                            }
                        }
                        if(ServicesList.contains('Bagging'))
                            Bagging.In_Scope__c=true;
                        ListOfCMSRecords.add(Bagging);                         
                    }
                    if(Shippingtypes[0].KPCL__c==true){
                        CMS_Operations__c KPCL=new CMS_Operations__c();
                        KPCL.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('KPCL').getRecordTypeId();
                        KPCL.Opportunity__c=opp.id;
                        KPCL.name='KPCL';
                        ListOfCMSRecords.add(KPCL);                         
                    }
                    if(Shippingtypes[0].KPCT__c==true){
                        CMS_Operations__c KPCT=new CMS_Operations__c();
                        KPCT.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('KPCT').getRecordTypeId();
                        KPCT.Opportunity__c=opp.id;
                        KPCT.name='KPCT';
                        ListOfCMSRecords.add(KPCT);                         
                    }
                }
                if(opp.Shipping_Type__c=='Container'){
                    if(Shippingtypes[1].Ocean_Freight__c==true){
                        system.debug('Ocean_Freight__c');
                        CMS_Operations__c OceanFreight=new CMS_Operations__c();
                        OceanFreight.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Ocean Freight').getRecordTypeId();
                        OceanFreight.Opportunity__c=opp.id;
                        OceanFreight.name='Ocean Freight';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k;
                        if(s!=Null){
                            k=s.substringBetween('Ocean Freight-', ';');
                        }
                        if(k!=null){
                            OceanFreight.Vendor_List__c=k;   
                        }
                        
                        if(ServicesList.contains('Ocean Freight'))
                            OceanFreight.In_Scope__c=true;
                        ListOfCMSRecords.add(OceanFreight); 
                    }
                    if(Shippingtypes[1].Transportation__c==true){
                        CMS_Operations__c Transportation=new CMS_Operations__c();
                        Transportation.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Transportation for Container').getRecordTypeId();
                        Transportation.Opportunity__c=opp.id;
                        Transportation.name='Transportation for Container';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k='';
                        if(s!=Null){
                            if(s.contains('Transportation by Road-'))
                                k ='Transportation by Road-'+s.substringBetween('Transportation by Road-', ';');
                            if(s.contains('Transportation by Rake-'))
                                k=k+'Transportation by Rake-'+s.substringBetween('Transportation by Rake-', ';');
                            system.debug('s'+s);
                            system.debug('k'+k);                    
                            if(k!=null && k!=''){
                                Transportation.Vendor_List__c=k;   
                            }
                        }
                        if(ServicesList.contains('Transportation by Road') || ServicesList.contains('Transportation by Rake'))
                            Transportation.In_Scope__c=true;
                        ListOfCMSRecords.add(Transportation); 
                    }
                    if(Shippingtypes[1].CHA__c==true){
                        CMS_Operations__c CHAcontainer=new CMS_Operations__c();
                        CHAcontainer.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('CHA for Container').getRecordTypeId();
                        CHAcontainer.Opportunity__c=opp.id;
                        CHAcontainer.name='CHA for Container';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k;
                        if(s!=Null){
                            k=s.substringBetween('CHA-', ';');
                        }
                        if(k!=null){
                            CHAcontainer.Vendor_List__c=k;   
                        }
                        
                        if(ServicesList.contains('CHA'))
                            CHAcontainer.In_Scope__c=true;
                        ListOfCMSRecords.add(CHAcontainer); 
                    }
                    if(Shippingtypes[1].Stuffing_Destuffing__c==true){
                        CMS_Operations__c stuffingdestuffing=new CMS_Operations__c();
                        if(stuffinganddestuffing=='ICD')
                            stuffingdestuffing.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('ICD').getRecordTypeId();
                        else
                            stuffingdestuffing.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Stuffing & Destuffing').getRecordTypeId();
                        stuffingdestuffing.Opportunity__c=opp.id;
                        stuffingdestuffing.name=stuffinganddestuffing;
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k; 
                        if(s!=Null){
                            k =s.substringBetween('SDS-', ';');
                        }
                        if(k!=null){
                            stuffingdestuffing.Vendor_List__c=k;   
                        }
                        if(ServicesList.contains('SDS'))
                            stuffingdestuffing.In_Scope__c=true;
                        ListOfCMSRecords.add(stuffingdestuffing); 
                    }
                    if(Shippingtypes[1].Bagging__c==true){
                        CMS_Operations__c Bagging=new CMS_Operations__c();
                        Bagging.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('Bagging').getRecordTypeId();
                        Bagging.Opportunity__c=opp.id;
                        Bagging.name='Bagging';
                        string s= oppmap.get(opp.id).Vendor_List__c;
                        string k;
                        if(s!=Null){
                            k=s.substringBetween('Bagging-', ';');
                        }
                        if(k!=null){
                            Bagging.Vendor_List__c=k;   
                        }
                        if(ServicesList.contains('Bagging'))
                            Bagging.In_Scope__c=true;
                        ListOfCMSRecords.add(Bagging); 
                    }
                    if(Shippingtypes[1].KPCL__c==true){
                        CMS_Operations__c KPCL=new CMS_Operations__c();
                        KPCL.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('KPCL').getRecordTypeId();
                        KPCL.Opportunity__c=opp.id;
                        KPCL.name='KPCL';
                        ListOfCMSRecords.add(KPCL); 
                    }
                    if(Shippingtypes[1].KPCT__c==true){
                        CMS_Operations__c KPCT=new CMS_Operations__c();
                        KPCT.RecordTypeId = Schema.SObjectType.CMS_Operations__c.getRecordTypeInfosByName().get('KPCT').getRecordTypeId();
                        KPCT.Opportunity__c=opp.id;
                        KPCT.name='Port';
                        ListOfCMSRecords.add(KPCT); 
                    }        
                }
            }
            system.debug('ListOfCMSRecords'+ListOfCMSRecords);
            //iterating over all the opportunities which invoked the trigger and creating CMS records completed
            
            if(SecondRun==true){
                // Database.insert(ListOfCMSRecords, false);
                insert ListOfCMSRecords;
                SecondRun=false;
            }   
        }
        catch(Exception e){
            system.debug('error message'+e.getMessage());
        }
    }
}
/*********************************************************************************
Class Name      : OpportunityTeamMemberMailController
Description     : Being used by OpportunityTeamMemberMailComponent to fetch opportunity details and opprtuntiyteammber details
Created By      : Naga sai
Created Date    : 26-Feb-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga sai                 26-Feb-2017           Design as per Client
*********************************************************************************/
public class OpportunityTeamMemberMailController {
    public string Opportunitteamid{get;set;}
    public  list<OpportunityTeamMember> oppteam;
    //method to query opportunity team member details
    public string getusername(){
        oppteam=new list<OpportunityTeamMember>();
        //querying userid ,so using that we can get the user name  of oppteammember
        oppteam= [select UserId,OpportunityId from OpportunityTeamMember where id=:Opportunitteamid];
        list<user> u=new  list<user>();
        if(oppteam.size()>0)
            u=[select name from user where id=:oppteam[0].UserId];
        if(u.size()>0)
            return u[0].name;
        else 
            return null;
    }
   //method to query opportunity details
    public Opportunity getOpportunity(){
        list<Opportunity>  Opportunity=new list<Opportunity>();
        if(oppteam.size()>0)
            Opportunity=   [select ReferenceNumber1__c,Name,Cargo__c,Shipping_Options__c,Shipping_Type__c,POL__c,POD__c,Scope_Of_Services_for_Container__c from Opportunity where id=:oppteam[0].OpportunityId];
        if(Opportunity.size()>0)
            return Opportunity[0];
        else
            return null;
    }    
}
@isTest
public class Test_Opportunity_Target_Handler {
@isTest static Opportunity ContainerRecordGeneration()
{
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='test9677@test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='test9677@test.com');
        insert u;
       
    //Creating a new Instance of DateObject for Opportunity CloseDate Field (*mandatory field)
    Account a = new Account(Name='TestAccount'); 
    insert a;
    Date closedDate = Date.newInstance(2018, 01, 09);
    Opportunity op = new Opportunity(OwnerId=u.id,Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate, Number_Of_40_type_Container__c=5, Number_Of_20_type_Container__c=5, Receivables_from_Additional_Charges__c=500,Shipping_Type__c='Container',Container_SubType__c='Dry', Per_TEU_Weight_in_MT__c=5);
    insert op;
    DateTime startDate = DateTime.newInstance(2018,01,01, 5, 2, 4);
    DateTime endDate = DateTime.newInstance(2018,01,31, 5, 2, 4);
    Target__c target = new Target__c(User__c=op.OwnerId,Start_Date__c=startDate,End_Date__c=endDate);
    insert target;
   	 startDate = DateTime.newInstance(2018,02,01, 5, 2, 4);
     endDate = DateTime.newInstance(2018,02,28, 5, 2, 4);
    target = new Target__c(User__c=op.OwnerId,Start_Date__c=startDate,End_Date__c=endDate);
   	insert target;
    op.StageName='Closed Won';
    update op;
    
    return op;
}

}
public class TimeSheet_Handler {
public static void Create_TimeSheet_Entry(TimeSheet__c NewRecord)
{
    if(NewRecord.Present_Absent__c=='P'){
    List<Time_Sheet_Entry__c> EntryList = new List<Time_Sheet_Entry__c>();
    List< Task_For_Employee_TimeSheet__mdt > Task = [select Task__c from Task_For_Employee_TimeSheet__mdt];
    for(Task_For_Employee_TimeSheet__mdt eachTask:Task)
    {
        Time_Sheet_Entry__c NewTimeSheetEntry = new Time_Sheet_Entry__c(name=''+eachTask.Task__c,TimeSheet__c=NewRecord.id);
        EntryList.add(NewTimeSheetEntry);
    }
    if(EntryList.size()>0)
    {
    insert EntryList;
    }
    }
}
public static void autoFill_id_department_by_name(List<TimeSheet__c> TimesheetRecords)
{
    List< Employee_ID_Dept_Name__mdt  > EmployeeDetails = [select name__c,id__c,Department__c  from Employee_ID_Dept_Name__mdt ];
    Map<String,Employee_ID_Dept_Name__mdt> EmployeeMap = new Map<String,Employee_ID_Dept_Name__mdt>();
    for(Employee_ID_Dept_Name__mdt rec:EmployeeDetails)
    {
        EmployeeMap.put(rec.name__c,rec);
    }
    for(TimeSheet__c timeSheet : TimesheetRecords)
    {
     	if(EmployeeMap.containsKey(timeSheet.Employee_Names__c))
        {
            Employee_ID_Dept_Name__mdt emp = EmployeeMap.get(timeSheet.Employee_Names__c);
            if(emp.id__c!=null)
            {
                timeSheet.Employee_ID__c=emp.id__c;
            }
            if(emp.Department__c!=null)
            {
                timeSheet.Departments__c=emp.Department__c;
            }
                
        }
        system.debug('In Trigger dept'+timeSheet.Departments__c);
        system.debug('In Trigger dept'+timeSheet.Employee_ID__c);
    }
    
}
}
/*********************************************************************************
Class Name      : SubmitRFQPaymentsForApproval
Description     : Being used by inbound email. To Capture the response and to update payment records with Acknowledgement mail.
Created By      : Madhur Behl
Created Date    : JAN-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Madhur Behl                 JAN-2018           Design as per Client
*********************************************************************************/
global class RFQPaymentApprovalEXT implements Messaging.InboundEmailHandler {
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Messaging.reserveSingleEmailCapacity(2);
        string comments = ''; // To Hold the comments
        string status = ''; // To Hold the status
        string rawStatus = '';  // To Hold Extarcted rawStatus
        string urlstr = ''+System.URL.getSalesforceBaseUrl().toExternalForm(); // To get the org Base URL
        string textBody = email.plainTextBody; // To Hold Email Text body
        String ReferalNumber = textBody.substringBetween('Reference Number: *','*');  // To getReference Number
        String PaymentNumber = textBody.substringBetween('Payment Numbers: *','*'); // To Payment Numbers
        List<string> paymnetList = PaymentNumber.split(',');  // To get payment numbers List
        String[] resultList = email.plainTextBody.split('  ', 2); 
        string statuscontent = resultList[0];
        urlstr = urlstr.substring(0,urlstr.indexOf('.'));
        // Getting Comments
        if(statuscontent.contains('comment') || statuscontent.contains('Comment')){
          comments = statuscontent.split(':',2)[1];
          if(comments.contains(';')){
            comments = comments.substring(0,comments.indexOf(';'));
        }
        else{
          comments = '--NONE-- ';
        }
          comments = comments.trim();
          rawStatus = statuscontent.split(':',2)[0];
        }
        else{
          rawStatus = statuscontent;
        }
        // To capture the response Approve/Reject
        if(rawStatus.contains('approve') || rawStatus.contains('Approve')){
          status = 'Approved';
        }
        if(rawStatus.contains('rej') || rawStatus.contains('Rej')){
          status = 'Rejected';
        }
        // Getting payment records and Updating them
        List<Payment__c> paymentList = new List<Payment__c>();
        for(Payment__c pay : [select Name, Submitted__c, Approved__c, Rejected__c, id, RFQ__c, RFQ__r.Due__c, RFQ__r.Balance_Amount__c, RFQ__r.RFQ_Service__c, RFQ__r.RFQ_Service__r.Name,
                              RFQ__r.Name, RFQ__r.RFQ_Service__r.Opportunity__c,RFQ__r.Amount_Paid__c, RFQ__r.RFQ_Service__r.Opportunity__r.Name, RFQ__r.RFQ_Service__r.Opportunity__r.Reference_Number__c  from Payment__c where Submitted__c = true and RFQ__r.RFQ_Service__r.Opportunity__r.Reference_Number__c =: ReferalNumber and RFQ__r.Status__c = 'Approved' and Name In : paymnetList]){
          pay.Submitted__c = false;
          if(status == 'Approved'){
            pay.Approved__c = true;
            pay.Rejected__c = false; 
          }
          if(status == 'Rejected')
            pay.Rejected__c = true; 
            paymentList.add(Pay);   
        }
        // Getting Opportunity and updating comments
        Opportunity opp = [select Name,id,comments__c,owner.Name,owner.Email, Reference_Number__c from Opportunity where Reference_Number__c =: ReferalNumber];
        if(!string.isEmpty(comments)){        
          if(string.isEmpty(opp.comments__c))
            opp.comments__c = '';
          opp.comments__c = Datetime.Now()+'-'+comments+'; '+opp.comments__c;
          database.update(opp, false);
        }
        // To sent Acknowledgement Email to Opportunity Owner
        if(!string.isEmpty(opp.owner.Email)){ 
          String[] toAddresses = new String[]{};           
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
          mail.setSenderDisplayName('Salesforce Support');
          mail.setBccSender(false);
          toAddresses.add(opp.owner.Email);
          mail.setToAddresses(toAddresses);
          mail.setUseSignature(false);
          mail.setSubject( Opp.Name +'- Vendor Payment Approval Response' );
          string body = '<html><body>Hi <b>' + opp.owner.Name + '</b>,<br/><br/> This mail is regarding Vendor Payment approval response for the <br/>Opportunity: <b>' + '<a href='+urlstr +'.lightning.force.com/one/one.app#/sObject/'+opp.Id+'/view>'+opp.Name+'</a>' + '</b><br/>Referal Number: <b>' + opp.Reference_Number__c + '</b><br/>Status: <b>' + status + '</b><br/>Payment Numbers: <b>' + PaymentNumber + '</b><br/>Comments: <b>' + comments + '</b><br/><br/>Thank You!!</body></html>';
          mail.setHtmlBody(body);
          Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        database.saveresult[] srList = Database.update(paymentList, false);
        return result;
      }
  }
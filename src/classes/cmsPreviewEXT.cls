/*********************************************************************************
Class Name      : cmsPreviewEXT
Description     : Being used by cmsPreviewEXT Lightning Component for previewing CMS Records respective to opportunity.
Created By      : Madhur Behl
Created Date    : Dec-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Madhur Behl                 Dec-2017            Design as per Client
*********************************************************************************/
public class cmsPreviewEXT{
    
    public cmsPreviewEXT(){ }

    @AuraEnabled
    public static List<CMS_Operations__c> getCMSOperations(Id OpportunityId){
        
        return [select Id,Name, Status__c, Sub_Status__c,Status_Description__c,RecordTypeId,CHA_TAT_Days__c,Overall_TAT__c,SDS_TAT_Days__c,Ocean_Freight_TAT__c from CMS_Operations__c where Opportunity__c =: OpportunityId];
    }
}
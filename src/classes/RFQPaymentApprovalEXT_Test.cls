/*********************************************************************************
Class Name      : RFQPaymentApprovalEXT_Test
Description     : Used TO Test RFQPaymentApprovalEXT
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Design as per Client
*********************************************************************************/
@isTest
public class RFQPaymentApprovalEXT_Test {
    @isTest static Opportunity TestDataGenerator()
    {
        // Create a new email, envelope object and Attachment
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='test9677@test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='test9677@test.com');
        insert u;
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account a=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert a;
        //Creating Date Instance for Closed Date (* required Field in Opportunity)
        Date closedDate = Date.newInstance(2018, 01, 09);
        //Creating Opportunity Record
        Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate,OwnerId=u.Id,Reference_Number__c='ENQ-00000098');
        insert op;
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
        op.StageName='CMS';
        update op;
        op.StageName='Finance';
        update op;
        //Creating RFQ Service Record
        RFQ_Service__c service1 = new RFQ_Service__c(Name='service1',Opportunity__c=op.Id);
        insert service1;
        //Creating RFQ Record for the RFQ service
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Rfq__c RFQ2 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ2;
        //creating Payment Record
        Payment__c payment1 = new Payment__c(Amount_Paid__c=50,RFQ__c=RFQ1.id,Submitted__c=true);
        insert payment1;
        Payment__c payment2 = new Payment__c(Amount_Paid__c=50,RFQ__c=RFQ1.id,Submitted__c=true);
        insert payment2;
        return op;
        
    }
    @isTest static void PaymentApproval(){
        
        Opportunity op = RFQPaymentApprovalEXT_Test.TestDataGenerator();
        //creating an inboundEmail
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        List<Payment__c> PaymentList = [select name from Payment__c where RFQ__r.RFQ_Service__r.Opportunity__c =: op.id and RFQ__r.Status__c = 'Approved' and RFQ__r.RFQ_Service__r.Opportunity__r.StageName = 'Finance' and Approved__c != true order by RFQ__r.RFQ_Service__r.Opportunity__c];
        String paymentNumbers='';
        //Getting all Payment number and store in a list
        for(Payment__c pay : PaymentList){
            paymentNumbers += pay.Name+',';
            
        }
        System.debug('Payment NUmber '+paymentNumbers);
        String Referalno=' Reference Number: *'+op.Reference_Number__c+'*';
        System.debug('Reference no'+Referalno);
        String Paymentno='Payment Numbers: *'+paymentNumbers+'*';
        String emailBody = 'Approve'+'\n'+' Comment:1234; '+'\n'+Referalno+'\n'+Paymentno+' ';
        email.fromName = 'test test';
        email.plainTextBody =emailBody; 
        envelope.fromAddress = 'user@Test.com';
        // setup Catcher object
        System.debug('Email PlainTextBOdy'+email.plainTextBody);
        RFQPaymentApprovalEXT Emailcatcher = new RFQPaymentApprovalEXT();
        Messaging.InboundEmailResult result = Emailcatcher.handleInboundEmail(email, envelope);
        Payment__c checkingPayment = [select id,Approved__c from Payment__c where RFQ__r.RFQ_Service__r.Opportunity__r.id=:op.Id limit 1];
        System.debug('***********************OPPortunity Email:'+op.owner.Email);
        //checking if the payment is approved
        System.assertEquals(true,checkingPayment.Approved__c);
        
    }
    @isTest static void PaymentReject(){
        Opportunity op = RFQPaymentApprovalEXT_Test.TestDataGenerator();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        List<Payment__c> PaymentList = [select name from Payment__c where RFQ__r.RFQ_Service__r.Opportunity__c =: op.id and RFQ__r.Status__c = 'Approved' and RFQ__r.RFQ_Service__r.Opportunity__r.StageName = 'Finance' and Approved__c != true order by RFQ__r.RFQ_Service__r.Opportunity__c];
        String paymentNumbers='';
        //Getting all Payment number and store in a list
        for(Payment__c pay : PaymentList){
            paymentNumbers += pay.Name+',';
        }
        String Referalno='Reference Number: *'+op.Reference_Number__c+'*';
        String Paymentno='Payment Numbers: *'+paymentNumbers+'*';
        
        email.fromName = 'test test';
        email.plainTextBody = 'Reject'+'\n'+'Comment:1234'+'\n'+''+Referalno+''+'\n'+Paymentno+'';
        envelope.fromAddress = 'user@Test.com';
        // setup Catcher object
        RFQPaymentApprovalEXT Emailcatcher = new RFQPaymentApprovalEXT();
        Messaging.InboundEmailResult result = Emailcatcher.handleInboundEmail(email, envelope);
        Payment__c checkingPayment = [select id,Approved__c,Rejected__c from Payment__c where RFQ__r.RFQ_Service__r.Opportunity__c=:op.Id limit 1];
        System.assertEquals(false,checkingPayment.Approved__c);
        System.assertEquals(true,checkingPayment.Rejected__c);
        
    }
    @isTest static void PaymentRejectNoComment(){
        Opportunity op = RFQPaymentApprovalEXT_Test.TestDataGenerator();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        List<Payment__c> PaymentList = [select name from Payment__c where RFQ__r.RFQ_Service__r.Opportunity__c =: op.id and RFQ__r.Status__c = 'Approved' and RFQ__r.RFQ_Service__r.Opportunity__r.StageName = 'Finance' and Approved__c != true order by RFQ__r.RFQ_Service__r.Opportunity__c];
        String paymentNumbers='';
        //Getting all Payment number and store in a list
        for(Payment__c pay : PaymentList){
            paymentNumbers += pay.Name+',';
        }
        String Referalno='Reference Number: *'+op.Reference_Number__c+'*';
        String Paymentno='Payment Numbers: *'+paymentNumbers+'*';
        email.fromName = 'test test';
        email.plainTextBody = 'Reject'+'\n'+''+'\n'+''+Referalno+''+'\n'+Paymentno+'';
        envelope.fromAddress = 'user@Test.com';
        // setup Catcher object
        RFQPaymentApprovalEXT Emailcatcher = new RFQPaymentApprovalEXT();
        Messaging.InboundEmailResult result = Emailcatcher.handleInboundEmail(email, envelope);
        Payment__c checkingPayment = [select id,Approved__c,Rejected__c from Payment__c where RFQ__r.RFQ_Service__r.Opportunity__c=:op.Id limit 1];
        System.assertEquals(false,checkingPayment.Approved__c);
        System.assertEquals(true,checkingPayment.Rejected__c);
        
    }
}
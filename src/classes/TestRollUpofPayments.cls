/*********************************************************************************
Class Name      : TestRollUpofPayments
Description     : Being used to test RollUpofPaymentsHandlerClass
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
@isTest
public class TestRollUpofPayments {
    @isTest static void testpaymentrecords(){
        //recordtypes are needed for creating two types of paymnets-vendor and customer 
        RecordType vendorrt=new RecordType();
        RecordType customerrt=new RecordType();
        //querying recordtypes and storing 
        list<RecordType> RecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Payment__c'];
        for(RecordType rts:RecordTypeList){
            if(rts.Name=='Payment to Vendor') 
                vendorrt =rts;
            if(rts.Name=='Payment from Customer') 
                customerrt=rts;
        }  list<Payment__c> Paymentlist=new list<Payment__c>();
        Date closedDate = Date.newInstance(2018, 01, 09);
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account account=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert account;
        //to insert payment records we need opportunity,rfqservice and rfq 
        Opportunity op = new Opportunity(Name='test',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert op;
        RFQ_Service__c rfqservice=new RFQ_Service__c();
        rfqservice.name='others';
        rfqservice.Opportunity__c=op.id;
        insert rfqservice;
        RFQ__c rfq=new RFQ__c();
        rfq.RFQ_Service__c=rfqservice.id;
        rfq.Vendor_Account__c=account.id;
        insert rfq;
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,RFQ__c=RFQ.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
        insert charge1;
        rfq.Status__c='Approved';
        update rfq;
        //creating vendor paymnets
        for(integer i=0;i<2;i++){
            Payment__c p=new Payment__c();
            p.Amount_Paid__c=100;
            p.RecordTypeId=vendorrt.id;
            p.RFQ__c=rfq.id;
            p.Approved__c=true;
            Paymentlist.add(p);
        }
        //creating customer payments
        for(integer i=0;i<2;i++){
            Payment__c p=new Payment__c();
            p.Amount_Paid__c=100;
            p.RecordTypeId=customerrt.id;
            p.Opportunity__c=op.id;
            p.Approved__c=true;
            Paymentlist.add(p);
        }
        //testing for insert and delete scenarios
        insert Paymentlist;
        RFQ__c RFQ1=[select Amount_Paid__c,Status__c from RFQ__C where RFQ_Service__c=:rfqservice.id];
        Opportunity opp=[select Amount_Received__c from Opportunity where id=:op.id];
        //As two payment records are inserted with each 100 expected total is 200
        System.assertEquals(200, RFQ1.Amount_Paid__c);
        System.assertEquals(200, opp.Amount_Received__c);
        delete Paymentlist;
        RFQ__c RFQ2=[select Amount_Paid__c,Status__c from RFQ__C where RFQ_Service__c=:rfqservice.id];
        Opportunity opp2=[select Amount_Received__c from Opportunity where id=:op.id];
        System.assertEquals(0, RFQ2.Amount_Paid__c);
        System.assertEquals(0, opp2.Amount_Received__c);
    }
}
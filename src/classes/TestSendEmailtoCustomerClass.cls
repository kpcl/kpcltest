/*********************************************************************************
Class Name      : TestSendEmailtoCustomerClass
Description     : Being used to test SendEmailtoCustomerClass class 
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
@isTest
public class TestSendEmailtoCustomerClass {
    static testMethod void CallSendMail()
    {
        //to execute sendmail method we need Opportunity id ,content document and one email id,so creating those
        Account account=new Account(name='test');
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity Opp1 = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp1;
        //changing Opportunity stage to CMS Successively
               Opp1.StageName='CMS';
        update Opp1;
        //creatinng second opportunity with no content  document
        Opportunity Opp2 = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp2;
        //changing Opportunity stage to CMS Successively
       
        Opp2.StageName='CMS';
        update Opp2;
        Contact con=new Contact(LastName='Smith',AccountId=account.id);
        insert con;
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'ABCFinalquote_v1',PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true,FirstPublishLocationId =Opp1.id);
        insert contentVersion_1;
        string returnvalue1=SendEmailtoCustomerClass.sendMail(Opp1.id,'nagasai.chalamalasetti@absyz.com','vinaywins@hotmail.com');     
        string returnvalue2=SendEmailtoCustomerClass.sendMail(Opp2.id,'nagasai.chalamalasetti@absyz.com','vinaywins@hotmail.com');     
        System.assertEquals('SUCCESS', returnvalue1);
        System.assertEquals('File Not Found', returnvalue2);
    }
    static testMethod void testExceptionCase()
    {
        Account account=new Account(name='test');
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp;   
        SendEmailtoCustomerClass.sendMail(Opp.id,'','');
        string returnvalue=SendEmailtoCustomerClass.getProfileId();
        System.assertEquals('false', returnvalue);
    }
}
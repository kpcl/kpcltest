/*********************************************************************************
Class Name      : ApprovalProcessForRFqClass
Description     : Being used by ApprovalForRFQComponent Lightning Component for mailing quoteworksheets to aprovers and submiiting opportuntiy for approval.
Created By      : Naga sai
Created Date    : Dec-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga sai                 Dec-2017            Design as per Client
*********************************************************************************/
public  class ApprovalProcessForRFqClass {
    @AuraEnabled
    //checks whether the logged in user is BD Profile or not
    public static string  getProfileId(){
        user u=[select Profile.name  from user where id=:UserInfo.getUserId()];
        profiles__c pfs=profiles__c.getOrgDefaults();
        if(pfs.KPCLMarketingUser__c==u.Profile.name || pfs.KPCL_Admin__c==u.Profile.name ||  u.Profile.name=='System Administrator')
           return 'true';
        else 
            return 'false';
    }
    @AuraEnabled
    public static string sendMail(id oppId)
    {
        try{           
            list<id> QuoteWorksheetIds=new list<id>();
            //for storing error message 
            string status;
            String[] Addresses=new string[]{};
                list<id> ApproverId=new list<id>();
            //querying if the record is already submitted for approval
            Opportunity opp=[select SentForApproval__c from Opportunity where id=:oppid];
            if(opp.SentForApproval__c==false){     
                //querying documents
                list<contentversion> QuoteWorksheetList=[select id from contentversion where Title like '%_quoteworksheet_v%' and FirstPublishLocationId=:OppId ORDER By ContentModifiedDate DESC limit 1];       
                if(QuoteWorksheetList.size()>0)
                {
                    //getting emailids of approvers from custtomlabels
                    String[] approverUsersList=label.Payment_Approver.split(',');
                    if(approverUsersList!=Null){
                        //setting the approval process
                          Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
                        psr.setObjectId(oppId);
                        psr.setProcessDefinitionNameOrId('RFQApprovalByHigherAuthority');
                        Approval.process(psr); 
                        
                        //querying respective users and adding their email to a string array
                        list<user> approverUsersListEmails=[select email from user where username in:approverUsersList];
                        for(user useremail:approverUsersListEmails){
                            Addresses.add(useremail.email); 
                            system.debug('useremail'+useremail.email);
                        }
                        //collecting worksheet ids
                        for(contentversion QuoteWorksheet: QuoteWorksheetList){
                            QuoteWorksheetIds.add(QuoteWorksheet.id);
                        }
                        String templateName = 'RFQApprovalByHigherAuthoritywithattachments';
                        EmailTemplate template =  [SELECT Id, Name FROM EmailTemplate WHERE Name like :templateName LIMIT 1];
                        contact  con=[select id,email from contact limit 1];
                        String[] toAddresses=Addresses;                    
                        //setting up email object
                        Messaging.SingleEmailMessage email=new  Messaging.SingleEmailMessage();             
                        email.setTreatTargetObjectAsRecipient(false);        
                        email.setTargetObjectId(con.id);            
                        email.setWhatId(Oppid);
                        email.setTemplateId(template.Id);               
                        email.setToAddresses(toAddresses);                          
                        email.setEntityAttachments(QuoteWorksheetIds);        
                        Messaging.SendEmailResult[] r=Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});                   
                        status='Submitted';                   
                    } 
                    else{
                        status='No Approver Email found on custom label';   
                    }
                }
                else{
                    status='No QuoteWorksheet document found';                                       
                }                
            } 
            else{
                status='Quote worksheet is already submitted for approval.';                       
            }         
            return status;
        }
        Catch(Exception e){
            system.debug('error'+e.getMessage());          
            return 'error';
        }
    }
}
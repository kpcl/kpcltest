public class StringDistance {
public static boolean isDistanceOneOrTwo(String FirstString,String SecondString)
{
  Integer FirstStringLength = FirstString.length();
  Integer SecondStringLength = SecondString.length();
  Integer count=0,i=0,j=0;
  if(math.abs(FirstStringLength-SecondStringLength)>2)
  {
      return false;
  }
  while(i<FirstStringLength && j<SecondStringLength)
  {
      if(FirstString.charAt(i)!=SecondString.charAt(j))
      {
          if(count==2)
          {
              return false;
          }
          if(FirstStringLength>SecondStringLength)
          {
              i++;
          }
          else if(FirstStringLength<SecondStringLength)
          {
              j++;
          }
          else
          {
              i++;
              j++;
          }
          count++;
      }
      else
      {
          i++;
          j++;
      }
  }
    if(i<FirstStringLength||j<SecondStringLength)
    {
        count++;
    }
  return (count==2 || count==1);  
}
}
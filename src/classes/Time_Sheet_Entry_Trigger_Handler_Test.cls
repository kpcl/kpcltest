@isTest
public class Time_Sheet_Entry_Trigger_Handler_Test {
@istest
   public static TimeSheet__c recordGeneration() {
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='test9677@test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='test9677@test.com');
        insert u;
       Date myDate = Date.newInstance(2018, 2, 19);
       
	TimeSheet__c TimeSheet = new TimeSheet__c(Employee_Name__c=u.id,SHIFT__c='9:00 AM - 06:00 PM',Present_Absent__c='P',Total_Hours__c=9, Date__c=myDate);
    insert TimeSheet;
    return TimeSheet;
   }
@istest 
    public static void update_Duration_hours()
    {
     Integer TotalTime=0;
     TimeSheet__c TimeSheet = Time_Sheet_Entry_Trigger_Handler_Test.recordGeneration();
        List<Time_Sheet_Entry__c> EntryList = [select name,id,Duration_In_Hours__c from Time_Sheet_Entry__c where TimeSheet__c=:TimeSheet.id];
        if(EntryList.size()>0)
        {	Integer i=0;
         	
            for(Time_Sheet_Entry__c eachRecord:EntryList)
            {
                if(i==3)
                {
                    break;
                }
                else
                {
                    TotalTime=TotalTime+3;
                    eachRecord.Duration_In_Hours__c=3;
                }
                i=i+1;
            }
        }
        update EntryList;
        TimeSheet = [select id,Sum_up_hours__c  from TimeSheet__c where id=:TimeSheet.id];
        system.assertEquals(TotalTime, TimeSheet.Sum_up_hours__c);
    }
    @istest 
    public static void update_Duration_hours_throw_Error()
    {
     Integer TotalTime=0;
     TimeSheet__c TimeSheet = Time_Sheet_Entry_Trigger_Handler_Test.recordGeneration();
        List<Time_Sheet_Entry__c> EntryList = [select name,id,Duration_In_Hours__c from Time_Sheet_Entry__c where TimeSheet__c=:TimeSheet.id];
        if(EntryList.size()>0)
        {	Integer i=0;
         	
            for(Time_Sheet_Entry__c eachRecord:EntryList)
            {
                if(i==5)
                {
                    break;
                }
                else
                {
                    TotalTime=TotalTime+30;
                    eachRecord.Duration_In_Hours__c=30;
                }
                i=i+1;
            }
        }
        try{
        update EntryList;  
       // System.assertEquals('Total Number of Hours Cant be more than 9',result.getErrors()[0].getMessage());
        }
        catch(Exception e)
		{
		Boolean expectedExceptionThrown =  e.getMessage().contains('Total Number of Hours Cant be more') ? true : false;
		System.AssertEquals(expectedExceptionThrown, true);
		} 
    }
    @istest 
    public static void update_Duration_hours_throw_Error_1()
    {
     Integer TotalTime=0;
     TimeSheet__c TimeSheet = Time_Sheet_Entry_Trigger_Handler_Test.recordGeneration();
        List<Time_Sheet_Entry__c> EntryList = [select name,id,Duration_In_Hours__c from Time_Sheet_Entry__c where TimeSheet__c=:TimeSheet.id];
        if(EntryList.size()>0)
        {	Integer i=0;
         	
            for(Time_Sheet_Entry__c eachRecord:EntryList)
            {
                if(i==5)
                {
                    break;
                }
                else
                {
                    TotalTime=TotalTime+1;
                    eachRecord.Duration_In_Hours__c=1;
                }
                i=i+1;
            }
        }
        try{
        update EntryList; 
        EntryList[0].Duration_In_Hours__c=55;
        update EntryList;   
            
       // System.assertEquals('Total Number of Hours Cant be more than 9',result.getErrors()[0].getMessage());
        }
        catch(Exception e)
		{
		Boolean expectedExceptionThrown =  e.getMessage().contains('Total Number of') ? true : false;
		System.AssertEquals(expectedExceptionThrown, true);
		} 
    }
    @istest 
    public static void update_Duration_hours_throw_Error_2()
    {
     Integer TotalTime=0;
     TimeSheet__c TimeSheet = Time_Sheet_Entry_Trigger_Handler_Test.recordGeneration();
        TimeSheet.Extra_hours__c=5;
        update TimeSheet;
        List<Time_Sheet_Entry__c> EntryList = [select name,id,Duration_In_Hours__c from Time_Sheet_Entry__c where TimeSheet__c=:TimeSheet.id];
        if(EntryList.size()>0)
        {	Integer i=0;
         	
            for(Time_Sheet_Entry__c eachRecord:EntryList)
            {
                if(i==5)
                {
                    break;
                }
                else
                {
                    TotalTime=TotalTime+1;
                    eachRecord.Duration_In_Hours__c=1;
                }
                i=i+1;
            }
        }
        try{
        update EntryList; 
        EntryList[0].Duration_In_Hours__c=55;
        update EntryList;   
            
       // System.assertEquals('Total Number of Hours Cant be more than 9',result.getErrors()[0].getMessage());
        }
        catch(Exception e)
		{
		Boolean expectedExceptionThrown =  e.getMessage().contains('Total Number of Hours Cant be more') ? true : false;
		System.AssertEquals(expectedExceptionThrown, true);
		} 
    }
    
}
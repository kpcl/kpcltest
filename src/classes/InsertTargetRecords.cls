global class InsertTargetRecords implements Database.batchable<sObject>{   
    global Database.QueryLocator start(Database.batchableContext bc){
        return Database.getQueryLocator('select name,id from user');
    }
    
    global void execute(Database.BatchableContext bc, List<user> UserList){
        list<Target__c> TargetList=new list<Target__c>();
        for(user u:UserList){
            Target__c t=new Target__c();
            t.User__c=u.id;
            Date startDate = System.Date.today().toStartOfMonth();
            system.debug('moneth start'+startDate);
            
            Date lastDate = startDate.addMonths(1).addDays(-1);
            system.debug('moneth end'+lastDate);
                        
            t.Start_Date__c=startDate;
            t.End_Date__c=lastDate;
            TargetList.add(t);
        }
        insert TargetList;
        
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
}
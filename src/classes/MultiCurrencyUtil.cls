public with sharing class MultiCurrencyUtil {  
    //Protected Members
    private static MultiCurrencyUtil instance;

    //Properties
    public final Map<string, CurrencyType> IsoCurrencyTypes;

    //Constructors
    private MultiCurrencyUtil() {
        IsoCurrencyTypes = getCurrencyTypesByIsoCode();
    }

    //Static Methods
    public static MultiCurrencyUtil getInstance() {
        if (instance == null) {
            instance = new MultiCurrencyUtil();
        }

        return instance;
    }

    public static decimal convertCurrency(string fromIsoCode, string toIsoCode, decimal value) {
        MultiCurrencyUtil util = MultiCurrencyUtil.getInstance();
        decimal convertedValue = value;

        boolean conversionRequired = (fromIsoCode != toIsoCode && value != null);
        boolean fromIsoCodeValid = util.IsoCurrencyTypes.containsKey(fromIsoCode);
        boolean toIsoCodeValid = util.IsoCurrencyTypes.containsKey(toIsoCode);

        if (conversionRequired && fromIsoCodeValid && toIsoCodeValid) {
            CurrencyType fromCurrencyType = util.IsoCurrencyTypes.get(fromIsoCode);
            convertedValue = value / fromCurrencyType.ConversionRate;

            CurrencyType toCurrencyType = util.IsoCurrencyTypes.get(toIsoCode);
            convertedValue = convertedValue * toCurrencyType.ConversionRate;
        }

        return convertedValue;
    }

    //Private Methods
    private Map<string, CurrencyType> getCurrencyTypesByIsoCode() {
        Map<string, CurrencyType> currencyTypesByIsoCode = new Map<string, CurrencyType>();

        List<CurrencyType> currencyTypes = queryCurrencyTypes();
        for (CurrencyType currencyType : currencyTypes) {
            currencyTypesByIsoCode.put(currencyType.IsoCode, currencyType);
        }

        return currencyTypesByIsoCode;
    }

    //Query Methods
    private List<CurrencyType> queryCurrencyTypes() {
        return [SELECT
                    Id
                    ,IsoCode
                    ,ConversionRate
                    ,IsCorporate
                FROM
                    CurrencyType];
    }
}
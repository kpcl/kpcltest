/*********************************************************************************
Class Name      : DisplayRFQsOnOpportunityController
Description     : Being used by a component displayrfqsonopportunity to display related RFQS on opprtunity page
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
public class DisplayRFQsOnOpportunityController {
 @AuraEnabled
    public static List<RFQ__c> getRFQ(Id OppId){
        
        list<RFQ_Service__c> RFQServicelist = [Select Id From RFQ_Service__c Where  Opportunity__c= :OppId];
        set<id> RFQServiceIds=new set<id>();
        for(RFQ_Service__c RFQService:RFQServicelist){
            RFQServiceIds.add(RFQService.id);
        }
        
        List<RFQ__c> RFQlist = [Select  Name,VendorName__c,RFQ_Service__r.Name  From RFQ__c Where RFQ_Service__c in :RFQServiceIds];
       system.debug('Entered');
        return RFQlist;
    }
}
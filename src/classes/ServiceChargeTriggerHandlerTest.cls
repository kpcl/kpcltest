/*********************************************************************************
Class Name      : ServiceChargeTriggerHandlerTest
Description     : Test class for ServiceChargeTriggerHandler in Service charge Object to implement Roll up Summary function in a lookup Relation between ServiceCharge and (RFQ,Opportunity)
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Design as per Client
*********************************************************************************/
@isTest
public class ServiceChargeTriggerHandlerTest {
    @isTest static id TestDataForOpportunity(){
         list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account a=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert a;
        //Creating Date Instance for Closed Date (* required Field in Opportunity)
        Date closedDate = Date.newInstance(2018, 01, 09);
        //Creating Opportunity Record
        Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate);
        insert op;
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
        op.StageName='CMS';
        update op;
        op.StageName='Finance';
        update op;
        return op.Id;
    }
    @isTest static id TestDataGenerator(){
        RecordType ServiceChargeForRFQ  = [SELECT Id,name FROM RecordType WHERE SObjectType='Service_Charge__c' AND Name='Service Charge For RFQ' LIMIT 1];
          list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account a=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert a;
        //Creating Date Instance for Closed Date (* required Field in Opportunity)
        Date closedDate = Date.newInstance(2018, 01, 09);
        //Creating Opportunity Record
        Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate);
        insert op;
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
        op.StageName='CMS';
        update op;
        op.StageName='Finance';
        update op;
        //Creating RFQ Service Record
        RFQ_Service__c service1 = new RFQ_Service__c(Name='service1',Opportunity__c=op.Id);
        insert service1;
        //Creating RFQ Record for the RFQ service
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ1.Id,RecordTypeId=ServiceChargeForRFQ.id);
        insert charge1;
        Service_Charge__c Charge2 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ1.Id,RecordTypeId=ServiceChargeForRFQ.id);
        insert charge2;
        Service_Charge__c Charge3 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ1.Id,RecordTypeId=ServiceChargeForRFQ.id);
        insert charge3;
        return RFQ1.id;
        
    }
    //to check for Records insertion under RFQ Service charge
    @isTest static void CheckinsertRecords(){
        id rfqId = ServiceChargeTriggerHandlerTest.TestDataGenerator();
        
        Rfq__c rfqRecord = [select Total_Amount_Payable__c,Total_Margin__c,Total_Taxable_Value__c,Total_Receivable__c from RFQ__c where id=:rfqId];
        System.assertEquals(3000, rfqRecord.Total_Amount_Payable__c);
        System.assertEquals(1500, rfqRecord.Total_Margin__c);
        System.assertEquals(4500, rfqRecord.Total_Taxable_Value__c);
        System.assertEquals(4500, rfqRecord.Total_Receivable__c);  
    }
    //to check for Records Updation under RFQ Service charge
    @isTest static void CheckUpdateRecords(){
        id rfqId = ServiceChargeTriggerHandlerTest.TestDataGenerator();
        
        List<Service_Charge__c> serviceChargeRec = [select Margin_Per_Unit__c, Quantity__c , Rate_Per_Unit__c ,RFQ__c from Service_Charge__c where RFQ__c=:rfqId]; 
        for(Service_Charge__c c:serviceChargeRec)
        {
            c.Quantity__c=200;
        }
        update serviceChargeRec;
        Rfq__c rfqRecord = [select Total_Amount_Payable__c,Total_Margin__c,Total_Taxable_Value__c,Total_Receivable__c from RFQ__c where id=:rfqId];
        System.assertEquals(6000, rfqRecord.Total_Amount_Payable__c);
        System.assertEquals(3000, rfqRecord.Total_Margin__c);
        System.assertEquals(9000, rfqRecord.Total_Taxable_Value__c);
        System.assertEquals(9000, rfqRecord.Total_Receivable__c);  
    }
    //to check for Records Deletion under RFQ Service charge
    @isTest static void CheckDeleteRecords(){
        id rfqId = ServiceChargeTriggerHandlerTest.TestDataGenerator();
        List<Service_Charge__c> serviceChargeRec = [select Margin_Per_Unit__c, Quantity__c , Rate_Per_Unit__c ,RFQ__c from Service_Charge__c  where RFQ__c=:rfqId];
        delete serviceChargeRec;
        Rfq__c rfqRecord = [select Total_Amount_Payable__c,Total_Margin__c,Total_Taxable_Value__c,Total_Receivable__c from RFQ__c where id=:rfqId];
        System.assertEquals(0, rfqRecord.Total_Amount_Payable__c);
        System.assertEquals(0, rfqRecord.Total_Margin__c);
        System.assertEquals(0, rfqRecord.Total_Taxable_Value__c);
        System.assertEquals(0, rfqRecord.Total_Receivable__c);
        
    }
    //to check for Records insertion under Opportunity -->Additional Service charge
    @isTest static void CheckInsertOpportunityChargeRecords(){
        RecordType AdditionalServiceCharge = [SELECT Id,name FROM RecordType WHERE SObjectType='Service_Charge__c' AND Name='Additional Service Charge' LIMIT 1];
        
        id OppId = ServiceChargeTriggerHandlerTest.TestDataForOpportunity();
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,Opportunity__c=OppId,RecordTypeId=AdditionalServiceCharge.id);
        insert charge1;
        Service_Charge__c Charge2 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,Opportunity__c=OppId,RecordTypeId=AdditionalServiceCharge.id);
        insert charge2;
        Service_Charge__c Charge3 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,Opportunity__c=OppId,RecordTypeId=AdditionalServiceCharge.id);
        insert charge3;
        
        Opportunity opRec = [select id,name,Additional_Finance_Charges__c from Opportunity where id=:OppId];
        System.assertEquals(4500, opRec.Additional_Finance_Charges__c);
    }
    //to check for Records Updation under Opportunity -->Additional Service charge
    @isTest static void CheckUpdateOpportunityChargeRecords(){
        RecordType AdditionalServiceCharge = [SELECT Id,name FROM RecordType WHERE SObjectType='Service_Charge__c' AND Name='Additional Service Charge' LIMIT 1];
        id OppId = ServiceChargeTriggerHandlerTest.TestDataForOpportunity();
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,Opportunity__c=OppId,RecordTypeId=AdditionalServiceCharge.id);
        insert charge1;
        Service_Charge__c Charge2 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,Opportunity__c=OppId,RecordTypeId=AdditionalServiceCharge.id);
        insert charge2;
        Service_Charge__c Charge3 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,Opportunity__c=OppId,RecordTypeId=AdditionalServiceCharge.id);
        insert charge3;
        List<Service_Charge__c> serviceChargeRec = [select Margin_Per_Unit__c, Quantity__c , Rate_Per_Unit__c ,RFQ__c from Service_Charge__c where Opportunity__c=:OppId]; 
        for(Service_Charge__c c:serviceChargeRec)
        {
            c.Quantity__c=300;
        }
        update serviceChargeRec;
        Opportunity opRec = [select id,name,Additional_Finance_Charges__c from Opportunity where id=:OppId];
        System.assertEquals(13500, opRec.Additional_Finance_Charges__c);
    }
    //to check for Records Deletion under Opportunity -->Additional Service charge
    @isTest static void CheckDeleteOpportunityChargeRecords(){
        RecordType AdditionalServiceCharge = [SELECT Id,name FROM RecordType WHERE SObjectType='Service_Charge__c' AND Name='Additional Service Charge' LIMIT 1];
        id OppId = ServiceChargeTriggerHandlerTest.TestDataForOpportunity();
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,Opportunity__c=OppId,RecordTypeId=AdditionalServiceCharge.id);
        insert charge1;
        List<Service_Charge__c> serviceChargeRec = [select Margin_Per_Unit__c, Quantity__c , Rate_Per_Unit__c ,RFQ__c from Service_Charge__c where Opportunity__c=:OppId]; 
        delete serviceChargeRec;
        Opportunity opRec = [select id,name,Additional_Finance_Charges__c from Opportunity where id=:OppId];
        System.assertEquals(0, opRec.Additional_Finance_Charges__c);
    }
    @isTest static void toThrowException()
    {
        try{
            Account a = new Account();
            insert a;
        }
        catch(Exception e)
        {
            
        }
    }
    
    
    
}
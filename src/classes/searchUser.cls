public with sharing class searchUser {
 
 @AuraEnabled
 public static List < Target__c > fetchAccount(String searchKeyWord) {
  String searchKey = searchKeyWord + '%';
  List < Target__c > returnList = new List < Target__c > ();
  List < Target__c > lstOfAccount = [select id, User__r.Name, Start_Date__c, End_Date__c from Target__c  where User__r.Name LIKE: searchKey];
 
  for (Target__c acc: lstOfAccount) {
   returnList.add(acc);
  }
  return returnList;
 }
}
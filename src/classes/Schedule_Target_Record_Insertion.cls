global class Schedule_Target_Record_Insertion implements Schedulable {
   global void execute(SchedulableContext sc) {
    InsertTargetRecords rec = new InsertTargetRecords();
    ID batchprocessid = Database.executeBatch(rec);
   }
}
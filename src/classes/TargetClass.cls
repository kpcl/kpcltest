public with sharing class TargetClass {
    @AuraEnabled
    public static object displaytargetdetails()
    {
        
        String objectName='Target__c';
        String queryFields='id,User__r.Name,Start_Date__c';
        String fullQuery='Select User__r.Name,Start_Date__c,End_Date__c from Target__c';
    	/*String objectName='Opportunity';
        String queryFields='Opportunity_Name__c,Cargo__c,Shipping_Type__c';
        String fullQuery='Select Opportunity_Name__c,Cargo__c,Shipping_Type__c from Opportunity';*/
    
        return TargetClass.Get_Record_For_Table_Input_From_Query(objectName, queryFields, fullQuery);
    }
    @AuraEnabled
    public static object displayCurrentTargetDetails()
    {
        Integer month = System.Today().MOnth();
		Integer yr = System.Today().Year();
        String objectName='Target__c';
        String queryFields='id,User__r.Name,Start_Date__c';
        String fullQuery='Select User__r.Name,Start_Date__c,End_Date__c from Target__c where CALENDAR_MONTH(Start_Date__c)='+month+' and CALENDAR_YEAR(Start_Date__c)='+yr+'';
        system.debug('fullQuery'+fullQuery);
        return TargetClass.Get_Record_For_Table_Input_From_Query(objectName, queryFields, fullQuery);
    }

    @AuraEnabled
    public static object Get_Record_For_Table_Input_From_Query(String objectName,String queryFields,String fullQuery)
    {
        Integer headerindex=0; 
        TargetTable TargetTable_inst = new TargetTable();
        List<EachRecord> allListOfRecords = new List<EachRecord>();
        
        //String objectName = 'Target__c';
        //String queryFields='id,User__r.Name,Start_Date__c,End_Date__c';
         String Query=fullQuery;
        List<String> fieldList= queryFields.split(',');
        //Creating a Dynamic Object Type with Custom object name
        String listType = 'List<' + objectName + '>';
        String EachRecordType =  ''+objectName+'';
        //Creating a generic Sobject to hold data of custom object
        List<SObject> ListOfRecords = (List<SObject>)Type.forName(listType).newInstance();
        SObject IndividualMember = (SObject)Type.forName(EachRecordType).newInstance();
        //Execute the Query to get and store the data in Generic object
        ListOfRecords = Database.query(Query);
        system.debug(ListOfRecords);
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        for(Integer i=0;i<ListOfRecords.size();i++)
            {
                IndividualMember = ListOfRecords[i];
                EachRecord Each_record_nested_List_Of_Fields = new EachRecord();
                List<object> temp = new List<object>();
              for(String field :fieldList)
              {
                  if(field=='id')
                  {
                      Each_record_nested_List_Of_Fields.recordId=(Id)IndividualMember.get(field);
                  }
                  if(field.containsAny('\\.'))
                  {
                      List<String> relation = field.split('\\.');
                      
                      temp.add(IndividualMember.getSobject(relation[0]).get(relation[1]));
                  }
                  else
                  {
                      if(field!='id'){
                     temp.add((IndividualMember.get(field)==null)?'':IndividualMember.get(field));
                      }
                  }
               }
                System.debug('i='+i+'-'+temp);
               Each_record_nested_List_Of_Fields.recordValue=temp;
			allListOfRecords.add(Each_record_nested_List_Of_Fields);
            }
        
        
        List<header> headerList = new List<header>();
        
        for(String field :fieldList)
        {
            if(field!='id'){
            header h = new header();
            h.index=headerindex++;
            
            String s = field;
            Integer i=s.lastIndexOf('_');
            if(i!=-1)
            {
               s=s.substring(0,i);
            }
            s=s.replaceAll('_',' ');
            system.debug('String'+s);
            h.name=s;
            h.sorted=2;
            headerList.add(h);
            }
        }
        //system.debug('Header:'+headerList);
        TargetTable_inst.headerList=headerList;
        TargetTable_inst.records=ListOfRecords;
        TargetTable_inst.ListOfEachRecord=allListOfRecords;
        
        
        
        
        return TargetTable_inst;
        
    }
    @AuraEnabled
    public static list<Target__c> displayCurrentUserDetails()
    {
        return [Select id,User__r.Name from Target__c];
    }
    public static void displayAllField()
    {
        Schema.DescribeSObjectResult r = Target__c.sObjectType.getDescribe();
        List<String>apiNames =  new list<String>();
        for(string apiName : r.fields.getMap().keySet()){
            System.debug(apiName+'\n');
        }
        
    }
    @AuraEnabled
    public static object fetchAccount(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        String objectName='Target__c';
        String queryFields='id,User__r.Name,Start_Date__c,End_Date__c';
        String fullQuery='Select id,User__r.Name,Start_Date__c,End_Date__c from Target__c where ';
        fullQuery+='User__r.Name LIKE \''+String.escapeSingleQuotes(searchKeyWord)+'%\'';
        system.debug('full query'+fullQuery);
        
      /*  List < Target__c > returnList = new List < Target__c > ();
        
        List < Target__c > lstOfAccount = [select id, User__r.Name, Start_Date__c, End_Date__c from Target__c  where User__r.Name LIKE: searchKey];
        
        for (Target__c acc: lstOfAccount) {
            returnList.add(acc);
        }*/
        return TargetClass.Get_Record_For_Table_Input_From_Query(objectName, queryFields, fullQuery);
    }
    
    public class header{
        @AuraEnabled public String name;
        @AuraEnabled public String api; 
        @AuraEnabled public Integer sorted;
        @AuraEnabled public Integer index;
        int a;
    }
    public class EachRecord{
        @AuraEnabled public Map<String,object> fieldValuePair;
        @AuraEnabled public List<object> recordValue;
        @AuraEnabled public Id recordId;
    }
    public class TargetTable{
        @AuraEnabled public List<header> headerList;
        @AuraEnabled public List <sObject> records;
        @AuraEnabled public List <EachRecord> ListOfEachRecord;
    }
    
}
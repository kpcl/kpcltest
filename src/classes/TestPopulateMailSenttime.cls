/*********************************************************************************
Class Name      : TestPopulateMailSenttime
Description     : Being used to test PopulateMailSenttime  class 
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
@isTest
public class TestPopulateMailSenttime {
    static testMethod void CallSendMail()
    {
         Profile RFQpf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        UserRole ur = new UserRole(Name = 'CEO');
        insert ur;
       // User usr = TestUserUtil.createTestUser(ur.Id, pf.Id, 'Test FirstName', 'Test LastName');
       String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User RFQuser = new User(  firstname = 'fName',
                                lastName = 'lName',
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = RFQpf.id,
                                UserRoleId = ur.id);
          User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
 System.runAs(RFQuser){
        //we need to send RFQ record to sendmail method,so creating RFQ.
          RecordType AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
       Account account=new Account(name='test',RecordTypeId=AccountRecordTypeList.id);
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        //inserting Opportunity,RFQService,RFQ
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=9);
        insert Opp;
      list<RFQ_Service__c> service1 = [select id from RFQ_Service__c];
        //Creating RFQ Record for the RFQ service
        list<Rfq__c> rfqlist=new list<RFQ__c>();
        for(RFQ_Service__c rfqservice:service1){
            Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=rfqservice.id,Amount_Paid__c=50, Status__c='Approved');
            rfqlist.add(RFQ1);
        }
        insert rfqlist;
        //craeting service charge for each RFQ
        list<Service_Charge__c> servicechargelist=new  list<Service_Charge__c>();
        for(RFQ__C rfq:rfqlist){
            Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
            servicechargelist.add(Charge1);
        }
        insert servicechargelist;
        Opp.StageName='Proposal';
        update Opp;
      Opp.StageName='Negotiation';
        update Opp;
          
    Contact con1=new Contact(LastName='Smith',Email='meghamanu06@gmail.com',AccountId=account.id,Type__c='Primary');
        insert con1;
        Contact con2=new Contact(LastName='Smith',Email='nagasai.chalamalasetti@absyz.com',AccountId=account.id,Type__c='Secondary');
        insert con2;
        RFQ_Service__c rfqservice=new RFQ_Service__c(Name='CHA',Opportunity__c=opp.id);
        insert rfqservice;
        RFQ__c rfq=new RFQ__c(Vendor_Account__c=account.id,RFQ_Service__c=rfqservice.id);
        insert rfq;
        OpportunityTeamMember oppteammember=new OpportunityTeamMember(TeamMemberRole='RFQ Manager',UserId=userinfo.getuserid(),OpportunityId=opp.id);
        insert oppteammember;
         insert new profiles__c(KPCL_Admin__c = 'KPCL RFQ User');
      
        PopulateMailSenttime.sendMail(rfq.id);   
        }
       }
   /* static testMethod void testException()
    {
        //we need to send RFQ record to sendmail method,so creating RFQ.
        Date closedDate = Date.newInstance(2018, 01, 09);
        //to test exception Account is not inserted
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake');
        insert Opp;
        opp.StageName='RFQ';
        update Opp;
        Opp.StageName='Proposal';
        update Opp;
        Opp.StageName='Negotiation';
        update Opp;
        Opp.StageName='CMS';
        update Opp;
        RFQ_Service__c rfq=new RFQ_Service__c(Name='CHA',Opportunity__c=opp.id);
        insert rfq;
        PopulateMailSenttime.sendMail(rfq.id);     
    }    */
}
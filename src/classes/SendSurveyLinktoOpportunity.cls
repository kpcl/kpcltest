/*********************************************************************************
Class Name      : SendSurveyLinktoOpportunity
Description     : Being used by SendSurveyLinktoOpportunity lightning component to email survey form link to opportunity
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
public class SendSurveyLinktoOpportunity {
    @AuraEnabled
    //checks whether the logged in user is BD Profile or not
    public static string  getProfileId(string id){
       Opportunity opp=[select OwnerId,StageName from Opportunity where id=:id];
         user profilename=[select Profile.name  from user where id=:UserInfo.getUserId()];
        profiles__c pfs=profiles__c.getOrgDefaults();
     if(opp.StageName!='CMS' && opp.StageName!='Finance' && opp.StageName!='Closed Won' && opp.StageName!='Closed Lost')
        return 'You cannot send survey at this stage of opportunity';
        else   if(profilename.Profile.name =='System Administrator' || pfs.KPCL_Admin__c==profilename.Profile.name || pfs.KPCL_Operations_User__c==profilename.Profile.name)
            return 'true';
        else
             return 'Insufficient privileges for action.';
    }
    @AuraEnabled
    public static string SendEmail(id opportunityid,string mailid){
        //declaring a variable to store error message  
        string error;
        try{
          
           
            // Fetching previous Surveytaken records if any ,also will get opportunity id from component and will fetch the mail address and send the mail using single email message class
            Opportunity opp=[select id,Email__c,NumberOfSurvey__c,(select id from Surveys_Taken__r) from Opportunity where id=:opportunityid];
             list<SurveyTaker__c> surveystaken=new list<SurveyTaker__c>();
            surveystaken=opp.Surveys_Taken__r;
          if(surveystaken.isEmpty()){       
            //querying for dummy contact
            contact  contact1=[select id,email from contact limit 1];
            EmailTemplate Et=[select id from emailTemplate where name='SurveyLink'];
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {mailid};
            message.setTreatTargetObjectAsRecipient(false);
            message.setTargetObjectId(contact1.id);  
            message.setWhatId(opportunityid);
            message.setTemplateid(Et.id);       
            Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
           //updating NumberOfSurvey field on opportunity
            if(opp.NumberOfSurvey__c==Null){
                opp.NumberOfSurvey__c=1;
                update opp;
            }
            return 'SUCCESS';
            }
            else 
                return 'Survey has already been recorded for this opportunity';
        }
        Catch(Exception e){
            return 'Server Error Please try again';
        }
         }    
}
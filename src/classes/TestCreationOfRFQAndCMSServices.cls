@isTest
public class TestCreationOfRFQAndCMSServices {
    @isTest static void testRFQAndCMSCreation(){
        Account account=new Account(name='test',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId());
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);  
        //This list is used for all inserts and updates
        list<Opportunity> Opplist=new list<Opportunity>();
        //creating three different types of opportunities to test different scenarios
        Opportunity ContainerOpp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Road;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='878',Destination_PinCode__c='767',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=3);
        Opplist.add(ContainerOpp); 
        Opportunity BulkOpp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='ICD',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Bulk',Scope_Of_Services_for_Container__c='CHA;Bagging;Vessel Chartering;Logistics;Stevedoring;Storage;Transportation by Road;Transportation by Rake;Transportation by Conveyor',Net_Weight__c=98,Source__c='899',Destination_PinCode__c='867',Type_of_Vehicle__c='Truck');
        Opplist.add(BulkOpp);
        Opportunity ContainerOpp2 = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='CFS',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA;Bagging;Ocean Freight;SDS;CFS;Storage;Fumigation;Palletization & Lashing;Transportation by Rake',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='878',Destination_PinCode__c='767',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=3);
        Opplist.add(ContainerOpp2); 
        insert Opplist; 
        Opplist.clear();
        //Testing RFQ creation method
        ContainerOpp.StageName='RFQ';
        BulkOpp.StageName='RFQ';
        ContainerOpp2.StageName='RFQ';
        Opplist.add(ContainerOpp); 
        Opplist.add(BulkOpp);
        Opplist.add(ContainerOpp2); 
        update Opplist;
        Opplist.clear();
        list<RFQ_Service__c> rfqservicelist=new list<RFQ_Service__c>();
        rfqservicelist.clear();
        //querying created rfqservices of opportunity and inserting vendor names 
        rfqservicelist=[select name,VenodorList__c from RFQ_Service__c where Opportunity__c =:ContainerOpp.id or  Opportunity__c =:BulkOpp.id];
        for(RFQ_Service__c rfqservice:rfqservicelist){
            rfqservice.VenodorList__c='ZamEngineering';
        }
        update rfqservicelist;
        //creating data required for CMS creation trigger
        list<Shipping_Type__c> shippingtypelist=new list<Shipping_Type__c>();
        Shipping_Type__c ContainerShippingtype=new Shipping_Type__c(Name='Container',Stuffing_Destuffing__c=true,Bagging__c=true);
        Shipping_Type__c BulkShippingtype=new Shipping_Type__c(Name='Bulk',Bagging__c=true);
        shippingtypelist.add(ContainerShippingtype);
        shippingtypelist.add(BulkShippingtype);
        insert shippingtypelist;
        //testing the CMS creation method
        list<RFQ_Service__c> service1 = [select id from RFQ_Service__c];
        //Creating RFQ Record for the RFQ service
        list<Rfq__c> rfqlist=new list<RFQ__c>();
        for(RFQ_Service__c rfqservice:service1){
            Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=rfqservice.id,Amount_Paid__c=50, Status__c='Approved');
            rfqlist.add(RFQ1);
        }
        insert rfqlist;
        //craeting service charge for each RFQ
        list<Service_Charge__c> servicechargelist=new  list<Service_Charge__c>();
        for(RFQ__C rfq:rfqlist){
            Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
            servicechargelist.add(Charge1);
        }
        insert servicechargelist;
        Opplist.clear();
        ContainerOpp.StageName='CMS';
        BulkOpp.StageName='CMS';
        ContainerOpp2.StageName='CMS';
        Opplist.add(ContainerOpp); 
        Opplist.add(BulkOpp);
        Opplist.add(ContainerOpp2); 
        update Opplist;
        Opplist.clear();
        system.debug('Containercmsoperations');
        list<CMS_Operations__c> Containercmsoperations=[select name from CMS_Operations__c where Opportunity__c=:ContainerOpp.id];
        list<CMS_Operations__c> Bulkcmsoperations=[select name from CMS_Operations__c where Opportunity__c=:BulkOpp.id];
        system.debug('Containercmsoperations'+Containercmsoperations);
        System.assertEquals(10, Containercmsoperations.size());
        System.assertEquals(11, Bulkcmsoperations.size());      
    }
    @isTest static void testRFQAndCMSCreationException(){
        Account account=new Account(name='test');
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);  
        //removing services field and creating exception
        Opportunity ContainerOpp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Container_SubType__c='dry',Per_TEU_Weight_in_MT__c=30,Source__c='878',Destination_PinCode__c='767',Type_of_Vehicle__c='Truck',Number_Of_20_type_Container__c=3);
        insert ContainerOpp; 
          ContainerOpp.StageName='Negotiation';
        update ContainerOpp;
        list<CMS_Operations__c> Containercmsoperations=[select name from CMS_Operations__c where Opportunity__c=:ContainerOpp.id];
        System.assertEquals(0, Containercmsoperations.size());
    }
}
/*********************************************************************************
Class Name      : displayVendorContactsController
Description     : Being used by DisplayVendorContacts Lightning Component for displaying vendor contact details for specific RFQ.
Created By      : Debasmita Rawooth
Created Date    : Dec-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Debasmita Rawooth          Dec-2017    Created controller class to fetch vendor contact details.
*********************************************************************************/

public class displayVendorContactsController {
    
    @AuraEnabled
    public static List<Contact> getVondorContacts(Id rfqId){
        
        List<Contact> vendorContacts = [Select Id,Name,Email,Phone,Type__c From Contact Where AccountId IN (Select Vendor_Account__c From RFQ__c Where Id = :rfqId)];
        return vendorContacts;
    }

}
/*********************************************************************************
Class Name      : RollUpofPaymentsHandlerClass
Description     : Being used to roll up payments to opportunity and RFQ respectively 
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
public class RollUpofPaymentsHandlerClass {
    public  void CalculateAmountonRFQ(set<id> paymentids,list<Payment__c> payments){
        try{
            String orgCurr = UserInfo.getDefaultCurrency();
			System.debug(orgCurr);
            String fromCurreny=orgCurr;
            set<id> RFQId=new set<id>();
            list<RFQ__c> updaterfqs=new list<RFQ__c>();
            for(payment__c p:payments){
                RFQId.add(p.RFQ__c);
            }       
            list<RFQ__c> rfqs=[select id,CurrencyIsoCode,Amount_Paid__c,name from RFQ__C where id in:RFQId];
            //This map will be used while iterating over aggregate result
            map<id,RFQ__c> rfqsmap=new  map<id,RFQ__c>();        
            for(RFQ__c rfq:rfqs){
                rfq.Amount_Paid__c=0;
                rfqsmap.put(rfq.id, rfq);
            }
            list<RFQ__c> rfqs1=new list<RFQ__c>();
            //getting aggregate of amount paid field from all the records with grouping by rfq id
            AggregateResult[] groupedResults = [select sum(Amount_Paid__c)Amt,RFQ__c,CurrencyIsoCode  from Payment__c  where RFQ__c in:rfqsmap.keyset() and Approved__c=true group by RFQ__c,CurrencyIsoCode];        
            if(groupedResults.size()>0){
                //iterating over the aggregate result and fetching the amount aggregate value and assigning it to amount paid in RFQ record
                for(AggregateResult ar : groupedResults){          
                    if( rfqsmap.get((string)ar.get('RFQ__c'))!=Null){
                        rfqsmap.get((string)ar.get('RFQ__c')).Amount_Paid__c+=MultiCurrencyUtil.convertCurrency(fromCurreny, rfqsmap.get((String)ar.get('RFQ__c')).CurrencyIsoCode, (Decimal)ar.get('Amt'));
                    }
                } 
                Database.update(rfqsmap.values(), false);
            }
            //in delete scenario no records will come, so setting the amount paid value to zero
            else{
                for(RFQ__c rfq:rfqs){
                    rfq.Amount_Paid__c=0;
                    updaterfqs.add(rfq);
                }
                Database.update(updaterfqs, false);
            }  
        }
        Catch(Exception e){
        }
    }  
    public void CalculateAmountonOpportunity(set<id> paymentids,list<Payment__c> payments){
        try{
            String orgCurr = UserInfo.getDefaultCurrency();
			System.debug(orgCurr);
            String fromCurreny=orgCurr;
            set<id> OppId=new set<id>();
            list<Opportunity> updateopps=new list<Opportunity>();
            for(payment__c p:payments){
                OppId.add(p.Opportunity__c);
            }
            list<Opportunity> opps=[select id,Amount_Received__c,name from Opportunity where id in:OppId];
            map<id,Opportunity> oppsmap=new  map<id,Opportunity>();        
            for(Opportunity opp:opps){
                opp.Amount_Received__c=0;
                oppsmap.put(opp.id, opp);
            }
            list<Opportunity> opps1=new list<Opportunity>(); 
            AggregateResult[] groupedResults = [select sum(Amount_Paid__c)Amt,Opportunity__c,CurrencyIsoCode from Payment__c  where Opportunity__c in:oppsmap.keyset()  group by Opportunity__c, CurrencyIsoCode];        
            if(groupedResults.size()>0){
                for(AggregateResult ar : groupedResults){          
                    if( oppsmap.get((string)ar.get('Opportunity__c'))!=Null){
                        oppsmap.get((string)ar.get('Opportunity__c')).Amount_Received__c+=MultiCurrencyUtil.convertCurrency(fromCurreny, oppsmap.get((String)ar.get('Opportunity__c')).CurrencyIsoCode, (Decimal)ar.get('Amt'));
                    }
                } 
                Database.update(oppsmap.values(), false);
            }
            else{
                for(Opportunity opp:opps){
                    opp.Amount_Received__c=0;
                    updateopps.add(opp);
                }
                Database.update(updateopps, false);
            }     
        }
        Catch(Exception e){
        }
    }
}
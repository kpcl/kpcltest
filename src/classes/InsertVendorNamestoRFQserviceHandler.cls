/*********************************************************************************
Class Name      : InsertVendorNamestoRFQserviceHandler
Description     : Being used to insert approved RFQ vendor names to respective RFQs separated by comma
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                Jan-2017           Designed as per Client requirements.
*********************************************************************************/
public class InsertVendorNamestoRFQserviceHandler {
    public static boolean firstRun1 = true;
    public void InsertVendornames(set<id> RFQServiceIds){
        map<id,RFQ_Service__c> RFQServicemap=new  map<id,RFQ_Service__c>();
        map<id,Opportunity> oppmap=new  map<id,Opportunity>();
        //query RFQ details using id list
        list<RFQ__C> RFQList=[select id,name,VendorName__c,RFQ_Service__r.VenodorList__c from RFQ__c where RFQ_Service__c in:RFQServiceIds and Status__c = 'Approved' order by RFQ_Service__c];
        //query RFQ Service details using id list
        list<RFQ_Service__c> rfqservices=[select id,VenodorList__c,Opportunity__c from RFQ_Service__c where id in:RFQServiceIds];
        //mapping RFQServices with its recpective IDs
        for(RFQ_Service__c rfqservice: rfqservices){
            //Emptying the existed vendor list ,because we are collecting all the respective RFQS and updating the vendorlist field
            rfqservice.VenodorList__c='';
            RFQServicemap.put(rfqservice.id, rfqservice);
        }
        //iterating over RFQservices and making vendorlist to null
        
        //Collecting vendor names from RFQs and inserting to RFQ Service field
        for(RFQ__c rfq:RFQList){
            string s=RFQServicemap.get(rfq.RFQ_Service__c).VenodorList__c;
            if(s!=''){
                RFQServicemap.get(rfq.RFQ_Service__c).VenodorList__c=s+','+rfq.VendorName__c;          
            }
            else{
                RFQServicemap.get(rfq.RFQ_Service__c).VenodorList__c=rfq.VendorName__c;
            }
        }
        try{
            //Preventing trigger to run multiple times
            if(firstRun1 == true){ 
                Database.update(RFQServicemap.values(), false);
                firstRun1=false;
            }   
        }
        Catch(Exception e){
            system.debug('error message'+e.getMessage());
        }
    }
}
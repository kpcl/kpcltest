/*********************************************************************************
Class Name      : ApprovedRFQstoBDTeam
Description     : Being used by ApprovedRFQstoBDTeamVFComponent to fetch all the related RFQS and Servcie charges with grouping by parent
Created By      : Naga sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga sai                 jan-2017            Design as per Client
*********************************************************************************/
public class ApprovedRFQstoBDTeam {
      public string Opportunitid{get;set;}
  
    public string getRFQsTable()
    { 
       
         /*RFQY = [SELECT id,VendorName__c,Shipping_Type2__c,Expirion_Date__c,Payment_term_to_vendor__c,Name,RFQ_Service__r.name,Terms_And_Conditions__c  FROM RFQ__c WHERE RFQ_Service__r.Opportunity__c =: Opportunitid and status__c='Approved'];      
      return RFQY; */
        
        id Oppid=Opportunitid;
            Map<string,Integer> RfqCount = new Map<string, Integer>();
      system.debug('Oppid'+Oppid);
            string tablebody = '';
            string body = '';
               List<RFQ__c> RFQList = [SELECT id,VendorName__c,Shipping_Type2__c,Expirion_Date__c,Payment_term_to_vendor__c,Name,RFQ_Service__r.name,Terms_And_Conditions__c  FROM RFQ__c WHERE RFQ_Service__r.Opportunity__c =:Opportunitid and status__c='Approved'];      
    
            for(aggregateresult ar : [select RFQ_Service__r.id RFQNAme,  count(id) Counts from RFQ__c where  RFQ_Service__r.Opportunity__c =: OppId and status__c='Approved'  group By  RFQ_Service__r.id ]){
                RfqCount.put((string)ar.get('RFQNAme'), (Integer)ar.get('Counts'));
            }
      
            string newRFQ = '';
            string oldRFQ = '';
            for(RFQ__c RFQ : RFQList){
               
               if(RFQ.Payment_term_to_vendor__c ==Null)
                   RFQ.Payment_term_to_vendor__c ='';
                 if(RFQ.Terms_And_Conditions__c ==Null)
                   RFQ.Terms_And_Conditions__c ='';
                
                   //RFQ.Expirion_Date__c =myDateTime;
                newRFQ = RFQ.RFQ_Service__c;                
                if(newRFQ == oldRFQ){
                    tablebody += '<tr><td style="padding:3px;text-align:center;">' + RFQ.Name + '</td><td style="padding:3px;text-align:center;">' + RFQ.VendorName__c + '</td><td style="padding:3px;text-align:center;">' + RFQ.Payment_term_to_vendor__c + '</td><td style="padding:3px;text-align:center;">' + RFQ.Terms_And_Conditions__c + '</td><td style="padding:3px;text-align:center;">' + (RFQ.Expirion_Date__c==Null?'None':String.valueOfGmt(RFQ.Expirion_Date__c)) + '</td></tr>';              
                }
                    else{
                     oldRFQ = newRFQ;
                    tablebody += '<tr><td style="padding:3px;text-align:center;" rowspan=" ' + RfqCount.get(RFQ.RFQ_Service__r.id) + ' ">' +RFQ.RFQ_Service__r.name + '</td><td style="padding:3px;text-align:center;">' + RFQ.Name + '</td><td style="padding:3px;text-align:center;">' + RFQ.VendorName__c + '</td><td style="padding:3px;text-align:center;">' + RFQ.Payment_term_to_vendor__c + '</td><td style="padding:3px;text-align:center;">' + RFQ.Terms_And_Conditions__c + '</td><td style="padding:3px;text-align:center;">' + (RFQ.Expirion_Date__c==Null?'None':String.valueOfGmt(RFQ.Expirion_Date__c)) + '</td></tr>';         
                        }
                }                      
            body += '<table  style = "border-collapse: collapse; border-style: solid;" align=\"center\" border = \"1\" ><tr><th style="padding:3px;text-align:center">RFQService Name</th><th style="padding:3px;text-align:center">RFQ Name</th><th style="padding:3px;text-align:center">Vendor Name</th><th style="padding:3px;text-align:center">Vendor Payment terms</th><th style="padding:3px;text-align:center">Terms And Conditions</th><th style="padding:3px;text-align:center">Expiration Date</th></tr>';
            body += tablebody;
            body += '</table>';
                return body;
    }
 
 public string getRFQserviceTable(){
      id Oppid=Opportunitid;
            Map<string,Integer> RfqCount = new Map<string, Integer>();
       
            string tablebody = '';
            string body = '';
               List<Service_Charge__c> RFQChargeList = [select  Name,RFQ_Serrvice_Name__c, Service_Sub_Category__c,Service_Category__c,UOM__c,Rate_Per_Unit__c,RFQ__c,Quantity__c,RFQ__r.Name,Total__c,RFQ__r.RFQ_Service__c,RFQ__r.RFQ_Service__r.Name,RFQ__r.VendorName__c
                                  from Service_Charge__c where RFQ__r.RFQ_Service__r.Opportunity__c =: OppId and RFQ__r.status__c='Approved'  order by RFQ__c ];
           
            for(aggregateresult ar : [select RFQ__r.id RFQNAme,  count(id) Counts from Service_Charge__c where  RFQ__r.RFQ_Service__r.Opportunity__c =: OppId  group By  RFQ__r.id ]){
                RfqCount.put((string)ar.get('RFQNAme'), (Integer)ar.get('Counts'));
            }
      
            string newRFQ = '';
            string oldRFQ = '';
     system.debug('RFQChargeList'+RFQChargeList);
            for(Service_Charge__c RFQCharge : RFQChargeList){
                if(RFQCharge.Service_Sub_Category__c==Null)
                   RFQCharge.Service_Sub_Category__c='';
                if(RFQCharge.Service_Category__c==Null)
                   RFQCharge.Service_Category__c='';
                if(RFQCharge.UOM__c==Null)
                   RFQCharge.UOM__c='';
                 if(RFQCharge.Rate_Per_Unit__c ==Null)
                   RFQCharge.Rate_Per_Unit__c =0;
                if(RFQCharge.Quantity__c ==Null)
                   RFQCharge.Quantity__c =0;               
                newRFQ = RFQCharge.RFQ__c;              
                if(newRFQ == oldRFQ){
                    tablebody += '<tr><td style="padding:3px;text-align:center;">' +RFQCharge.name+ '</td><td style="padding:3px;text-align:center;">' +RFQCharge.Service_Category__c+ '</td><td style="padding:3px;text-align:center;">' +RFQCharge.Service_Sub_Category__c+ '</td><td style="padding:3px;text-align:center;">' + RFQCharge.UOM__c + '</td><td style="padding:3px;text-align:center;">' + RFQCharge.Rate_Per_Unit__c + '</td><td style="padding:3px;text-align:center;">' + RFQCharge.Quantity__c + '</td><td style="padding:3px;text-align:center;">' + RFQCharge.Total__c + '</td></tr>';              
                }
                    else{
                     oldRFQ = newRFQ;
                    tablebody += '<tr><td style="padding:3px;text-align:center;" rowspan=" ' + RfqCount.get(RFQCharge.RFQ__r.id) + ' ">' + RFQCharge.RFQ__r.name + '</td><td style="padding:3px;text-align:center;" rowspan=" ' + RfqCount.get(RFQCharge.RFQ__r.id) + ' ">' + RFQCharge.RFQ__r.VendorName__c+ '</td><td style="padding:3px;text-align:center;">' + RFQCharge.name + '</td><td style="padding:3px;text-align:center;">' +RFQCharge.Service_Category__c+ '</td><td style="padding:3px;text-align:center;">' +RFQCharge.Service_Sub_Category__c+ '</td><td style="padding:3px;text-align:center;">' + RFQCharge.UOM__c + '</td><td style="padding:3px;text-align:center;">' + RFQCharge.Rate_Per_Unit__c + '</td><td style="padding:3px;text-align:center;">' + RFQCharge.Quantity__c + '</td><td style="padding:3px;text-align:center;">' + RFQCharge.Total__c + '</td></tr>';            
                        }
                }
               
                       
            body += '<table  style = "border-collapse: collapse; border-style: solid;" align=\"center\" border = \"1\" ><tr><th style="padding:3px;text-align:center">RFQ Name</th><th style="padding:3px;text-align:center">Vendor Name</th><th style="padding:3px;text-align:center">Charge Name</th><th style="padding:3px;text-align:center">Service Category</th><th style="padding:3px;text-align:center">Service Sub-category</th><th style="padding:3px;text-align:center">UOM</th><th style="padding:3px;text-align:center">Rate Per Unit</th><th style="padding:3px;text-align:center">Quantity</th><th style="padding:3px;text-align:center">Total</th></tr>';
            body += tablebody;
            body += '</table>';
                return body;
            
    } 
 
}
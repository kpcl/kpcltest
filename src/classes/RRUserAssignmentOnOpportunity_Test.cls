/*********************************************************************************
Class Name      : RRUserAssignmentOnOpportunity_Test
Description     : Used TO Test RRUserAssignmentOnOpportunity
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Design as per Client
*********************************************************************************/
@isTest
public class RRUserAssignmentOnOpportunity_Test {
    @isTest static Opportunity TestDataGenerator()
    {
        // Create a new email, envelope object and Attachment
        Account a = new Account(name='TestAccount');
        insert a;
        //Creating Date Instance for Closed Date (* required Field in Opportunity)
        Date closedDate = Date.newInstance(2018, 01, 09);
        //Creating Opportunity Record
        Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate,Reference_Number__c='ENQ-00000098');
        insert op;
        
        return op;
        
    }
    
    @isTest static void CheckOnUpdateRFQ()
    {
        //get RFQ member list from RFQ Team
        List<RFQ_Member__mdt>   RFMembers = [select id,userName__c from RFQ_Member__mdt];
        List<String> ListOfUserName = new List<String>();
        for (RFQ_Member__mdt m:RFMembers)
        {
            ListOfUserName.add(m.userName__c);   
        }
        //get UserList from List of Username in the team.
        System.debug('ListOfUserName'+ListOfUserName);
        List<User> UserList = [select id,name,Username from User where Username=:ListOfUserName];
        Opportunity op = RRUserAssignmentOnOpportunity_Test.TestDataGenerator();
        op.StageName='RFQ';
        update op;
        //get the opportunity records by querying op id bcoz bcoz we will auto number value by this way else it be null
        Opportunity Result = [select id,name,OwnerId,RRModInput__c from Opportunity where id=:op.Id];
        System.debug('Result'+Result);
        if(UserList.size()>0)
        {
            Integer ResultRemainder = math.mod(integer.valueof(Result.RRModInput__c),UserList.size());
            OpportunityTeamMember memberResult = [select TeamMemberRole,OpportunityId,UserId,OpportunityAccessLevel from OpportunityTeamMember where OpportunityId=:Result.Id ];
            System.assertEquals(UserList[ResultRemainder].id, memberResult.UserId);
        }
          op.StageName='Proposal';
        update op;
         op.RFQ_Unqualified_Reason__c='Test';
       op.StageName='RFQ';
        update op;
        
    }
    @isTest static void Check_On_Update_Finance_Member_Task_Assignment()
    {
        Opportunity op = RRUserAssignmentOnOpportunity_Test.TestDataGenerator();
        op.StageName='RFQ';
        update op;
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
        op.StageName='CMS';
        update op;
        op.StageName='Finance';
        update op;
        DateTime dT = System.now();
        op.Storage_Calculation_Start_Date__c = dt;
        op.Storage_Free_Period__c=5;
        update op;
      	List<Task> createdTask = [select id,ReminderDateTime,ActivityDate,Description,Subject from Task where subject like '%Storage%'];
        System.debug('createdTask'+createdTask);
        System.assertEquals(system.today()+op.Storage_Free_Period__c.intValue(),createdTask[0].ActivityDate);
        System.assertEquals(system.today()+op.Storage_Free_Period__c.intValue()-2,createdTask[0].ReminderDateTime);   
    }
    @isTest static void Check_On_Update_Finance_Member_Task_Assignment_With_No_Free_Period()
    {
        Opportunity op = RRUserAssignmentOnOpportunity_Test.TestDataGenerator();
        op.StageName='RFQ';
        update op;
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
        op.StageName='CMS';
        update op;
        op.StageName='Finance';
        update op;
        DateTime dT = System.now();
        op.Storage_Calculation_Start_Date__c = dt;
        update op;
      	List<Task> createdTask = [select id,ReminderDateTime,ActivityDate,Description,Subject from Task where subject like '%Storage%'];
        System.debug('createdTask'+createdTask);
        System.assertEquals(system.today(),createdTask[0].ActivityDate);
        System.assertEquals(system.today()-2,createdTask[0].ReminderDateTime);   
    }
    //to check the standard member allocated to the Opportunity on CMS stage and new member added based on RR logic
    @isTest static void CheckOnUpdateCMS()
    {
        //new member based on RR logic
        List<CMSMember__mdt>    CMSMembers = [select id,userName__c from CMSMember__mdt];
        List<String> ListOfUserName = new List<String>();
        for (CMSMember__mdt m:CMSMembers)
        {
            ListOfUserName.add(m.userName__c);   
        }
        System.debug('ListOfUserName'+ListOfUserName);
        List<User> UserList = [select id,name,Username from User where Username=:ListOfUserName];
        //Standard Member added to Opportunity on CMS Stage irrespective of RR logic
        List<StandardMemberOnCMSStage__mdt>    StandardCMSMembers = [select id,userName__c from StandardMemberOnCMSStage__mdt];
        ListOfUserName = new List<String>();
        for (StandardMemberOnCMSStage__mdt m:StandardCMSMembers)
        {
            ListOfUserName.add(m.userName__c);   
        }
        List<User> Standard_User_On_CMS_Stage =[select id,name,Username from User where Username=:ListOfUserName];
        System.debug('Standard_User_On_CMS_Stage :'+Standard_User_On_CMS_Stage);
        Opportunity op = RRUserAssignmentOnOpportunity_Test.TestDataGenerator();
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
        op.StageName='CMS';
        update op;
        //get the opportunity records by querying op id bcoz bcoz we will auto number value by this way else it be null
        Opportunity Result = [select id,name,OwnerId,RRModInput__c,StageName from Opportunity where id=:op.Id];
        System.debug('Result'+Result);
        if(UserList.size()>0)
        {
            Integer ResultRemainder = math.mod(integer.valueof(Result.RRModInput__c),UserList.size());
            List<OpportunityTeamMember> memberResult = [select TeamMemberRole,OpportunityId,UserId,OpportunityAccessLevel from OpportunityTeamMember where OpportunityId=:Result.Id ];
            List<Task> task_For_Member = [select id, Subject, Description from Task where Ownerid=:memberResult[1].UserId];
            if(task_For_Member.size()>0)
            {
                List<Opportunity_Task__mdt> OpportunityTask = [select id,Subject__c,StageName__c,Due_Date__c,Description__c from Opportunity_Task__mdt where StageName__c=:Result.StageName];
                if(OpportunityTask.size()>0)
                {
                    System.assertEquals(OpportunityTask[0].Subject__c+' '+op.Name,task_For_Member[0].Subject);
                    System.assertEquals(OpportunityTask[0].Description__c,task_For_Member[0].Description);
                }
            }
            
            
            System.debug('UserList:'+UserList);
            System.debug('MemberResult:'+memberResult);    
            //Check RR user inserted correctly 
            Set<id> memResult = new Set<id>();
           	for(OpportunityTeamMember x:memberResult)
            {
                memResult.add(x.UserId);
            }
            System.assertEquals(true,memResult.contains(UserList[ResultRemainder].id));
            //check standard user inserted correctly
            System.assertEquals(true,memResult.contains(Standard_User_On_CMS_Stage[0].id));
          
            
        }
    }
    
}
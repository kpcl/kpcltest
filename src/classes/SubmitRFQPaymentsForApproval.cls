/*********************************************************************************
Class Name      : SubmitRFQPaymentsForApproval
Description     : Being used by SubmitRFQPaymentsForApproval Lightning Component forSending email containing RFQ Payments.
Created By      : Madhur Behl
Created Date    : JAN-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Madhur Behl                 JAN-2018            Design as per Client
*********************************************************************************/
public class SubmitRFQPaymentsForApproval{
    @AuraEnabled
    //checks whether the logged in user is BD Profile or not
    public static string  getProfileId(){
        user u=[select Profile.name  from user where id=:UserInfo.getUserId()];
        profiles__c pfs=profiles__c.getOrgDefaults();
        if(pfs.KPCL_Finance_User__c==u.Profile.name || pfs.KPCL_Admin__c==u.Profile.name  || u.Profile.name=='System Administrator')
            return 'true';
        else 
            return 'false';
    }
    // To Send email for Approval of Payment Records
    // To Update Payment Records.
    // Calling from SubmitRFQPaymentsForApproval Lighting Component.
    @AuraEnabled
    public static List<String> SubmitForApproval(String OppID){
        
        List<String> statusList = new List<String>();
        try{
            List<Payment__c> PayToUdateList = new List<Payment__c>();	// To Hold Payments to update
            Map<string,Integer> RfqCount = new Map<string, Integer>();	// To get the count of Repeation of same RFQ
            string tablebody = ''; // To Hold Table Body
            string body = '';	// To Hold The Email Body
            string urlstr = ''+System.URL.getSalesforceBaseUrl().toExternalForm(); // To Get the Base URL
            urlstr = urlstr.substring(0,urlstr.indexOf('.'));
            // Getting all Payment Records which is not Approved
            List<Payment__c> PaymentList = [select RFQ__r.Vendor_Account__r.Name, Name, Submitted__c, Approved__c, Rejected__c, id, Amount_Paid__c, RFQ__c, RFQ__r.Due__c, RFQ__r.RFQ_Service__c, RFQ__r.RFQ_Service__r.Name,
                                            RFQ__r.Name , RFQ__r.RFQ_Service__r.Opportunity__c,RFQ__r.Amount_Paid__c,RFQ__r.Total_Amount_Payable__c, RFQ__r.RFQ_Service__r.Opportunity__r.Name, RFQ__r.RFQ_Service__r.Opportunity__r.Reference_Number__c  from Payment__c where RFQ__r.RFQ_Service__r.Opportunity__c =: OppId and RFQ__r.Status__c = 'Approved' and RFQ__r.RFQ_Service__r.Opportunity__r.StageName = 'Finance' and Approved__c != true order by RFQ__r.RFQ_Service__r.Opportunity__c];
            if(! (PaymentList.size() > 0)){
                statusList.add('ERROR');
                statusList.add('No Payment found.');
                return statusList;
            }
            // To get All the aggregate Result for Payment Records
            for(aggregateresult ar : [select RFQ__r.RFQ_Service__r.Name RFQNAme,  count(id) Counts from Payment__c where RFQ__r.Status__c = 'Approved' and RFQ__r.RFQ_Service__r.Opportunity__c =: OppId and Approved__c != true and RFQ__r.RFQ_Service__r.Opportunity__r.StageName = 'Finance' group By  RFQ__r.RFQ_Service__r.Name ]){
                RfqCount.put((string)ar.get('RFQNAme'), (Integer)ar.get('Counts'));
            }
            string newRFQ = ''; // To Hold new RFQ Name
            string oldRFQ = ''; // To Hold old RFQ Name
            string paymentNumbers = ''; // To hold all the payment numbers
            string RecieverName = 'Reciever\'s Name';
            for(user RecieverUser : [select FirstName, LastName from user where email =: Label.Payment_Approver]){
                RecieverName = RecieverUser.FirstName+' '+RecieverUser.LastNAme+', ';
            }
            // Building Email Table and Body
            for(Payment__c pay : PaymentList){
                paymentNumbers += pay.Name+',';
                pay.Submitted__c = true;
                pay.Rejected__c = false;
                pay.Approved__c = false;
                PayToUdateList.add(pay);
                newRFQ = pay.RFQ__r.RFQ_Service__r.Name;
                body = '<html><body>Hi <b>' + RecieverName + '</b><br/><br/> This mail is regarding Vendor Payment approval for the <br/>Opportunity: <b>' + '<a href='+urlstr +'.lightning.force.com/one/one.app#/sObject/'+Pay.RFQ__r.RFQ_Service__r.Opportunity__c+'/view>'+Pay.RFQ__r.RFQ_Service__r.Opportunity__r.Name+'</a>' + '</b><br/>Reference Number: <b>' + Pay.RFQ__r.RFQ_Service__r.Opportunity__r.Reference_Number__c + '</b><br/>Payment Numbers: <b>' + paymentNumbers + '</b><br/>Below section comprises the Approval Details<br/><br/>';
                if(newRFQ == oldRFQ){
                    tablebody += '<tr><td style="padding:3px;text-align:center;">' + pay.RFQ__r.Vendor_Account__r.Name + '</td><td style="padding:3px;text-align:center;">' + pay.Name + '</td><td style="padding:3px;text-align:center;">' + pay.RFQ__r.Total_Amount_Payable__c + '</td><td style="padding:3px;text-align:center">' + ( pay.Amount_Paid__c == null ? 0.00 : pay.Amount_Paid__c ) + '</td><td style="padding:3px;text-align:center;">' + pay.RFQ__r.Due__c+ '</td></tr>';
                }
                else{
                    oldRFQ = newRFQ;
                    tablebody += '<tr><td style="padding:3px;text-align:center;" rowspan=" ' + RfqCount.get(pay.RFQ__r.RFQ_Service__r.Name) + ' ">' + pay.RFQ__r.RFQ_Service__r.Name + '</td><td style="padding:3px;text-align:center;">' + pay.RFQ__r.Vendor_Account__r.Name + '</td><td style="padding:3px;text-align:center;">' + pay.Name + '</td><td style="padding:3px;text-align:center;">' + pay.RFQ__r.Total_Amount_Payable__c + '</td><td style="padding:3px;text-align:center">' + ( pay.Amount_Paid__c == null ? 0.00 : pay.Amount_Paid__c ) + '</td><td style="padding:3px;text-align:center;">' + pay.RFQ__r.Due__c + '</td></tr>';
                }
            }
            // Updating Payment Records
            List<Database.SaveResult> URList = Database.update(PayToUdateList, False);
            for(Database.SaveResult DUR : URList){
                if(!DUR.isSuccess()){
                    statusList.add('ERROR');
                    for(Database.Error ex : DUR.getErrors()){
                        statusList.add(ex.getMessage());
                    }
                    return statusList;
                }
                else{
                }
            }
            // Formation of email Body.
            body += '<table  style = "border-collapse: collapse; border-style: solid;" align=\"center\" border = \"1\" ><tr><th style="padding:3px;text-align:center">RFQ Service Name</th><th style="padding:3px;text-align:center">Vendor Name</th><th style="padding:3px;text-align:center">Payment Number</th><th style="padding:3px;text-align:center" >Total Amount Payable</th><th style="padding:3px;text-align:center" >Amount To Pay</th><th style="padding:3px;text-align:center" >Due</th></tr>';
            body += tablebody;
            body += '</table><br/><br/> If you want to approve or reject the Payments send reply to this email writing <b>Approve/Approved</b> or <b>Reject/Rejected</b> (is not case-sensitive). You can add comments by writing <b>Comment/Comments</b> and colon(:) after it. Add your text and end comments with <b>;</b>';
            // To Send email
            list < Messaging.SingleEmailMessage > EmailToSendList = new list < Messaging.SingleEmailMessage > (); 
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            string repl = Label.RFQPaymentApprovalEmailService;
            list < string > userEmail = new list < string > ();
            userEmail.add(system.Label.Payment_Approver);
            emailToSend.setToAddresses(userEmail);
            emailToSend.setHTMLBody(body);
            emailToSend.setReplyTo(repl);
            emailToSend.setSenderDisplayName('Vishwasamudra-Finance Team');
            emailToSend.setSubject(PaymentList[0].RFQ__r.RFQ_Service__r.Opportunity__r.Name+'Request for Vendor Payment Approvals');
            EmailToSendList.add(emailToSend);
            Messaging.SendEmailResult[] sentmailresult = Messaging.sendEmail(EmailToSendList);
            statusList.add('SUCCESS');
        }
        catch(Exception ex){
            statusList.add('ERROR');
            statusList.add(ex.getMessage());
            return statusList;
        }	
        return statusList;
    }
}
/*********************************************************************************
Class Name      : SubmitRFQPaymentsForApprovalTest
Description     : Used TO Test SubmitRFQPaymentsForApproval
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Design as per Client
*********************************************************************************/

@isTest
public class SubmitRFQPaymentsForApprovalTest {
    //To Test will All Valid Records (Best Case)
    @isTest static void withAllCorrectRecords(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias = 'standt', Email='gokul.rajan@absyz.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='test9677@test.com');
        insert u;
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account a=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert a;
        //Creating Date Instance for Closed Date (* required Field in Opportunity)
        Date closedDate = Date.newInstance(2018, 01, 09);
        //Creating Opportunity Record
        Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate,OwnerId=u.Id,Reference_Number__c='ENQ-00000098');
        insert op;
        op.StageName='Proposal';
        update op;
        op.StageName='Negotiation';
        update op;
        op.StageName='CMS';
        update op;
        op.StageName='Finance';
        update op;
        //Creating RFQ Service Record
        RFQ_Service__c service1 = new RFQ_Service__c(Name='service1',Opportunity__c=op.Id);
        insert service1;
        //Creating RFQ Record for the RFQ service
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Rfq__c RFQ2 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ2;
        //creating Payment Record
        Payment__c payment1 = new Payment__c(Amount_Paid__c=50,RFQ__c=RFQ1.id,Submitted__c=false);
        insert payment1;
        Payment__c payment2 = new Payment__c(Amount_Paid__c=50,RFQ__c=RFQ1.id,Submitted__c=false);
        insert payment2;
        //Pass Opportunity Id
        List<String> status=SubmitRFQPaymentsForApproval.SubmitForApproval(op.Id);
        Payment__c result = [select id,name,Submitted__c from Payment__c where id=:payment1.id];
        System.assertEquals(true,result.Submitted__c);
        
    }
    //To Test with Invalid Records(WorstCase)
    @isTest static void withInCorrectRecords(){
        //Account Record Creation 
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account a=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert a;
        //Creating Date Instance for Closed Date (* required Field in Opportunity)
        Date closedDate = Date.newInstance(2018, 01, 09);
        //Creating Opportunity Record
        Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,StageName='Qualification',CloseDate=closedDate);
        insert op;
        //Creating RFQ Service Record
        RFQ_Service__c service1 = new RFQ_Service__c(Name='service1',Opportunity__c=op.Id);
        insert service1;
        //Creating RFQ Record for the RFQ service
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Rfq__c RFQ2 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ2;
        //creating Payment Record
        Payment__c payment1 = new Payment__c(Amount_Paid__c=50,RFQ__c=RFQ1.id,Submitted__c=false);
        insert payment1;
        Payment__c payment2 = new Payment__c(Amount_Paid__c=50,RFQ__c=RFQ1.id,Submitted__c=false);
        insert payment2;
        List<String> status=SubmitRFQPaymentsForApproval.SubmitForApproval(op.Id);
        Payment__c result = [select id,name,Submitted__c from Payment__c where id=:payment1.id];
        
        System.assertEquals(false,result.Submitted__c);
        
        
    }
    @isTest static void RecordsToCauseUpdateException(){
        //Opportunity Record Creation 
        list<RecordType> AccountRecordTypeList = [SELECT Id,name FROM RecordType WHERE SObjectType='Account' and name='Vendor'];
        Account a=new Account(name='test',recordtypeid=AccountRecordTypeList[0].id);
        insert a;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity op = new Opportunity(Name='HipHop-',AccountId=a.Id,Amount_Received__c=10000,StageName='Qualification',CloseDate=closedDate);
        insert op;
        RFQ_Service__c service1 = new RFQ_Service__c(Name='service1',Opportunity__c=op.Id,TotalQuotetoCustomer__c=50);
        insert service1;
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Rfq__c RFQ2 = new Rfq__c(Vendor_Account__c=a.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ2;
        Payment__c payment1 = new Payment__c(Amount_Paid__c=500000,RFQ__c=RFQ1.id,Submitted__c=false);
        insert payment1;
        Payment__c payment2 = new Payment__c(Amount_Paid__c=500000,RFQ__c=RFQ1.id,Submitted__c=false);
        insert payment2;
        payment2.Amount_Paid__c=-5;
        List<String> status=SubmitRFQPaymentsForApproval.SubmitForApproval(op.Id);
        //Payment__c result = [select id,name,Submitted__c from Payment__c where id=:payment1.id];
        //System.assertEquals(false,result.Submitted__c);
        
    }
    
}
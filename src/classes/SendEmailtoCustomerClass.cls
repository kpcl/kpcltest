/*********************************************************************************
Class Name      : SendEmailtoCustomerClass
Description     : Being used to  Send Email to Customer 
Created By      : Naga Sai
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Naga Sai                jan-2017            Design as per Client
*********************************************************************************/
public class SendEmailtoCustomerClass {
    @AuraEnabled
    //checks whether the logged in user is BD Profile or not
    public static string  getProfileId(){
        user u=[select Profile.name  from user where id=:UserInfo.getUserId()];
          if(Label.BDProfileName==u.Profile.name)
            return 'true';
        else 
            return 'false';
    }
    @AuraEnabled
    public static string  sendMail(Id oppId,String mailId,string ccmailids)
    {
        string status;
        try{
            //querying for dummy contact
            contact  contact1=[select id,email from contact limit 1];
            EmailTemplate Et=[select id from emailTemplate where name='FinalQuoteToCustomer'];
            system.debug(oppId);
            String[] toAddresses=new  String[]{};
                String[] CCAddresses=new  String[]{};
                    Messaging.SingleEmailMessage email=new  Messaging.SingleEmailMessage();
            //splitting mailids and ading them to toaddresses
            if(ccmailIds!=Null){
            String[] ccmails=ccmailIds.split(',');
         for(string mail:ccmails){
                CCAddresses.add(mail);
            }
            }
            if(mailId!=Null){
             String[] mails=mailId.split(',');
            for(string mail:mails){
                toAddresses.add(mail);
            }
            }
            //placing current user default in CC
              user u=[select email from user where id=:userinfo.getUserId()];
            CCAddresses.add(u.email);
            //querying file
            list<contentversion> c=[select id from contentversion where title like '%_Finalquote_v%' and FirstPublishLocationId = :oppId ORDER By ContentModifiedDate DESC limit 1];            
            list<id> d=new list<id>(); 
            //if no content document then stopping further process
            if(c.size()==0){
                // return 'No file with name starting with Final found';
                status='File Not Found';
            }
            if(c.size()>0)   
            {
                //adding all contentdocumnet ids to a set
                for(contentversion c1: c){
                    d.add(c1.id);
                }
                email.setTreatTargetObjectAsRecipient(false);
                email.setTargetObjectId(contact1.id);  
                email.setWhatId(oppId);
                email.setSaveAsActivity(false);
                email.setToAddresses(toAddresses);
                if(CCAddresses.size()>0)
                email.setCcAddresses(CCAddresses);
                email.setTemplateid(Et.id);               
                email.setEntityAttachments(d);        
                //email.setFileAttachments(new  Messaging.EmailFileAttachment[] {efa});
                Messaging.SendEmailResult[] results =Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});             
                status='SUCCESS';                
            }              
        }
        catch(Exception e){            
            status='Enter Email Addresses separated by comma';
        } 
        return status;
    }
}
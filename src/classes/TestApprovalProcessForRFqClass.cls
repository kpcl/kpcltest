/*********************************************************************************
Class Name      : TestApprovalProcessForRFqClass
Description     : Being used to test ApprovalProcessForRFqClass 
Created By      : Megha Manu
Created Date    : jan-2017
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Megha  Manu                jan-2017            Designed as per client requirements.
*********************************************************************************/
@isTest
public class TestApprovalProcessForRFqClass {
    static testMethod void testsendMail()
    {
        //inserting opportuntiy
        Account account=new Account(name='test',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId());
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA',Container_SubType__c='dry',Number_Of_20_type_Container__c=3,Per_TEU_Weight_in_MT__c=30);
        insert Opp;
        //changing Opportunity stage to CMS Successively
        opp.StageName='RFQ';
        update Opp;
        RFQ_Service__c service1 = [select id from RFQ_Service__c where opportunity__c=:opp.id limit 1];
        //Creating RFQ Record for the RFQ service
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ1.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
        insert charge1;
        Opp.StageName='Proposal';
        update Opp;
        Opp.StageName='Negotiation';
        update Opp;
        Contact con=new Contact(LastName='Smith',AccountId=account.id);
        insert con;
        //inserting file
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Customersquoteworksheet_v',PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true,FirstPublishLocationId =Opp.id);
        insert contentVersion_1;
        ApprovalProcessForRFqClass.sendMail(Opp.id);
        ApprovalProcessForRFqClass.getProfileId();
    }
    static testMethod void testNoFileFoundCase()
    {
        Account account=new Account(name='test',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId());
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA',Container_SubType__c='dry',Number_Of_20_type_Container__c=3,Per_TEU_Weight_in_MT__c=30);
        insert Opp;
        //changing Opportunity stage to CMS Successively
        opp.StageName='RFQ';
        update Opp;
         RFQ_Service__c service1 = [select id from RFQ_Service__c where opportunity__c=:opp.id limit 1];
        //Creating RFQ Record for the RFQ service
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ1.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
        insert charge1;
        Opp.StageName='Proposal';
        update Opp;
        Opp.StageName='Negotiation';
        update Opp;
        Contact con=new Contact(LastName='Smith',AccountId=account.id);
        insert con;
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = '',PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true,FirstPublishLocationId =Opp.id);
        insert contentVersion_1;
        ApprovalProcessForRFqClass.sendMail(Opp.id);
    }
    static testMethod void testException()
    {
        Account account=new Account(name='test',recordtypeid=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId());
        insert account;
        Date closedDate = Date.newInstance(2018, 01, 09);
        Opportunity Opp = new Opportunity(Name='test',Shipping_Options__c='Export',Stuffing_Method__c='Port',AccountId=account.id,StageName='Qualification',CloseDate=closedDate,Shipping_Type__c='Container',Scope_Of_Services_for_Container__c='CHA',Container_SubType__c='dry',Number_Of_20_type_Container__c=3,Per_TEU_Weight_in_MT__c=30);
        insert Opp;
        //changing Opportunity stage to CMS Successively
        opp.StageName='RFQ';
        update Opp;
         RFQ_Service__c service1 = [select id from RFQ_Service__c where opportunity__c=:opp.id limit 1];
        //Creating RFQ Record for the RFQ service
        Rfq__c RFQ1 = new Rfq__c(Vendor_Account__c=account.Id,RFQ_Service__c=service1.id,Amount_Paid__c=50, Status__c='Approved');
        insert RFQ1;
        Service_Charge__c Charge1 = new Service_Charge__c(Rate_Per_Unit__c=10,Quantity__c=100,Margin_Per_Unit__c=5,RFQ__c=RFQ1.Id,RecordTypeId=Schema.SObjectType.Service_Charge__c.getRecordTypeInfosByName().get('Service Charge for RFQ').getRecordTypeId());
        insert charge1;
        Opp.StageName='Proposal';
        update Opp;
        Opp.StageName='Negotiation';
        update Opp;
        ApprovalProcessForRFqClass.sendMail(account.id);
    }
    
}
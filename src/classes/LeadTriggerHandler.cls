/*********************************************************************************
Class Name      : LeadTriggerHandler
Description     : Trigger Handler for Lead which has functionality for assigning owner id
				  to Lead based on Round Robin Logic.
Created By      : Gokul Rajan
Created Date    : Jan-2018
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Gokul Rajan                 Jan-2018            Design as per Client
*********************************************************************************/
public class LeadTriggerHandler {
    
    public static void Assign_LeadOwner_BasedOn_RRLogic_OnInsert(Lead newLead)
    {
        //query Lead details
        Lead newlyInsertedLead = [select id,RRModInput__c,OwnerId,name from Lead where id=:newLead.Id];
        
        //get List of Users belonging to the Member queue defined in Custom Metadata Types
        List<User>  UserList = RRUserAssignmentOnOpportunity.getList_Of_User_From_This_Custom_MetaData('LeadOwnerMember__mdt');
        if(UserList.size()>0)
        {
            Integer ResultRemainder = math.mod(integer.valueof(newlyInsertedLead.RRModInput__c),UserList.size());
            //Assign the RR Result Value to User list to get the user id and assign it to LeadOwner id 
            newlyInsertedLead.OwnerId = UserList[ResultRemainder].id;
        }
        update newlyInsertedLead;
    }
    public static void Assign_LeadOwner_BasedOn_RRLogic_OnUpdate_if_Pass_to_BD_Team_Selected(Lead newLead)
    {
        //query Lead details
       // Lead newlyInsertedLead = [select id,RRModInput__c,OwnerId,name from Lead where id=:newLead.Id];
        manualShareRead(newLead.Id,newLead.OwnerId);
        //get List of Users belonging to the Member queue defined in Custom Metadata Types
        List<User>  UserList = RRUserAssignmentOnOpportunity.getList_Of_User_From_This_Custom_MetaData('BD_Member__mdt');
        if(UserList.size()>0)
        {
            Integer ResultRemainder = math.mod(integer.valueof(newLead.RRModInput__c),UserList.size());
            //Assign the RR Result Value to User list to get the user id and assign it to LeadOwner id 
            newLead.OwnerId = UserList[ResultRemainder].id;
        }
    }
    public static void Assign_LeadOwner_BasedOn_RRLogic_OnInsert_if_Pass_to_BD_Team_Selected(Lead newLead)
    {
        Lead newlyInsertedLead = [select id,RRModInput__c,OwnerId,name from Lead where id=:newLead.Id];
        
        //get List of Users belonging to the Member queue defined in Custom Metadata Types
        List<User>  UserList = RRUserAssignmentOnOpportunity.getList_Of_User_From_This_Custom_MetaData('BD_Member__mdt');
        if(UserList.size()>0)
        {
            Integer ResultRemainder = math.mod(integer.valueof(newlyInsertedLead.RRModInput__c),UserList.size());
            //Assign the RR Result Value to User list to get the user id and assign it to LeadOwner id 
            newlyInsertedLead.OwnerId = UserList[ResultRemainder].id;
        }
        update newlyInsertedLead;
    }
    
    public static boolean manualShareRead(Id recordId, Id userOrGroupId){
      // Create new sharing object for the custom object Job.
      LeadShare  leadShare  = new LeadShare ();
   
      // Set the ID of record being shared.
      leadShare.LeadId = recordId;
        
      // Set the ID of user or group being granted access.
      leadShare.UserOrGroupId = userOrGroupId;
        
      // Set the access level.
      leadShare.LeadAccessLevel = 'Read';
        
      // Set rowCause to 'manual' for manual sharing.
      // This line can be omitted as 'manual' is the default value for sharing objects.
      leadShare.RowCause = Schema.LeadShare.RowCause.Manual;
        
      // Insert the sharing record and capture the save result. 
      // The false parameter allows for partial processing if multiple records passed 
      // into the operation.
      Database.SaveResult sr = Database.insert(leadShare,false);

      // Process the save results.
      if(sr.isSuccess()){
         // Indicates success
         return true;
      }
      else {
         // Get first save result error.
         Database.Error err = sr.getErrors()[0];
         
         // Check if the error is related to trival access level.
         // Access level must be more permissive than the object's default.
         // These sharing records are not required and thus an insert exception is acceptable. 
         if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                  err.getMessage().contains('AccessLevel')){
            // Indicates success.
            return true;
         }
         else{
            // Indicates failure.
            return false;
         }
       }
   }
}
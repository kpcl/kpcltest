public class Time_Sheet_Entry_Trigger_Handler {
public static void prevent_More_Than_9_Hours(Time_Sheet_Entry__c Record)
{
    TimeSheet__c parentRecord = [select id,Extra_hours__c, Total_Hours__c from TimeSheet__c where id=:Record.TimeSheet__c];
    Decimal extraHours = (parentRecord.Extra_hours__c!=null?parentRecord.Extra_hours__c:0);
    Decimal TotalHours = (parentRecord.Total_Hours__c!=null?parentRecord.Total_Hours__c:0);
    Decimal TimeSheet_Total_time = extraHours+TotalHours;
    TimeSheet_Total_time = (TimeSheet_Total_time==0?9:TimeSheet_Total_time);
    AggregateResult[] groupedResults = [select TimeSheet__c,sum(Duration_In_Hours__c)Duration from Time_Sheet_Entry__c where TimeSheet__c=:Record.TimeSheet__c group by TimeSheet__c];
	system.debug('********'+groupedResults); 
    if(groupedResults.size()>0){
                for(AggregateResult a:groupedResults)
                {
                  Decimal Duration = (Decimal)a.get('Duration');
                  if(Duration!=null)
                  {
                    Decimal total = Duration+Record.Duration_In_Hours__c;
                    if(total>TimeSheet_Total_time)
                    {
                        Record.addError('Total Number of Hours Cant be more than'+TimeSheet_Total_time);
                    }
                  }
                    else
                    {
                        if(Record.Duration_In_Hours__c>TimeSheet_Total_time){
                            Record.addError('Total Number of Hours Cant be more than'+TimeSheet_Total_time);
                        }
                        
                    }
				}
				}
}
public static void prevent_More_Than_9_Hours_in_entry(Map<id,Time_Sheet_Entry__c> oldMap,Map<id,Time_Sheet_Entry__c> newMap)
{
    Map<Id,String> TimesheetAndEntryMapping = new Map<Id,String>();
    for(Id t:newMap.keyset())
    {
    TimesheetAndEntryMapping.put(t,(String)newMap.get(t).TimeSheet__c);    
    }
    List<TimeSheet__c> parentRecord = [select id,Extra_hours__c, Total_Hours__c from TimeSheet__c where id=:TimesheetAndEntryMapping.values()];
    Map<Id,Decimal> ParentRecord_TotalTime = new Map<Id,Decimal>();
    for(TimeSheet__c x:parentRecord)
    {
    Decimal extraHours = (x.Extra_hours__c!=null?x.Extra_hours__c:0);
    Decimal TotalHours = (x.Total_Hours__c!=null?x.Total_Hours__c:0);
    Decimal TimeSheet_Total_time = extraHours+TotalHours;
    ParentRecord_TotalTime.put(x.Id,TimeSheet_Total_time);
    }
    AggregateResult[] groupedResults = [select TimeSheet__c,sum(Duration_In_Hours__c)Duration from Time_Sheet_Entry__c where TimeSheet__c in:ParentRecord_TotalTime.keyset() group by TimeSheet__c];
	if(groupedResults.size()>0)
    			{
                for(AggregateResult a:groupedResults)
                {
                    Decimal TimesheetTotalTime =  ParentRecord_TotalTime.get((Id)a.get('TimeSheet__c'));
                    Decimal Duration = (Decimal)a.get('Duration');
                    if(Duration!=null)
                  	{
					List<Id> ts_EntryId = getKeyByValue(TimesheetAndEntryMapping,(String)a.get('TimeSheet__c'));	
                        for(Id t:ts_EntryId)
                        {
                            Time_Sheet_Entry__c oldRec = oldMap.get(t);
                            Time_Sheet_Entry__c newRec = newMap.get(t);
                            Decimal Total = (Duration-(oldRec.Duration_In_Hours__c==null?0:oldRec.Duration_In_Hours__c))+newRec.Duration_In_Hours__c;
                            if(total>ParentRecord_TotalTime.get((Id)a.get('TimeSheet__c')))
                            {
                                newMap.get(t).addError('Total Number of Hours Cant be more than'+ParentRecord_TotalTime.get((Id)a.get('TimeSheet__c')));
                            }
                        }
                       
                  	}
                    else
                    {
                        List<Id> ts_EntryId = getKeyByValue(TimesheetAndEntryMapping,(String)a.get('TimeSheet__c'));	
                        for(Id t:ts_EntryId)
                        {
                    
                        Time_Sheet_Entry__c newRec = newMap.get(t);
                        if(newRec.Duration_In_Hours__c>ParentRecord_TotalTime.get((Id)a.get('TimeSheet__c')))
                        {
                                newMap.get(t).addError('Total Number of Hours Cant be more than'+ParentRecord_TotalTime.get((Id)a.get('TimeSheet__c')));
                        }
                        }
                        
                    }
                    
                }
    }
}
    public static List<Id> getKeyByValue(Map<id,String> mymap,String value)
    {
        List<Id> returnID = new List<ID>();
        for(Id s:mymap.keySet())
        {
            if(mymap.get(s).contains(value))
            {
                returnID.add(s);
            }
            
        }
        return returnID;
    }

}
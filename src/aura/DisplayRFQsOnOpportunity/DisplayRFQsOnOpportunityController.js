({
    submit : function(component, event, helper) {
      helper.helperMethod(component, event, helper)
    },    
    navigateToCHARecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        alert(idx);
        var contact = component.get("v.CHARFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire();         
    },
    navigateToOceanFreightRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.OceanFreightRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
        
    },
     navigateToSDSRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var RFquote = component.get("v.SDSRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: RFquote.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
        
    },
    navigateToCharteringRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var RFquote = component.get("v.CharteringRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");        
        navEvent.setParams({
            recordId: RFquote.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
        
    },
    navigateToLogisticsServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.LogisticsRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
     navigateToSteveDoringServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.SteveDoringRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
    navigateToCFSServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.CFSRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
    navigateToBaggingServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.BaggingRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
    navigateToStorageServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.StorageRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
    navigateToFumigationServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.FumigationRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
    navigateToPalletizationLashingServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.PalletizationLashingRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
    navigateToTransportationbyRoadServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.TransportationbyRoadRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
        
    },
    navigateToTransportationbyRakeServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.TransportationbyRakeRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
               navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire();         
    },
    navigateToTransportationbyConveyorServicesRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.TransportationbyConveyorRFQs")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
               navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire();         
    },
   

})
({
	helperMethod : function(component, event, helper) {
		var action = component.get("c.getRFQ");
        var id = component.get("v.recordId");  
        var RFQ=component.get("v.RFQ");
        action.setParams({ 
            "OppId" : id
        });       
        action.setCallback(this, function(response) {
            var state = response.getState();           
            if (state === "SUCCESS") {               
                var CHA = [];
                var OceanFreight = [];
                var SDS = [];
                var Chartering = [];
                var Logistics = [];
                var Stevedoring =[];
                var CFS=[];
                var Bagging=[];
                var Storage=[];
                var Fumigation=[];
                var PalletizationLashing=[];
                var TransportationbyRoad=[];
                var TransportationbyRake=[];
                var TransportationbyConveyor=[];
                var StoreResponse = response.getReturnValue();               
                component.set("v.CHARFQs",response.getReturnValue());
                for (var i=0;i<StoreResponse.length;i=i+1 ) {                 
                    RFQ=StoreResponse[i];                    
                    var name=RFQ.RFQ_Service__r.Name ;                   
                    if(name==='CHA')
                        CHA.push(RFQ);
                    if(name==='Ocean Freight')
                        OceanFreight.push(RFQ);
                    if(name==='SDS')
                        SDS.push(RFQ);
                    if(name==='Vessel Chartering')
                        Chartering.push(RFQ);
                    if(name==='Logistics')
                        Logistics.push(RFQ);
                    if(name==='Stevedoring')
                        Stevedoring.push(RFQ);
                    if(name==='CFS')
                        CFS.push(RFQ);
                    if(name==='Bagging')
                        Bagging.push(RFQ);
                    if(name==='Storage')
                        Storage.push(RFQ);
                    if(name==='Fumigation')
                        Fumigation.push(RFQ);
                    if(name==='PalletizationLashing')
                        PalletizationLashing.push(RFQ);
                    if(name==='Transportation by Road')
                        TransportationbyRoad.push(RFQ);
                    if(name==='Transportation by Rake')
                        TransportationbyRake.push(RFQ);
                    if(name==='Transportation by Conveyor')
                        TransportationbyConveyor.push(RFQ);
                }
                component.set("v.CHARFQs",CHA);
                component.set("v.CHAlength",CHA.length);
                component.set("v.OceanFreightRFQs",OceanFreight);
                component.set("v.OceanFreightlength",OceanFreight.length);
                component.set("v.SDSRFQs",SDS);
                component.set("v.SDSlength",SDS.length);
                component.set("v.CharteringRFQs",Chartering);
                component.set("v.Charteringlength",Chartering.length);
                component.set("v.LogisticsRFQs",Logistics);
                component.set("v.Logisticslength",Logistics.length);
                component.set("v.SteveDoringRFQs",Stevedoring);
                component.set("v.SteveDoringlength",Stevedoring.length);
                component.set("v.CFSRFQs",CFS);
                component.set("v.CFSlength",CFS.length);
                component.set("v.BaggingRFQs",Bagging);
                component.set("v.Bagginglength",Bagging.length);
                component.set("v.StorageRFQs",Storage);
                component.set("v.Storagelength",Storage.length);
                component.set("v.FumigationRFQs",Fumigation);
                component.set("v.Fumigationlength",Fumigation.length);
                component.set("v.PalletizationLashingRFQs",PalletizationLashing);
                component.set("v.PalletizationLashinglength",PalletizationLashing.length);
                component.set("v.TransportationbyRoadRFQs",TransportationbyRoad);
                component.set("v.TransportationbyRoadlength",TransportationbyRoad.length);
                component.set("v.TransportationbyRakeRFQs",TransportationbyRake);
                component.set("v.TransportationbyRakelength",TransportationbyRake.length);
                component.set("v.TransportationbyConveyorRFQs",TransportationbyConveyor);
                component.set("v.TransportationbyConveyorlength",TransportationbyConveyor.length);                
            }         
        });      
        $A.enqueueAction(action);   
	}
})
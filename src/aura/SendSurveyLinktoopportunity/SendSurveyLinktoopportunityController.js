({
    //To validate profile
    doInit: function(component, event, helper) {
        var getprofileStatus = component.get("c.getProfileId");  
         getprofileStatus.setParams({
            'id': component.get("v.recordId"),
                    });
        getprofileStatus.setCallback(this, function(response) {
            var profile= response.getReturnValue();
            console.log("profiles"+profile);
            component.set("v.profile",profile);
            //if logged in user profile doesnt have privileges
            if(profile=='You cannot send survey at this stage of opportunity' ||profile=='Insufficient privileges for action.'){
                //Toggle display
                var toggleText = component.find("text");
                $A.util.toggleClass(toggleText, "toggle");
                //Close QuickAction
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": profile,
                    type: 'error'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(getprofileStatus);
    }, 
    openActionWindow : function(component, event, helper) {
       var validemail =true;
        var emailField = component.find("mail");
         var emailFieldValue =emailField.get("v.value");
         var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
      if(($A.util.isEmpty(emailFieldValue))){
            component.set("v.IsInValidEmail","True");	
                validemail=false;
         }
        else{
         if(!(emailFieldValue.match(regExpEmailformat))){
            component.set("v.IsInValidEmail","true"); 
        validemail=false;
        }
            if(emailFieldValue.match(regExpEmailformat)){
  component.set("v.IsInValidEmail","false");	
     }
    // if Email Address is valid then execute code     
       if(validemail){
         // code write here..if Email Address is valid. 
          var action = component.get("c.SendEmail");
        //setting Opportunity id and input email
        action.setParams({
            'opportunityid': component.get("v.recordId"),
            'mailid': component.find("mail").get("v.value")
        });
        action.setCallback(this, function(response) {
            var state = response.getReturnValue();
            //for success
            if (state === "SUCCESS")
            {
                //closing quickaction 
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                //firing toast event
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Email Sent successfully.",
                    type: 'success'
                });
                toastEvent.fire();
            }
            else
            {
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                var toastEvent= $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": state,
                    type: 'error'
                });
                toastEvent.fire();
            }
        });
  $A.enqueueAction(action);
       }
        //calling sendemail method   
        }
    },
    closewindow : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})
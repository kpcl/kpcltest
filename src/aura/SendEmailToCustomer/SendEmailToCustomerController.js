({
    //To validate profile
    doInit: function(component, event, helper) {
        var getprofileStatus = component.get("c.getProfileId");  
        getprofileStatus.setCallback(this, function(response) {
            var profile= response.getReturnValue();
            component.set("v.profile",profile);
            //if logged in user profile doesnt have privileges
            if(profile=='false'){
                //Toggle display
                var toggleText = component.find("text");
                $A.util.toggleClass(toggleText, "toggle");
                //Close QuickAction
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Insufficient privileges for action.",
                    type: 'error'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(getprofileStatus);
    }, 
    sendMails : function(component, event, helper) {
        component.set("v.IsInValidEmail","false");	
        component.set("v.IsEmptyEmail","false");	
        var validemail =true;
        var emailField=component.find("mail");
        var CCemailField =component.find("ccmail");
        var CCemailFieldValue =CCemailField.get("v.value");
        var emailFieldValue =emailField.get("v.value");
        if(! ($A.util.isEmpty(CCemailFieldValue)) && !($A.util.isEmpty(emailFieldValue))){
            var emailFieldValues=emailFieldValue + "," +CCemailFieldValue;
        }
        if( ($A.util.isEmpty(CCemailFieldValue)) && ! ($A.util.isEmpty(emailFieldValue))){
            var emailFieldValues=emailFieldValue;
        }
        if( ($A.util.isEmpty(emailFieldValue)) && ! ($A.util.isEmpty(CCemailFieldValue))){
            var emailFieldValues= CCemailFieldValue;
        }
        
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
        if(($A.util.isEmpty(emailFieldValue)) && ($A.util.isEmpty(CCemailFieldValue))){
            component.set("v.IsEmptyEmail","True");	
            validemail=false;
        }
        
        else{
            
            var emailArray = emailFieldValues.split(",");
            for(var i= 0; i <= (emailArray.length - 1); i++){
                if(!(emailArray[i].match(regExpEmailformat))){
                    component.set("v.IsInValidEmail","true"); 
                    validemail=false;
                    
                }
            }
            
            
            // if Email Address is valid then execute code     
            if(validemail){
                // code write here..if Email Address is valid. 
                var mail =  component.find("mail").get("v.value");
                var ccmail =  component.find("ccmail").get("v.value");
                //validating empty input
                var sendAction = component.get("c.sendMail");
                var id = component.get("v.recordId"); 
                sendAction.setParams({ 
                    "oppId" : id,
                    "mailId" : mail,
                    "ccmailids":ccmail
                });
                sendAction.setCallback(this, function(response) {
                    var state = response.getReturnValue();
                    //checking for responce
                    if (state === "SUCCESS") 
                    {
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "Email Sent successfully.",
                            type: 'success'
                        });
                        
                        toastEvent.fire();
                    }
                    else
                    {
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                        var toastEvent= $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": state,
                            type: 'error'
                        });
                        toastEvent.fire();
                    }
                });
                $A.enqueueAction(sendAction);
            }
        }
    },
    closewindow : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})
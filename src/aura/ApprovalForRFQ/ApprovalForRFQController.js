({
    doInit: function(component, event, helper) {
        var getprofileStatus = component.get("c.getProfileId");  
        getprofileStatus.setCallback(this, function(response) {
            var profile= response.getReturnValue();
            component.set("v.profile",profile);
            if(profile=='false'){
                var toggleText = component.find("text");
                $A.util.toggleClass(toggleText, "toggle");
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Insufficient privileges for action.",
                    type: 'error'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(getprofileStatus);
    },
    SubmitForApproval : function(component, event, helper) {
        var action = component.get("c.sendMail");
        var id = component.get("v.recordId");   
        action.setParams({ 
            "oppId" : id
        });      
        action.setCallback(this, function(response) {
            var state = response.getReturnValue();
            if (state == "Submitted") {
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Record submitted for approval.",
                    type: 'success'
                });
                toastEvent.fire();
            }
            else
            {
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": state,
                    type: 'error'
                });
                toastEvent.fire();
            }           
        });
        $A.enqueueAction(action);
    },
    closewindow : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})
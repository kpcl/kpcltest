({
    doInit : function(component, event, helper) {
        var action = component.get("c.getVondorContacts");
        var id = component.get("v.recordId");
        
        action.setParams({ 
            "rfqId" : id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();   
            if (state === "SUCCESS") {
                
                component.set("v.contacts",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    navigateToRecord : function(component, event, helper) {
        var idx = event.target.getAttribute('data-index');
        var contact = component.get("v.contacts")[idx];
        var navEvent = $A.get("e.force:navigateToSObject");
        
        navEvent.setParams({
            recordId: contact.Id,
            slideDevName: "detail"
        });
        navEvent.fire(); 
    }
})
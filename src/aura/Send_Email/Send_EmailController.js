({
   openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
   },
    sendMails : function(component, event, helper) {
        component.set("v.isOpen", false);
        var sendAction = component.get("c.sendMail");
        var rfq = component.get("v.recordId");
        sendAction.setParams({ 
            "request" : rfq
        });
        sendAction.setCallback(this, function(response) {
            var state = response.getReturnValue();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Email Sent successfully.",
                    type: 'success'
                });
                toastEvent.fire();
            }
            else{
                var state=response.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "failed!",
                    "message": state,
                    type: 'error'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(sendAction);
    }
})
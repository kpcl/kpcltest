({
	    //To validate profile
    doInit: function(component, event, helper) {
        var getprofileStatus = component.get("c.getProfileId"); 
         getprofileStatus.setParams({
            'id': component.get("v.recordId"),
                    });
        getprofileStatus.setCallback(this, function(response) {
            var profile= response.getReturnValue();
            component.set("v.profile",profile);
            //if logged in user profile doesnt have privileges
            if(profile=='false'){
                //Toggle display
                var toggleText = component.find("text");
                $A.util.toggleClass(toggleText, "toggle");
                //Close QuickAction
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "message": "Insufficient privileges for action.",
                    type: 'error'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(getprofileStatus);
    }, 
    cancleClick : function(component, event, helper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    handleClick : function(component, event, helper) {
    	// Prepare the action to create the new contact
		var saveContactAction = component.get("c.SubmitForApproval");
		console.log('recordId**'+component.get("v.recordId"));
		saveContactAction.setParams({
		"OppID": component.get("v.recordId"),
		});
		// Configure the response handler for the action
		saveContactAction.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				var response = response.getReturnValue();
				console.log('response**'+response);
				// Prepare a toast UI message
				if(response[0] === "SUCCESS"){
					var toastEvent = $A.get("e.force:showToast");
					  toastEvent.setParams({
			            "title": "Success!",
			            "type":"success",
			            "message": "The Payment has been successfully submitted for Approval."
			        });
					// Update the UI: close panel, show toast, refresh account page
					$A.get("e.force:closeQuickAction").fire();
					toastEvent.fire();
					$A.get("e.force:refreshView").fire();
				}
				else{
					var msg = response[1];
					var errorToast = $A.get("e.force:showToast");
					 errorToast.setParams({
			            "title": "Error!",
			            "type":"error",
			            "message": msg
			        });
					$A.get("e.force:closeQuickAction").fire();
					errorToast.fire();
				}	
			}
			else if (state === "ERROR") {
				console.log('Problem saving contact, response state: ' + state);
			}
			else {
				console.log('Unknown problem, response state: ' + state);
			}
		});
		// Send the request to create the new contact
		$A.enqueueAction(saveContactAction);
    }
})
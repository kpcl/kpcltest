({
	myAction : function(component, event, helper) {
		
	},
    doInit: function(component, event, helper) {
        /*
		component.set('v.mycolumns', [
		{ fieldName: 'Name', maxColumnWidth: '10px', type: 'url', 
			typeAttributes: { value:'/one/one.app?source=aloha#/sObject/+Id+/view', label:'Name'}},
		{label: 'Status', fieldName: 'Status__c', type: 'text', maxColumnWidth: '10px'},
		{label: 'Sub Status', fieldName: 'Sub_Status__c', type: 'text', maxColumnWidth: '10px'}
		]);
        */
         $A.get('e.force:refreshView').fire();
		var action = component.get('c.getCMSOperations');
		action.setParams({"OpportunityId": component.get("v.recordId")});
		action.setCallback(this, $A.getCallback(function (response) {
		var state = response.getState();
		if (state === "SUCCESS") {
			component.set('v.cmsList', response.getReturnValue());
		 // component.set('v.mydata', response.getReturnValue());
		} else if (state === "ERROR") {
			var errors = response.getError();
		}
		}));
		$A.enqueueAction(action);
	},
})
({
    handleApplicationEvent_For_Record : function(component, event, helper) {
        component.set("v.showAccordion",true);
       //  var action = component.get("c.displayUserdetails"); 
        var action = component.get("c.displayUserdetails_Month_Year"); 
        var recordID =event.getParam("recordID");
        if(recordID==null){
           component.set("v.showAccordion",false); 
        }
        else{
        component.set("v.RecordID",recordID);
        action.setParams({
            'id':recordID ,
                    });
        action.setCallback(this, function(response) {
            var name_month_year= response.getReturnValue();
           component.set("v.UserName_Month_Year",response.getReturnValue());
                    });
        $A.enqueueAction(action);
        }
                },
    submit : function(component, event, helper) {
        var filledField =0;
        var ResultMessage = component.get("v.ResultMessage");
        var containerMap = component.get("v.Container_Map");
        var ContainerList = component.find("container");
        for (var i=0;i<ContainerList.length;i++){
            var name =ContainerList[i].get("v.name");
            var value =ContainerList[i].get("v.value");
            if(value!=null){
                containerMap[name]=value;
                filledField++;
            }
        }
        var targetMap = component.get("v.Target_Field_Values_Map");
        var Tonslist = component.find("TargetMetricTons");
        for (var i=0;i<Tonslist.length;i++){
            console.log(Tonslist[i].get("v.value"));
            console.log(Tonslist[i].get("v.name"));
            var name =Tonslist[i].get("v.name");
            var value =Tonslist[i].get("v.value");
            if(value!=null){
                targetMap[name]=value;
                filledField++;
            }
        }
        var RevenueList = component.find("TargetRevenue");
        for (var i=0;i<RevenueList.length;i++){
            console.log(RevenueList[i].get("v.value"));
            console.log(RevenueList[i].get("v.name"));
            var name =RevenueList[i].get("v.name");
            var value =RevenueList[i].get("v.value"); 
            if(value!=null){
                targetMap[name]=value;
                filledField++;
            }
        }
        if(filledField>0)
        {
        var action = component.get("c.insertData");
        action.setParams({"TargetMap":targetMap,
                          "recordID":component.get("v.RecordID"),
                          "containerMap":containerMap});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                ResultMessage=response.getReturnValue();
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                        ResultMessage=  "Error message: " +errors[0].message;
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success!",
                "message": ResultMessage
            });
            toastEvent.fire();    
            //$A.get('e.force:refreshView').fire();
            component.set("v.showAccordion",false);
            
        });
        $A.enqueueAction(action);
        }
        else
        {
            alert("Fill Some Fields");            
        }
    },
    doInit : function(component, event, helper) {
        var action = component.get("c.getCustomMetaData");
        action.setCallback(this,function(result){
            component.set("v.CustomMetaDataList",result.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
     sectionOne : function(component, event, helper) {
       helper.helperFun(component,event,'articleOne');
    },
    
   sectionTwo : function(component, event, helper) {
      helper.helperFun(component,event,'articleTwo');
    }
})
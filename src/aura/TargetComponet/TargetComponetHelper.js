({
     SearchHelper: function(component, event) {
        
        var action = component.get("c.fetchAccount");
        action.setParams({
            'searchKeyWord': component.get("v.searchKeyword")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // set searchResult list with return value from server.
                component.set("v.Table_header_Records", storeResponse);
                var records = component.get("v.Table_header_Records");
                console.log(records.ListOfEachRecord.length);
                if (records.ListOfEachRecord.length == 0) {
                    component.set("v.Message", true);
                    component.set("v.showHeaders",false);
                    component.set("v.showMonth",false);
                } else {
                    component.set("v.Message", false);
                }
                component.set("v.searchKeyword",null);
            }
 
        });
        $A.enqueueAction(action);
        
 
    },
    Assign_Returned_Data: function(component, event) {
        
    }
    
})
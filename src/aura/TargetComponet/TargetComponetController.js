({
    Save : function(component, event, helper) {
        component.set("v.showHeaders",true);
        component.set("v.showMonth",false);
        component.set("v.Message",false);
        
        var action=component.get("c.displaytargetdetails");
        //action.setParams({"insertObj":component.get("v.display")});
        action.setCallback(this,function(response)
                           {
                               component.set("v.Table_header_Records",response.getReturnValue());
                               //alert("Display "+display3);
                               var records = component.get("v.Table_header_Records");
                               console.log(component.get("v.Table_header_Records"));
                               console.log(records.ListOfEachRecord.length);
                               if (records.ListOfEachRecord.length == 0) {
                                   component.set("v.Message", true);
                                   component.set("v.showHeaders",false);
                                   component.set("v.showMonth",false);
                               } else {
                                   component.set("v.Message", false);
                               }
                           });
        $A.enqueueAction(action);
    },
    
    Current : function(component, event, helper) {
        component.set("v.showHeaders",true);
        component.set("v.showMonth",true);
        component.set("v.Message",false);
        var months    = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        var now       = new Date();
        var month = now.getMonth();
        console.log("month-"+month);
        var thisMonth = months[month];
        console.log(thisMonth);
        var monthName = component.get("v.MonthName");
        component.set("v.MonthName",thisMonth);
        var action=component.get("c.displayCurrentTargetDetails");
        //action.setParams({"insertObj":component.get("v.display")});
        action.setCallback(this,function(result)
                           {
                               var result = result.getReturnValue();
                               var ResultObject=JSON.parse(JSON.stringify(result));
                               component.set("v.Table_header_Records",ResultObject);
                               var records = component.get("v.Table_header_Records");
                               console.log(records.ListOfEachRecord.length);
                               if (records.ListOfEachRecord.length == 0) {
                                   component.set("v.Message", true);
                                   component.set("v.showHeaders",false);
                                   component.set("v.showMonth",false);
                               } else {
                                   component.set("v.Message", false);
                               } 
                           });
        $A.enqueueAction(action);
        
    },
    sortColumn : function(component, event, helper) {
        var ctarget = event.currentTarget;
        var Fieldindex = ctarget.dataset.value;
        console.log("selected Field"+Fieldindex);
        var Wrapedrecords = component.get("v.Table_header_Records");
        var objectRecord = Wrapedrecords.ListOfEachRecord;
        var headerRecord = Wrapedrecords.headerList;
        console.log(headerRecord);
        for(var i=0;i<headerRecord.length;i++){
            console.log("loop:"+headerRecord[i].name+" - "+Fieldindex);
            if(headerRecord[i].index==Fieldindex)
            {	console.log("match entered");
             var sorted = headerRecord[i].sorted;
             if(sorted==1)
             {
                 //desc
                 console.log("descending");
                 objectRecord.sort(function(a,b) {
                     return (a['recordValue'][headerRecord[i].index].toString().toLowerCase()  < b['recordValue'][headerRecord[i].index].toString().toLowerCase() ) ? 1 : ((b['recordValue'][headerRecord[i].index].toString().toLowerCase()  < a['recordValue'][headerRecord[i].index].toString().toLowerCase() ) ? -1 : 0);} );
                 sorted=0;
             }
             else if(sorted==0 || sorted==2)
             {
                 console.log("ascending");
                 objectRecord.sort(function(a,b) {return (a['recordValue'][headerRecord[i].index].toString().toLowerCase()  > b['recordValue'][headerRecord[i].index].toString().toLowerCase() ) ? 1 : ((b['recordValue'][headerRecord[i].index].toString().toLowerCase()  > a['recordValue'][headerRecord[i].index].toString().toLowerCase() ) ? -1 : 0);} ); 
                 sorted=1;
             }
             headerRecord[i].sorted=sorted; 
             
            }
            else{
                headerRecord[i].sorted=2;
            }
        }
        Wrapedrecords.records=objectRecord;
        Wrapedrecords.headerList=headerRecord;
        console.log(Wrapedrecords);
        component.set("v.Table_header_Records",Wrapedrecords);
        
        
    },
    displayfields : function(component, event, helper) {
        component.set("v.showHeaders",true);
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        console.log(id_str);
        var appEvent = $A.get("e.c:Send_Target_Record_ID_Event");
        appEvent.setParams({
            "recordID" : id_str 
        });
        appEvent.fire();
        
    },
    keyup:function(component, event, helper) {
        console.log("keyupfunction"+event.keyCode); 
        
        if (event.keyCode === 13) {
            
            var searchKeyFld = component.find("searchId");
            var srcValue = searchKeyFld.get("v.value");
            if (srcValue == '' || srcValue == null) {
                alert("Enter some value to Search");
            }
            else{
                component.set("v.showMonth",false);
                component.set("v.showHeaders",true);
                helper.SearchHelper(component,event);
            }
        }
        
    },
    Search: function(component, event, helper) {
        component.set("v.showMonth",false);
        var searchKeyFld = component.find("searchId");
        var srcValue = searchKeyFld.get("v.value");
        if (srcValue == '' || srcValue == null) {
            alert("Enter some value to Search");
        } 
        else{
            // call helper method
            helper.SearchHelper(component, event);
        }
    }
})